# Neural-Network, a machine-learning library written in Scala

## Introduction

Neural-Network is a machine-learning library written in Scala.
In contains the implementation of various standard deep learning tools, including LSTMs, TreeLSTMs and convolutional networks. 
It currently runs only on CPU (no GPU), but it is compatible with Scala.js, so you can use it for cool stuff like what you can find [here](https://vantot.gitlab.io/website/nn-demo1.html).

[...]

## Compilation

In order to compile the library into a jar file, simple run the gradlew (or gradlew.bat) script with the "jar" task.
For instance:
> ./gradlew jar

Then, the library will be available in the `/build/libs` directory.
Alternatively, you can use the "installDist" task which, in addition, generates scripts in the `/build/install/neuralNetwork/bin` directory allowing you to easily execute the Bench object situated at the root of the library.
This Bench object is an entry point to test various aspects of the library.
For instance, if you have downloaded and extracted the [MNIST dataset](http://yann.lecun.com/exdb/mnist/) in a directory MNIST_PATH, then you can train a small convolutional network with the following command:
> neural-network/build/install/neural-network/bin/neural-network ConvNet-MNIST_PATH

To compile the library for Scala.js (instead of Scala/JVM), simply give the additional `-b build-scalajs.gradle` option to the gradlew script.

## Examples

You can find a few simple examples in the [src/main/scala/examples directory](https://gitlab.com/vanTot/neural-network/tree/master/src/main/scala/examples).
In particular:
* [src/main/scala/examples/convNet.scala](https://gitlab.com/vanTot/neural-network/blob/master/src/main/scala/examples/convNet.scala) shows how to define and train a convolutional network to recognise the digits of the MNIST dataset (it is the very code that is used for the Scala.js demo [here](https://vantot.gitlab.io/website/nn-demo1.html#convolutional)).
* [src/main/scala/examples/lstm/shakespeare.scala](https://gitlab.com/vanTot/neural-network/blob/master/src/main/scala/examples/lstm/shakespeare.scala) shows how to define and train an LSTM to generate random lines for Hamlet character by character (it is the very code that is used for the Scala.js demo [here](https://vantot.gitlab.io/website/nn-demo1.html#lstm)).

## Scaladoc

The Scaladoc is accessible here: <http://vantot.gitlab.io/neural-network/scaladoc/> (it is still highly incomplete).