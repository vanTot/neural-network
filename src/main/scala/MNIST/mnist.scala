// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package MNIST;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;

trait FileReader[T] {
	def magicNumber: Int;
	
	def apply(filePath: String): T = {
		val stream: DataInputStream = new DataInputStream(new FileInputStream(filePath));
		
		{val tmp = stream.readInt;
		assert(tmp == magicNumber, "wrong magic number: " + tmp + " instead of " + magicNumber + " expected");}
		
		val data = process(stream);
		
		stream.close();
		
		data;
	}
	
	// After the magic number is checked
	def process(stream: DataInputStream): T;
}

object ImageReader extends FileReader[Array[ImageBW]] {
	val magicNumber = 2051;
	
	// After the magic number is checked
	def process(stream: DataInputStream): Array[ImageBW] = {
		val nbImages = stream.readInt;
		val nbRows = stream.readInt;
		val nbColumns = stream.readInt;
		
		val imageSize = nbRows * nbColumns; // number of pixels in an image
		
		val data = Array.ofDim[ImageBW](nbImages);
		
		var i = 0;
		while(i < nbImages) {
//			val pixels = Array.ofDim[Double](imageSize);
//			
//			var j = 0;
//			while(j < imageSize) {
//				pixels(j) = (stream.readUnsignedByte / 255.0);
//				
//				j += 1;
//			}
//			
//			data(i) = new ImageBW(pixels, nbColumns);
			data(i) = ImageBW(stream, nbColumns, nbRows);
			
			i += 1;
		}
		
		data;
	}
}

object ImageBW {
	def apply(stream: DataInputStream, width: Int, height: Int): ImageBW = {
		val imageSize = width * height;
		
		val pixels = Array.ofDim[Double](imageSize);
		
		var j = 0;
		while(j < imageSize) {
			pixels(j) = (stream.readUnsignedByte / 255.0);
			
			j += 1;
		}
		
		new ImageBW(pixels, width);
	}
}

class ImageBW(val pixels: Array[Double], val width: Int) {
	def height = pixels.length / width;
	
	def to2DArray: Array[Array[Double]] = {
		val result = Array.ofDim[Array[Double]](height);
		
		var row = 0;
		while(row < height) {
			val line = Array.ofDim[Double](width);
			
			val offset = row * width;
			
			var i = 0;
			while(i < width) {
				line(i) = pixels(i + offset);
				
				i += 1;
			}
			
			result(row) = line;
			
			row += 1;
		}
		
		result;
	}
	
	// TODO À tester
	def write(stream: DataOutputStream): Unit = {
		var i = 0;
		while(i < pixels.length) {
			stream.write((pixels(i) * 255).toInt);
			
			i += 1;
		}
	}
	
	def isSame(other: ImageBW): Boolean = if(other.width == width && other.pixels.length == pixels.length) {
		var i = 0;
		var continue = true;
		while((i < pixels.length) && continue) {
			if(pixels(i) == other.pixels(i)) i += 1;
			else continue = false;
		}
		
		continue;
	}
	else false;
}

object LabelReader extends FileReader[Array[Int]] {
	val magicNumber = 2049;
	
	// After the magic number is checked
	def process(stream: DataInputStream): Array[Int] = {
		val nbLabels = stream.readInt;
		
		val data = Array.ofDim[Int](nbLabels);
		
		var i = 0;
		while(i < nbLabels) {
			data(i) = stream.readUnsignedByte;
			
			i += 1;
		}
		
		data;
	}
}

class DataSet(val images: Array[ImageBW], val labels: Array[Int]) {
	require(images.size == labels.size, images.size + " != " + labels.size);
	
	val size = images.size;
	
	def apply(i: Int): (ImageBW, Int) = (images(i), labels(i));
}

// The MNIST data set: http://yann.lecun.com/exdb/mnist/
object MNIST {
	val trainImageFileName = "train-images.idx3-ubyte";
	val trainLabelFileName = "train-labels.idx1-ubyte";
	val testImageFileName = "t10k-images.idx3-ubyte";
	val testLabelFileName = "t10k-labels.idx1-ubyte";
	
	def loadTrain(path: String): DataSet = {
		val images = ImageReader(path + "/" + trainImageFileName);
		val labels = LabelReader(path + "/" + trainLabelFileName);
		
		new DataSet(images, labels);
	}
	
	def loadTest(path: String): DataSet = {
		val images = ImageReader(path + "/" + testImageFileName);
		val labels = LabelReader(path + "/" + testLabelFileName);
		
		new DataSet(images, labels);
	}
	
	def test(path: String): Unit = {
		val dataSet = loadTest(path);
		
		Console.println("test size: " + dataSet.size);
		
		var i = 0;
		while(i < dataSet.size && io.StdIn.readLine("Wanna see an image? ") != "n") {
			val (image, label) = dataSet(i);
			for(line <- image.to2DArray) {
				Console.println(line.map(x => if(x > 0.5) "0" else " ").mkString);
			}
			
			Console.println("label = " + label);
			
			i += 1;
		}
	}
}
