// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package PSO;

import neuralNetwork.Math;

trait PositionHandler[T] {
	def addTo(u: T, v: T): Unit; // inplace
	//def substract(u: T, v: T): T;
	//def negate(u: T): T;
	def equateTo(u: T, v: T): Unit; // inplace
	//def multiplyTo(u: T, x: Double): Unit; // inplace
	def copy(u: T): T;
	//def random(min: T, max: T): T;
	def velocityUpdate(velocity: T, pos: T, omega: Double, phiP: Double, bestPos: T, phiG: Double, globalBestPos: T): Unit; // inplace
	//def limit(pos: T, velocity: T, lowerBound: T, upperBound: T): Unit; // inplace
	def toString(u: T): String;

	def empty: T;
}

class ArrayHandler extends PositionHandler[Array[Double]] {
	// Generate a random array
	// This function is not part of PositionHandler
	def randomGenerator(lowerBound: Array[Double], upperBound: Array[Double], initVelocityRate: Double): (() => (Array[Double], Array[Double])) = {
		val velocityUpperBound = {
			val tmp = Math.substract(upperBound, lowerBound);
			Math.multiplyTo(tmp, initVelocityRate);
			
			tmp;
		}
		val velocityLowerBound = Math.negate(velocityUpperBound);
		
		def aux(min: Array[Double], max: Array[Double]): Array[Double] = { // One-liner: min.zip(max).map(x => (x._1 + util.Random.nextDouble * (x._2 - x._1)));
			val res = Array.ofDim[Double](min.length);
			
			var i = 0;
			while(i < min.length) {
				res(i) = min(i) + util.Random.nextDouble * (max(i) - min(i));
				
				i += 1;
			}
			
			res;
		}
		
		() => (aux(lowerBound, upperBound), aux(velocityLowerBound, velocityUpperBound));
	}
	
	// This function is not part of PositionHandler
	// inplace
	def limiter(lowerBound: Array[Double], upperBound: Array[Double]): ((Array[Double], Array[Double]) => Unit) = (pos: Array[Double], velocity: Array[Double]) => {
		var i = 0;
		while(i < pos.length) {
			if(pos(i) > upperBound(i)) {
				velocity(i) = 0.0;
				pos(i) = upperBound(i);
			}
			else if(pos(i) < lowerBound(i)) {
				velocity(i) = 0.0;
				pos(i) = lowerBound(i);
			}
		
			i += 1;
		}
	}
	
	// inplace
	def addTo(u: Array[Double], v: Array[Double]): Unit = Math.addTo(u, v);
	
	//def substract(u: Array[Double], v: Array[Double]): Array[Double] = Math.substract(u, v);
	
	//def negate(u: Array[Double]): Array[Double] = Math.negate(u);
	
	// inplace
	def equateTo(u: Array[Double], v: Array[Double]): Unit = Math.equateTo(u, v);
	
	// inplace
	//def multiplyTo(u: Array[Double], x: Double): Unit = Math.multiplyTo(u, x);
	
	def copy(u: Array[Double]): Array[Double] = u.clone;
	
	// inplace
	def velocityUpdate(velocity: Array[Double], pos: Array[Double], omega: Double, phiP: Double, bestPos: Array[Double], phiG: Double, globalBestPos: Array[Double]): Unit = {
		var i = 0;
		while(i < pos.length) {
			val rP = util.Random.nextDouble;
			val rG = util.Random.nextDouble;
			
			val tmp = (omega * velocity(i)) + (phiP * rP * (bestPos(i) - pos(i))) + (phiG * rG * (globalBestPos(i) - pos(i)));
			
			velocity(i) = tmp;
			pos(i) += tmp;
			
			i += 1;
		}
	}
	
	def toString(u: Array[Double]): String = u.mkString(", ");

	def empty: Array[Double] = null;
}

object PositionHandler {
	implicit val arrayHandler = new ArrayHandler;
}
