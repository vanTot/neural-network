// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package PSO;

//import akka.actor.Actor;
//import akka.actor.ActorSystem;
//import akka.actor.ActorRef;
//import akka.actor.Props;

import neuralNetwork.Math;

import PositionHandler._;

trait Particle[T] {
	def pos: T;
	def velocity: T;
	
	def bestPos: T;
	def bestScore: Double;
	def bestScore_=(x: Double): Unit;
	
	def init(_pos: T, _velocity: T, score: Double)(implicit positionHandler: PositionHandler[T]): Unit;
}

object Particle {
	def apply[T](pos: T, velocity: T, bestPos: T, bestScore: Double): Particle[T] = new ParticleImp[T](pos, velocity, bestPos, bestScore);
	def empty[T](implicit positionHandler: PositionHandler[T]): Particle[T] = new ParticleImp(positionHandler.empty, positionHandler.empty, positionHandler.empty, 0.0);
}

class ParticleImp[T](
	var pos: T,
	var velocity: T,
	var bestPos: T,
	var bestScore: Double
) extends Particle[T] {
	def init(_pos: T, _velocity: T, score: Double)(implicit positionHandler: PositionHandler[T]): Unit = {
		pos = _pos;
		velocity = _velocity;
		bestPos = positionHandler.copy(pos);
		bestScore = score;
	}
}

object Optimizer {
	def optimize[T](
		nbParticles: Int,
		omega: Double, // Inertia
		phiP: Double, // Weight of particle best
		phiG: Double, // Weight of global best
		f: (T => Double), 
		randomGenerator: (() => (T, T)), 
		stop: ((Int, Double, collection.GenSeq[_]) => Boolean), // true ~ stop
		limiterOpt: Option[(T, T) => Unit] = None, 
		verbose: Boolean = false
	)(implicit positionHandler: PositionHandler[T]): (
		T, // best position
		Double, // best score
		Int // number of iterations
	) = aux[T](Array.fill(nbParticles)(Particle.empty[T]), omega, phiP, phiG, f, randomGenerator, stop, limiterOpt, verbose);
	
	// Uses parallel collections to parallelize the computation over all particles
	// Warning: only useful with computationally expensive f functions
	def optimizeConcurrent[T](
		nbParticles: Int,
		omega: Double, // Inertia
		phiP: Double, // Weight of particle best
		phiG: Double, // Weight of global best
		f: (T => Double), 
		randomGenerator: (() => (T, T)), 
		stop: ((Int, Double, collection.GenSeq[_]) => Boolean), // true ~ stop
		limiterOpt: Option[(T, T) => Unit] = None, 
		limitParallel: Int = -1,
		verbose: Boolean = false
	)(implicit positionHandler: PositionHandler[T]): (
		T, // best position
		Double, // best score
		Int // number of iterations
	) = {
		assert(limitParallel != 1, "Calling optimizeConcurrent with (limitParallel=1) instead of optimize");

		val particles = collection.parallel.mutable.ParArray.fill(nbParticles)(Particle.empty[T]);
		if(limitParallel > 1) particles.tasksupport = new collection.parallel.ForkJoinTaskSupport(new java.util.concurrent.ForkJoinPool(limitParallel));
		
		aux[T](particles, omega, phiP, phiG, f, randomGenerator, stop, limiterOpt, verbose);
	}

	private def aux[T](
		particles: collection.GenSeq[Particle[T]],
		omega: Double, // Inertia
		phiP: Double, // Weight of particle best
		phiG: Double, // Weight of global best
		f: (T => Double), 
		randomGenerator: (() => (T, T)), 
		stop: ((Int, Double, collection.GenSeq[_]) => Boolean), // true ~ stop
		limiterOpt: Option[(T, T) => Unit], 
		verbose: Boolean
	)(implicit positionHandler: PositionHandler[T]): (
		T, // best position
		Double, // best score
		Int // number of iterations
	) = {
		val nbParticles = particles.length;
		
		// Initialisation of the swarm
		// I'm using those empty particles + init because operations on parallel collections such as map do not preserve tasksupport, so one needs to always work with the same collection
		particles.foreach(particle => {
			//val pos = positionHandler.random(lowerBound, upperBound);
			//val velocity = positionHandler.random(velocityLowerBound, velocityUpperBound);
			val (pos, velocity) = randomGenerator();
			
			if(verbose) Console.println("initialise " + positionHandler.toString(pos));
			val bestScore = f(pos); // In some hybrid algorithms, f can modify pos, so it's important to call it before cloning pos to bestPos
			
			particle.init(pos, velocity, bestScore);
		});
		
		var globalBestScore = Double.NegativeInfinity;
		
		var indexMax = -1;
		var i = 0;
		while(i < nbParticles) {
			val particle = particles(i);
			if(particle.bestScore > globalBestScore) {
				globalBestScore = particle.bestScore;
				indexMax = i;
			}
			
			i += 1;
		}
		
		val globalBestPos = positionHandler.copy(particles(indexMax).pos);
		
		Console.println("GLOBAL BEST is " + globalBestScore + ", achieved by " + positionHandler.toString(globalBestPos));
		//Console.println("best:" + globalBestScore);
		
		// Optimization
		var iter = 0;
		while(stop(iter, globalBestScore, particles) == false) {
			particles.foreach(particle => {
				positionHandler.velocityUpdate(particle.velocity, particle.pos, omega, phiP, particle.bestPos, phiG, globalBestPos);
				positionHandler.addTo(particle.pos, particle.velocity);
				
				limiterOpt match {
					case Some(limiter) => limiter(particle.pos, particle.velocity);
					case None => ();
				}
				//if(hardBound) positionHandler.limit(particle.pos, particle.velocity, lowerBound, upperBound);
				
				if(verbose) Console.println("go " + positionHandler.toString(particle.pos));
				val tmp = f(particle.pos);
				
				if(tmp > particle.bestScore) {
					particle.bestScore = tmp;
					positionHandler.equateTo(particle.bestPos, particle.pos);
				}
			});
			
			var indexMax = -1;
			var i = 0;
			while(i < nbParticles) {
				val particle = particles(i);
				if(particle.bestScore > globalBestScore) {
					globalBestScore = particle.bestScore;
					indexMax = i;
				}
				
				i += 1;
			}
			
			if(indexMax != -1) {
				positionHandler.equateTo(globalBestPos, particles(indexMax).pos);
				
				Console.println("(iter " + iter + ") GLOBAL BEST is " + globalBestScore + ", achieved by " + positionHandler.toString(globalBestPos));
				//Console.println(iter, "new best:" + globalBestScore);
			}
			
			iter += 1;
		}
		
		(globalBestPos, globalBestScore, iter);
	}

	// Turns a stop condition into a stop condition with a control file
	def controledStopCondition(stop: ((Int, Double, collection.GenSeq[_]) => Boolean), controleFileOpt: Option[String]): ((Int, Double, collection.GenSeq[_]) => Boolean) = controleFileOpt match { 
		case Some(fileName) => (iter: Int, best: Double, particles: collection.GenSeq[_]) => {
			val lines: Iterator[String] = io.Source.fromFile(fileName).getLines; // file description: parallelism \n stop
			
			val parallelism = lines.next.toInt;
			particles match {
				case particles: collection.parallel.ParSeq[_] if(parallelism != particles.tasksupport.parallelismLevel) => particles.tasksupport = new collection.parallel.ForkJoinTaskSupport(new java.util.concurrent.ForkJoinPool(parallelism));
				case _ => ();
			}
			
			if(lines.hasNext && lines.next.nonEmpty) true;
			else stop(iter, best, particles);
		}
		
		case None => stop;
	}
	
	/*// Specialized version for Position ~ Array[Double]
	def auxArray(
		baseCol: collection.GenSeq[Null],
		lowerBound: Array[Double], // "bottom-left" corner of the search space
		upperBound: Array[Double], // "top-right" corner of the search space
		omega: Double, // Inertia
		phiP: Double, // Weight of particle best
		phiG: Double, // Weight of global best
		f: (Array[Double] => Double),
		stop: ((Int, Double) => Boolean) // true ~ stop
	): (
		Array[Double], // best position
		Double, // best score
		Int // number of iterations
	) = {
		val nbParticles = baseCol.length;
		
		// Generation of the swarm
		val particles = baseCol.map(_ => {
				val pos = Array.ofDim[Double](lowerBound.length);
				val velocity = Array.ofDim[Double](lowerBound.length);
				val bestPos = Array.ofDim[Double](lowerBound.length);
				
				var d = 0;
				while(d < lowerBound.length) {
					val range = upperBound(d) - lowerBound(d);
					val tmpP = util.Random.nextDouble * range + lowerBound(d);
					
					pos(d) = tmpP;
					bestPos(d) = tmpP;
					velocity(d) = util.Random.nextDouble * (2 * range) - range;
					
					d += 1;
				}
				
				val bestScore = f(pos);
				
				new ParticleImp(pos, velocity, bestPos, bestScore);
		});
		
		val globalBestPos = Array.ofDim[Double](lowerBound.length);
		var globalBestScore = Double.NegativeInfinity;
		
		var indexMax = -1;
		var i = 0;
		while(i < nbParticles) {
			val particle = particles(i);
			if(particle.bestScore > globalBestScore) {
				globalBestScore = particle.bestScore;
				indexMax = i;
			}
			
			i += 1;
		}
		
		if(indexMax != -1) {
			Console.println("GLOBAL BEST is " + globalBestScore + ", achieved by " + particles(indexMax).pos.mkString(", "));
			//Console.println("best:" + globalBestScore);
			
			Math.equateTo(globalBestPos, particles(indexMax).pos);
		}
		
		// Optimization
		var iter = 0;
		while(stop(iter, globalBestScore) == false) {
			particles.foreach(particle => {
				val pos = particle.pos;
				val velocity = particle.velocity;
				val bestPos = particle.bestPos;
				
				var d = 0;
				while(d < pos.length) {
					val rP = util.Random.nextDouble;
					val rG = util.Random.nextDouble;
					
					val tmp = (omega * velocity(d)) + (phiP * rP * (bestPos(d) - pos(d))) + (phiG * rG * (globalBestPos(d) - pos(d)));
					
					velocity(d) = tmp;
					pos(d) += tmp;
					
					d += 1;
				}
				
				val tmp = f(pos);
				
				if(tmp > particle.bestScore) {
					particle.bestScore = tmp;
					Math.equateTo(bestPos, pos);
				}
			});
			
			var indexMax = -1;
			var i = 0;
			while(i < nbParticles) {
				val particle = particles(i);
				if(particle.bestScore > globalBestScore) {
					globalBestScore = particle.bestScore;
					indexMax = i;
				}
				
				i += 1;
			}
			
			if(indexMax != -1) {
				Console.println("GLOBAL BEST is " + globalBestScore + ", achieved by " + particles(indexMax).pos.mkString(", "));
				//Console.println(iter, "new best:" + globalBestScore);
				
				Math.equateTo(globalBestPos, particles(indexMax).pos);
			}
			
			iter += 1;
		}
		
		(globalBestPos, globalBestScore, iter);
	}*/
}
