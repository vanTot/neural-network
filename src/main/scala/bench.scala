// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import annotation.tailrec;

import neuralNetwork._;
import ParameterBrickConversions._;

object Bench {
	def main(args: Array[String]): Unit = {
		if(args.length == 0) {
			Console.println("NeuralNetwork\n");
			neuralNetworkBench;
			
			Console.println("\n\nPSO\n");
			PSOBench();
			
			Console.println("\n\nLSTM test\n");
			examples.lstm.graph.LSTM.test;
		}
		else for(arg <- args) arg match {
			case "PSO" => {
				Console.println("PSO\n");
				PSOBench();
			}
			
			case "NN" => {
				Console.println("NeuralNetwork\n");
				neuralNetworkBench;
			}

			case "attention" => {
				Console.println("Attention\n");
				examples.Attention.run;
			}
			
			case "graph.ChildSumTreeLSTM" => {
				Console.println("graph.ChildSumTreeLSTM test\n");
				examples.lstm.graph.ChildSumTreeLSTM.test;
			}
			
			case "LSTM-Shakespeare" => {
				Console.println("LSTM Shakespeare test\n");
				examples.lstm.Shakespeare.run;
			}
			
			case "LSTM" => {
				Console.println("LSTM test\n");
				examples.lstm.LSTM.test;
			}
			
			case "graph.LSTM" => {
				Console.println("graph.LSTM test\n");
				examples.lstm.graph.LSTM.test;
			}
			
			case str if(str.startsWith("ConvNet")) => {
				Console.println("ConvNet test\n");
				
				val dataPath = str.drop(8); // str is "ConvNet-PATH"
				examples.ConvNet.run(dataPath);
			}
			
			case str if(str.startsWith("LoadMNIST")) => {
				Console.println("Load MNIST test\n");
				
				val dataPath = str.drop(10); // str is "LoadMNIST-PATH"
				MNIST.MNIST.test(dataPath);
			}
			
			case str if(str.startsWith("LoadGloVe")) => {
				Console.println("Load GloVe\n");
				
				val dataFile = str.drop(10); // str is "LoadGloVe-FILE"
				neuralNetwork.embeddings.GloVe.test(dataFile);
			}
			
			case str => {
				Console.println("What is \"" + str + "\"?");
			}
		}
	}
	
	def run[A](nbIter: Int, nbRun: Int)(f: (() => A)): Long = {
		var ellapsedTime = 0L;
		
		var run = 0;
		while(run < nbRun) {
			ellapsedTime = time(nbIter)(f);
			
			Console.println(run + "th run: " + ellapsedTime + " ns");
				
			run += 1;
		}
		
		ellapsedTime;
	}
	
	def time[A](nbIter: Int)(f: (() => A)): Long = {
		var time = 0L;
		
		var iter = 0;
		while(iter < nbIter) {
			val t0 = System.nanoTime;
			
			val result = f();
			
			time += System.nanoTime - t0;
			
			iter += 1;
		}
		
		time;
	}
	
	def neuralNetworkBench: Unit = {
		val nbRun = 4;
		val nbIter = 100000;
		
		val inSize = 200;
		val outSize = 100;
		
		// Note: remember that tanh is relatively expensive
		val distribution = ((c: Double) => (util.Random.nextDouble - 0.5) / c);
		val layer2D = TransformNNFactory.buildLayer(inSize, outSize, Array("2DL", "Tanh"), distribution);
		val layerMat = TransformNNFactory.buildLayer(inSize, outSize, Array("ML", "Tanh"), distribution);
		
		/*val layerBreezeMat = {
			val breezeMat = breeze.linalg.DenseMatrix.fill[Double](outSize, inSize)(distribution(inSize));
			val breezeVec = breeze.linalg.DenseVector.fill[Double](outSize)(distribution(1));
			
			BreezeMatLayer(breezeMat, breezeVec, BreezeTanh)
		}*/
		
		{
			val in = Array.fill(inSize)(util.Random.nextDouble);
			//val breezeIn = breeze.linalg.DenseVector.rand(inSize);
			
			Console.println("apply");
		
			Console.println(layer2D);
			val time1 = run(nbIter, nbRun)(()  => layer2D.apply(in));
		
			Console.println(layerMat);
			val time2 = run(nbIter, nbRun)(()  => layerMat.apply(in));
			Console.println("relative: " + (100 * time2 / time1) + " %");
		
			/*Console.println(layerBreezeMat);
			val time3 = run(nbIter, nbRun)(()  => layerBreezeMat.apply(breezeIn));
			Console.println("relative: " + (100 * time3 / time1) + " %");*/
		}
		
		Console.println;
		
		{
			val in = Array.fill(inSize)(util.Random.nextDouble);
			val delta = Array.fill(outSize)(util.Random.nextDouble);
			/*val breezeIn = breeze.linalg.DenseVector.rand(inSize);
			val breezeDelta = breeze.linalg.DenseVector.rand(outSize);*/
			
			Console.println("gradient");
		
			Console.println(layer2D);
			val time1 = run(nbIter, nbRun)(()  => layer2D.gradient(in)._2(delta));
		
			Console.println(layerMat);
			val time2 = run(nbIter, nbRun)(()  => layerMat.gradient(in)._2(delta));
			Console.println("relative: " + (100 * time2 / time1) + " %");
		
			/*Console.println(layerBreezeMat);
			val time3 = run(nbIter, nbRun)(()  => layerBreezeMat.gradient(breezeIn)._2(breezeDelta));
			Console.println("relative: " + (100 * time3 / time1) + " %");*/
		}
		
		Console.println;
		
		{
			val x = util.Random.nextDouble;
			
			Console.println("p.multiply");
		
			Console.println(layer2D);
			val time1 = run(nbIter, nbRun)(()  => layer2D.p.multiply(x));
			
			Console.println(layerMat);
			val time2 = run(nbIter, nbRun)(()  => layerMat.p.multiply(x));
			Console.println("relative: " + (100 * time2 / time1) + " %");
			
			/*Console.println(layerBreezeMat);
			val time3 = run(nbIter, nbRun)(()  => layerBreezeMat.p.multiply(x));
			Console.println("relative: " + (100 * time3 / time1) + " %");*/
		}
		
		Console.println;
		
		{var referenceTime = 0L;
		var ellapsedTime = 0L;
		Console.println("p.norm2Sq");
		
		Console.println(layer2D);
		{val f = (()  => {
			layer2D.p.norm2Sq;
		});
		
		var run = 0;
		while(run < nbRun) {
			ellapsedTime = time(nbIter)(f);
			
			Console.println(run + "th run: " + ellapsedTime + " ns");
			
			run += 1;
		}
		
		referenceTime = ellapsedTime;
		Console.println("relative: " + (100 * ellapsedTime / referenceTime) + " %");}
		
		Console.println(layerMat);
		{val f = (()  => {
			layerMat.p.norm2Sq;
		});
		
		var run = 0;
		while(run < nbRun) {
			ellapsedTime = time(nbIter)(f);
			
			Console.println(run + "th run: " + ellapsedTime + " ns");
			
			run += 1;
		}
		
		Console.println("relative: " + (100 * ellapsedTime / referenceTime) + " %");}}
	}
	
	def PSOBench(dim: Int = 10, sleep: Int = 0, jMax: Int = 100000000, limitParallel: Int = 2): Unit = {
		val f: (Array[Double] => Double) = {
			val target = 0.5;
			
			(x => {
				var i = 0;
				for(j <- 0 to jMax) {i += j;};	// Some computation
				Thread.sleep(sleep);			// Some sleep
				-math.abs(x.sum - target);		// Something sensible
			});
		}
		
		val lowerBound = Array.fill[Double](dim)(-1.0);
		val upperBound = Array.fill[Double](dim)(1.0);
		val randomGenerator = PSO.PositionHandler.arrayHandler.randomGenerator(lowerBound, upperBound, 1.0);
		val limiterOpt = Some(PSO.PositionHandler.arrayHandler.limiter(lowerBound, upperBound));
		
		val nbParticles = 3 * dim;
		
		val omega = 0.5;	// Inertia
		val phiP = 0.1;		// Step toward particle best
		val phiG = 0.4;		// Step toward global best
		
		val maxIter: Int = 100000;
		val delta: Double = -1.0E-5;
		
		Console.println("-- sequential PSO");
		
		{val stop = (iter: Int, best: Double, particles: collection.GenSeq[_]) => {
			if(iter >= maxIter || best > delta) true;
			else false;
		}
		
		val beginTime = System.currentTimeMillis;
		
		val result = PSO.Optimizer.optimize(
			nbParticles,
			omega, // Inertia
			phiP, // Weight of particle best
			phiG, // Weight of global best
			f, 
			randomGenerator, 
			stop, // true ~ stop
			limiterOpt
		);
		
		Console.println("time per (iter+1): " + ((System.currentTimeMillis - beginTime) / (result._3+1)));
		
		Console.println("bestPos: " + result._1.mkString(", "));
		Console.println("bestF: " + result._2);
		Console.println("nb of iterations: " + result._3);}
		
		Console.println("-- concurrent PSO");
		
		{val stop = (iter: Int, best: Double, particles: collection.GenSeq[_]) => {
			if(iter >= maxIter || best > delta) true;
			else false;
		}
		
		val beginTime = System.currentTimeMillis;
		
		val result = PSO.Optimizer.optimizeConcurrent(
			nbParticles,
			omega, // Inertia
			phiP, // Weight of particle best
			phiG, // Weight of global best
			f, 
			randomGenerator, 
			stop, // true ~ stop
			limiterOpt,
			limitParallel
		);
		
		Console.println("time per (iter+1): " + ((System.currentTimeMillis - beginTime) / (result._3+1)));
		
		Console.println("bestPos: " + result._1.mkString(", "));
		Console.println("bestF: " + result._2);
		Console.println("nb of iterations: " + result._3);}
		
		/*Console.println("-- sequential PSO (Array[Double] specialisation)");
		
		{val stop = (iter: Int, best: Double, particles: collection.GenSeq[_]) => {
			if(iter >= maxIter || best > delta) true;
			else false;
		}
		
		val beginTime = System.currentTimeMillis;
		
		val result = PSO.Optimizer.auxArray(
			Array.fill(nbParticles)(null),
			lowerBound, // "bottom-left" corner of the search space
			upperBound, // "top-right" corner of the search space
			omega, // Inertia
			phiP, // Weight of particle best
			phiG, // Weight of global best
			f,
			stop // true ~ stop
		);
		
		Console.println("time per iter: " + ((System.currentTimeMillis - beginTime) / result._3));
		
		Console.println("bestPos: " + result._1.mkString(", "));
		Console.println("bestF: " + result._2);
		Console.println("nb of iterations: " + result._3);}*/
	}
}
