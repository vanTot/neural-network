// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package examples;

import annotation.tailrec;

import collection._;

import neuralNetwork._;
import ParameterBrickConversions._;

object Attention {
	def run: Unit = {
		val vocSize = 10;
		
		//val nbMerge = vocSize; // Two words of the source language that are translated as one word of the target language: a b -> C
		val nbMerge = 0;
		val merges = Array.fill(nbMerge)(((util.Random.nextInt(vocSize), util.Random.nextInt(vocSize)), util.Random.nextInt(vocSize))).toMap;
		
		//val nbSplit = math.sqrt(vocSize).toInt; // One word of the source language that is translated as two words of the target language: a -> B C
		val nbSplit = 0;
		val splits = Array.fill(nbSplit)((util.Random.nextInt(vocSize), (util.Random.nextInt(vocSize), util.Random.nextInt(vocSize)))).toMap;
		
		//val nbInversion = vocSize; // Two words that are translated in reverse order: a b -> B A
		val nbInversion = 0;
		val inversions = Array.fill(nbInversion)((util.Random.nextInt(vocSize), util.Random.nextInt(vocSize))).toSet;

		// Test
		Console.println("merges: " + merges);
		Console.println("splits: " + splits);
		Console.println("inversions: " + inversions);
		for(i <- 0 until 10) {
			val length = 1 + naiveBinomial(20, 0.5);
			val word = Array.fill(length)(util.Random.nextInt(vocSize));
			val translation = translate(word, vocSize, merges, splits, inversions);
			Console.println(word.mkString("") + " -> " + translation.mkString(""));
		}

		val vectorSize = 8;
		val inputMemorySize = 16;
		val outputMemorySize = 8;

		val model = {
			val vectorsSource = Array.fill(vocSize)(Array.fill(vectorSize)(util.Random.nextGaussian));
			val vectorsTarget = Array.fill(vocSize + 1)(Array.fill(vectorSize)(util.Random.nextGaussian)); // +1 for the end-of-sentence token
			val inputForwardInitState = Array.fill(2 * inputMemorySize)(util.Random.nextGaussian);
			val inputBackwardInitState = Array.fill(2 * inputMemorySize)(util.Random.nextGaussian);
			val inputBiLSTM = neuralNetwork.lstm.BiLSTMEncoder(vectorSize, inputMemorySize);
			val outputInitStater = TransformNNFactory(Array("ML:Tanh"), Array((2 * outputMemorySize), inputMemorySize));
			val outputLSTM = neuralNetwork.lstm.LSTMFactory(outputMemorySize, vectorSize);
			val attention = neuralNetwork.Attention(outputLSTM.memorySize, inputBiLSTM.outputSize);
			val outputClassifier = TransformNNFactory(Array("ML:SoftMax"), Array((vocSize + 1), (outputMemorySize + attention.vectorSize)));

			new Model(vectorsSource, vectorsTarget, inputForwardInitState, inputBackwardInitState, inputBiLSTM, outputInitStater, outputLSTM, attention, outputClassifier);
		}

		val learningMethod = learning.NormalLearning(0.001);
		//val learningMethod = learning.MomentumLearning(0.01, 0.9);
		//val learningMethod = learning.Adagrad(0.1);
		
		val regularizer = learning.ClockRegularizer(100, learning.L2(0.00001));
		//val regularizer = learning.ClockRegularizer(0, learning.L2(0.001));

		val learner = learningMethod.instantiate(model);
		val nbEpoch = 1000;
		val epochSize = 1000;
		for(epoch <- 0 until nbEpoch) {
			var sumLoss = 0.0;
			for(iter <- 0 until epochSize) {
				val length = 1 + naiveBinomial(20, 0.5);
				val word = Array.fill(length)(util.Random.nextInt(vocSize));
				val translation = translate(word, vocSize, merges, splits, inversions);
				
				val (gradient, loss) = model.instanceToGrad(word, translation);
				sumLoss += loss;
				learner.updateModel(gradient);
				regularizer(model);
			}
			
			Console.println("loss at epoch " + epoch + ": " + (sumLoss / epochSize));
			Console.println("model parameters (max, avg): " + model.parameters.statsAvg);
		}

		// TODO Testing




	}
	
	class Model(
		val vectorsSource: Array[Array[Double]],
		val vectorsTarget: Array[Array[Double]],
		val inputForwardInitState: Array[Double],
		val inputBackwardInitState: Array[Double],
		val inputBiLSTM: neuralNetwork.lstm.BiLSTMEncoder,
		val outputInitStater: TransformNN,
		val outputLSTM: neuralNetwork.lstm.LearningLSTM,
		val attention: Attention,
		val outputClassifier: TransformNN
	) extends neuralNetwork.Model {
		val parameters: CompoundParameter = CompoundParameter(
			vectorsSource,
			vectorsTarget,
			inputForwardInitState,
			inputBackwardInitState,
			inputBiLSTM.param,
			outputInitStater.param,
			outputLSTM.param,
			attention.param,
			outputClassifier.param
		);

		def instanceToGrad(word: Array[Int], translation: Array[Int]): (CompoundParameter, Double) = {
			val sequence = word.map(vectorsSource(_));

			val (encodings, biLSTMF) = {
				val tmp = inputBiLSTM.encodeAndGradient(inputForwardInitState, inputBackwardInitState, sequence);
				(tmp._1.toArray, tmp._2);
			}

			val dOutputClassifier = ParamList(outputClassifier.emptyParam);
		
			var attentionFs: List[(Array[Double] => (List[ParameterBrick], Array[Array[Double]], Array[Double]))] = Nil;
			var dOutputs: List[Array[Double]] = Nil;
			var chars: List[Int] = Nil;
			var outputLSTMFs: List[(Array[Double] => (List[ParameterBrick], Array[Double]))] = Nil;
			
			var loss: Double = 0.0;
			
			var (outputState, outputStateF) = outputInitStater.transformAndGradient(java.util.Arrays.copyOfRange(encodings(0), inputBiLSTM.forwardSize, inputBiLSTM.outputSize)); // The initial state of the decoder is built from the backward encoding of the input sentence (BEIS) - It seems to be what is done in Bahdanau et al. (2014), p.13
			var prevVector = vectorsTarget(vectorsTarget.length - 1); // Vector of the end-of-sentence token
			var i = 0;
			while(i < translation.length) {
				val context = java.util.Arrays.copyOfRange(outputState, outputLSTM.memorySize, outputLSTM.height); // The context is the output part of the LSTM state
				
				// Phase A
				val (attentionedVector, attentionF) = attention.attendAndGradient(context, encodings);
				attentionFs ::= attentionF;

				// Phase B
				val (probabilities, probabilitiesF) = outputClassifier.transformAndGradient(context ++ attentionedVector);
				val dOut = CrossEntropy.derivativeOp(translation(i), probabilities);
				val dTmp = probabilitiesF(dOut);
				dOutputClassifier.addTo(dTmp._1);
				dOutputs ::= dTmp._2;
				loss += CrossEntropy(translation(i), probabilities);
				
				chars ::= translation(i);
				
				if(i != (translation.length - 1)) {
					// Phase C
					val tmp = outputLSTM.outputAndGradient(outputState ++ vectorsTarget(translation(i)));
					outputState = tmp._1;
					outputLSTMFs ::= tmp._2;

					prevVector = vectorsTarget(translation(i));
				}

				i += 1;
			}

			val (dAttention, dOutputLSTM, dEncodings, dVectorsTarget, dState) = backwardPass(attentionFs, dOutputs, chars, outputLSTMFs);

			// Two things remain:
			// — send dState through outputStateF
			val (dOutputInitStater, dBEIS) = outputStateF(dState);
			Math.addTo(dEncodings(0), inputBiLSTM.forwardSize, dBEIS);

			// — send dEncodings (+ the part that come from the previous stage) through biLSTMF
			val (dInputBiLSTM, dSequence, dInputForwardInitState, dInputBackwardInitState) = biLSTMF(dEncodings);

			val dVectorsSource = mutable.Map[Int, Array[Double]]();
			(word, dSequence).zipped.foreach((id, u) => dVectorsSource.get(id) match {
				case Some(v) =>	Math.addTo(v, u);
				case None => dVectorsSource(id) = u;
			});
			
			val gradient = CompoundParameter(
				dVectorsSource,
				dVectorsTarget,
				dInputForwardInitState,
				dInputBackwardInitState,
				dInputBiLSTM,
				dOutputInitStater,
				dOutputLSTM,
				dAttention,
				dOutputClassifier
			);
			
			(gradient, loss);
		}

		def backwardPass(
			attentionFs: List[(Array[Double] => (List[ParameterBrick], Array[Array[Double]], Array[Double]))],
			dOutputs: List[Array[Double]],
			chars: List[Int],
			outputLSTMFs: List[(Array[Double] => (List[ParameterBrick], Array[Double]))]
		): (ParamList, ParamList, Array[Array[Double]], mutable.Map[Int, Array[Double]], Array[Double]) = {
			val dAttention = ParamList(attention.param);
			val dOutputLSTM = ParamList(outputLSTM.param);
			var dEncodings: Option[Array[Array[Double]]] = None;
			val dVectorsTarget = mutable.Map[Int, Array[Double]]();

			@tailrec
			def loop(
				_attentionFs: List[(Array[Double] => (List[ParameterBrick], Array[Array[Double]], Array[Double]))],
				_dOutputs: List[Array[Double]],
				_chars: List[Int],
				_outputLSTMFs: List[(Array[Double] => (List[ParameterBrick], Array[Double]))],
				dState: Array[Double],
				notFirst: Boolean
			): Array[Double] = _attentionFs match {
				case attentionF :: __attentionFs => {
					val dOutput :: __dOutputs = _dOutputs;
					val char    :: __chars    = _chars;

					// Phase C
					val (__outputLSTMFs, dPrevState) = if(notFirst) {
						val outputLSTMF :: __outputLSTMFs = _outputLSTMFs;
						val tmpOutputLSTM = outputLSTMF(dState);
						dOutputLSTM.addTo(tmpOutputLSTM._1);
						val dPrevState = java.util.Arrays.copyOfRange(tmpOutputLSTM._2, 0, outputLSTM.height);
						dVectorsTarget.get(char) match {
							case Some(v) => Math.addTo(v, tmpOutputLSTM._2, outputLSTM.height);
							case None => dVectorsTarget(char) = java.util.Arrays.copyOfRange(tmpOutputLSTM._2, outputLSTM.height, outputLSTM.width);
						}

						(__outputLSTMFs, dPrevState);
					}
					else (_outputLSTMFs, Array.ofDim[Double](outputLSTM.height));

					// Phase B
					Math.addTo(dPrevState, outputLSTM.memorySize, dOutput);
					val dAttentioned = java.util.Arrays.copyOfRange(dOutput, outputLSTM.memorySize, outputClassifier.width);

					// Phase A
					val tmpAttention = attentionF(dAttentioned);
					dAttention.addTo(tmpAttention._1);
					dEncodings match {
						case Some(array2D) => structureHandlers.Array2DHandler.addTo(array2D, tmpAttention._2);
						case None => dEncodings = Some(tmpAttention._2);
					}
					Math.addTo(dPrevState, outputLSTM.memorySize, tmpAttention._3);

					loop(__attentionFs, __dOutputs, __chars, __outputLSTMFs, dPrevState, true);
				}

				case Nil => {
					assert(_dOutputs.isEmpty);
					assert(_chars.isEmpty);
					assert(_outputLSTMFs.isEmpty);

					dState;
				}
			}

			val dState = loop(attentionFs, dOutputs, chars, outputLSTMFs, null, false);

			(dAttention, dOutputLSTM, dEncodings.get, dVectorsTarget, dState);
		}
	}

	def translate(word: Array[Int], vocSize: Int, merges: Map[(Int, Int), Int], splits: Map[Int, (Int, Int)], inversions: Set[(Int, Int)]): Array[Int] = {
		val result = Array.ofDim[List[Int]](word.length + 1); // +1 for the end-of-sentence token.
		result(word.length) = List(vocSize); // End-of-sentence token
		
		// Merges
		for(i <- 0 until (word.length - 1)) merges.get((word(i), word(i+1))) match {
			case None => ();
			case Some(c) => {
				result(i) = List(c);
				result(i+1) = Nil;
			}
		}

		// Splits
		for(i <- 0 until word.length if(result(i) == null)) splits.get(word(i)) match {
			case None => ();
			case Some((c1, c2)) => result(i) = List(c1, c2);
		}

		// Inversions
		for(i <- 0 until (word.length - 1) if(result(i) == null && result(i+1) == null)) inversions.contains((word(i), word(i+1))) match {
			case false => ();
			case true => {
				result(i) = List(word(i+1));
				result(i+1) = List(word(i));
			}
		}

		// Normal translation
		for(i <- 0 until word.length if(result(i) == null)) result(i) = List(word(i));

		result.flatten;
	}

	def naiveBinomial(n: Int, p: Double): Int = {
		var result = 0;
		for(i <- 0 until n) if(util.Random.nextDouble < p) result += 1;

		result;
	}
}
