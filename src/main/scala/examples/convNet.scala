// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package examples;

import annotation.tailrec;

import neuralNetwork._;
import ParameterBrickConversions._;

object ConvNet {
	def run(dataPath: String): Unit = {
		Console.println("Loading MNIST training set");
		val trainSet = MNIST.MNIST.loadTrain(dataPath);
		
		val (imageHeight, imageWidth) = {
			val tmp = trainSet.images.head;
			
			(tmp.height, tmp.width);
		}
		
		Console.println("Generating the network");
		// Definition of the model (contains all the parameters)
		class ModelImp(val network: TransformNN) extends Model {
			val parameters: CompoundParameter = CompoundParameter(network.param);
			
			def predict(image: Array[Double]): Int = {
				val out = network.execute(image);
				
				Math.indexOfMax(out);
			}
			
			def instanceToGrad(pixels: Array[Double], label: Int): (CompoundParameter, Double) = {
				// Maximize the opposite of the cross entropy
				val (out, dF) = network.transformAndGradient(pixels);
				val dOut = CrossEntropy.derivativeOp(label, out);
				val dParam = dF(dOut)._1;
				
				val gradient = CompoundParameter(dParam);
				val loss = CrossEntropy(label, out);
				
				(gradient, loss);
			}
		}
		
		val model = {
			val network = {
				// 1 convolutional layer consisting of 10 filters of dimension 9x9 with 2x2 max-pooling
				val convLayer = {
					val nbFilters = 10;
					val filtersXSize = 9;
					val filtersYSize = 9;
					val poolingX = 2;
					val poolingY = 2;
					
					val filtersType = "ML";
					val filtersNonLinearity = "ReLU";
					val pixelSize = 1;
					
					val layerOptions = Array(
						(filtersType + "|" + filtersNonLinearity), // filters options
						(filtersXSize + "x" + filtersYSize), // filterSize 
						(poolingX + "x" + poolingY), // pooling
					).map(_.toString);
					
					val distribution = ((c: Double) => (util.Random.nextDouble - 0.5) / c);
					
					TransformNNFactory.buildConvLayer(imageWidth, imageHeight, pixelSize, nbFilters, layerOptions, distribution);
				}
				
				// followed by a matrix layer with SoftMax non-linearity
				val outLayer = {
					val inSize = convLayer.p.height;
					val outSize = 10;
					val distribution = ((c: Double) => (util.Random.nextDouble - 0.5) / c);
					
					TransformNNFactory.buildLayer(inSize, outSize, Array("ML", "SoftMax"), distribution);
				}
				
				TransformNN(List(convLayer, outLayer));
			}
			
			new ModelImp(network);
		}
		
		Console.println("Training (1 epoch)");
		{
			val beginTime = System.currentTimeMillis;
			
			val learner = learning.NormalLearning(0.001).instantiate(model);
			var i = 0;
			while(i < trainSet.size) {
				val (image, label) = trainSet(i);
				val (gradient, loss) = model.instanceToGrad(image.pixels, label);
				learner.updateModel(i, gradient);
				//Console.println("loss at iteration " + i + ": " + loss);
				
				i += 1;
			}
			val time = System.currentTimeMillis - beginTime;
			Console.println("execution time: " + time);
		}
		
		Console.println("Loading MNIST testing set");
		val testSet = MNIST.MNIST.loadTest(dataPath);
		
		Console.println("Testing");
		{
			val beginTime = System.currentTimeMillis;
			var nbCorrect = 0;
			var i = 0;
			while(i < testSet.size) {
				val (image, label) = testSet(i);
				
				val predicted = model.predict(image.pixels);
				if(predicted == label) nbCorrect += 1;
				
				i += 1;
			}
			Console.println("Precision on test set = " + (nbCorrect.toDouble / testSet.size));
			val time = System.currentTimeMillis - beginTime;
			Console.println("execution time: " + time);
		}
		
		io.StdIn.readLine("File in which to write the network (empty string for none)? ") match {
			case "" => ();
			case fileName => {
				val writer = new java.io.BufferedWriter(new java.io.FileWriter(fileName));
				
				writer.append(model.network.customSerialize);
				
				writer.close;
			}
		}
		
		{var i = 0;
		while(i < testSet.size && io.StdIn.readLine("Execute the network on an image? ") != "n") {
			val (image, label) = testSet(i);
			for(line <- image.to2DArray) {
				Console.println(line.map(x => if(x > 0.5) "0" else " ").mkString);
			}
			
			Console.println("label = " + label);
			
			val predicted = model.predict(image.pixels);
			Console.println("predicted label = " + predicted + " " + (if(predicted == label) "correct" else "ERROR"));
			
			i += 1;
		}}
	}
}
