// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package examples.lstm.graph;

import annotation.tailrec;

import neuralNetwork._;
import ParameterBrickConversions._;
import lstm.graph._;

object LSTM {
	def test: Unit = {
		problem1(learning.NormalLearning(0.001), 3, 10);
		problem2(learning.NormalLearning(0.001), 3, 10, 5);
	}
	
	// Make the LSTM learn by heart a sequence of symbols
	def problem1(learningMethod: learning.LearningMethod, alphabetSize: Int, sequenceLength: Int): Unit = {
		class ModelImp(val initLSTM: LSTMCore, val endNN: TransformNN) extends Model {
			val parameters: CompoundParameter = CompoundParameter(endNN.param, initLSTM.param);
			
			def instanceToGrad(sequence: Array[Int]): (CompoundParameter, Double) = {
				val dEndNNParam = endNN.emptyParam;
				
				var currentLSTM: LearningLSTM = initLSTM.toLeaf;
				var lstmInfos: List[LearningLSTM] = List(currentLSTM);
				var loss: Double = 0.0;
				
				var i = 0;
				while(i < sequenceLength - 1) {
					val input = new Array[Double](alphabetSize);
					input(sequence(i)) = 1.0;
					currentLSTM = currentLSTM.update(input);
					
					val (output, dF) = endNN.transformAndGradient(currentLSTM.output);
					
					// Gradient
					val dOut = CrossEntropy.derivativeOp(sequence(i + 1), output);
					val (dEndNN, dTmp) = dF(dOut);
					
					(dEndNNParam, dEndNN).zipped.map((p, q) => p.addTo(q));
					
					System.arraycopy(dTmp, 0, currentLSTM.dOutput, 0, dTmp.length);
					
					lstmInfos ::= currentLSTM;
					loss += CrossEntropy(sequence(i + 1), output);
					i += 1;
				}
				
				val dLSTM = backwardPass(lstmInfos, initLSTM.emptyGatesParam);
				
				val gradient = CompoundParameter(dEndNNParam, dLSTM);
				
				(gradient, loss);
			}
		}
		
		val model = {
			val cellStateSize = math.ceil(math.log(sequenceLength) / math.log(2.0)).toInt;
			val initLSTM = LSTMCore(cellStateSize, alphabetSize);
			val endNN = TransformNNFactory(Array("ML:SoftMax"), Array(alphabetSize, initLSTM.output.length));
			
			new ModelImp(initLSTM, endNN)
		}
		
		val sequence = Array.fill(sequenceLength)(util.Random.nextInt(alphabetSize)); // The sequence to be learnt
		
		val learner = learningMethod.instantiate(model);
		val nbIterations = 100000;
		for(iter <- 0 until nbIterations) {
			val (gradient, loss) = model.instanceToGrad(sequence);
			learner.updateModel(iter, gradient);
			Console.println("loss at iteration " + iter + ": " + loss);
		}
	}
	
	// Train a LSTM to read a sequence and to indicate which letter was the most frequent in the last 5 elements
	def problem2(learningMethod: learning.LearningMethod, alphabetSize: Int, sequenceLength: Int, windowLength: Int): Unit = {
		class ModelImp(val initLSTM: LSTMCore, val endNN: TransformNN) extends Model {
			val parameters: CompoundParameter = CompoundParameter(endNN.param, initLSTM.param);
			
			def instanceToGrad(sequenceLength: Int): (CompoundParameter, Double) = {
				val dEndNNParam = endNN.emptyParam;
				
				val counts = new Array[Double](alphabetSize);
				var lastElements = new collection.mutable.Queue[Int]();
				
				var currentLSTM: LearningLSTM = initLSTM.toLeaf;
				var lstmInfos: List[LearningLSTM] = List(currentLSTM);
				var loss: Double = 0.0;
				
				var i = 0;
				while(i < sequenceLength) {
					val rand = util.Random.nextInt(alphabetSize); // The sequence is randomly generated progressively
					
					val input = new Array[Double](alphabetSize);
					input(rand) = 1.0;
					currentLSTM = currentLSTM.update(input);
					
					counts(rand) += 1;
					lastElements.enqueue(rand);
					if(lastElements.size > windowLength) counts(lastElements.dequeue) -= 1;
					
					val majority = Math.indexOfMax(counts);
					
					val (output, dF) = endNN.transformAndGradient(currentLSTM.output);
					
					val dOut = CrossEntropy.derivativeOp(majority, output);
					val (dEndNN, dTmp) = dF(dOut);
					
					(dEndNNParam, dEndNN).zipped.map((p, q) => p.addTo(q));
					
					System.arraycopy(dTmp, 0, currentLSTM.dOutput, 0, dTmp.length);
					
					lstmInfos ::= currentLSTM;
					loss += CrossEntropy(majority, output);
					i += 1;
				}
				
				val dLSTM = backwardPass(lstmInfos, initLSTM.emptyGatesParam);
				
				val gradient = CompoundParameter(dEndNNParam, dLSTM);
				
				(gradient, loss);
			}
		}
		
		val model = {
			val cellStateSize = (windowLength + alphabetSize);
			val initLSTM = LSTMCore(cellStateSize, alphabetSize);
			val endNN = TransformNNFactory(Array("ML:SoftMax"), Array(alphabetSize, initLSTM.output.length));
			
			new ModelImp(initLSTM, endNN);
		}
		
		val learner = learningMethod.instantiate(model);
		val nbIterations = 100000;
		for(iter <- 0 until nbIterations) {
			val (gradient, loss) = model.instanceToGrad(sequenceLength);
			learner.updateModel(iter, gradient);
			Console.println("loss at iteration " + iter + ": " + loss);
		}
	}
	
	def backwardPass(l: List[LearningLSTM], dGates: LSTMGatesParam): LSTMParam = {
		@tailrec
		def loop(lLoop: List[LearningLSTM]): LSTMParam = lLoop match {
			case h :: t => h match {
				case node: NodeLLSTM => {
					val (dParam, _) = node.gradient;
					
					dGates.addTo(dParam);
					
					loop(t);
				}
				
				case leaf: LeafLLSTM => {
					assert(t == Nil);
					
					dGates.toLSTMParam(leaf.dCellState, leaf.dOutput);
				}
			}
			
			case Nil => ???
		}
		
		loop(l);
	}
}
