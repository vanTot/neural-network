// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package examples.lstm;

import annotation.tailrec;
import collection._;

import neuralNetwork._;
import ParameterBrickConversions._;
import lstm._;

object Shakespeare {
	// The LSTM learns to produce lines from Hamlet
	def run: Unit = {
		//val learningMethod = learning.NormalLearning(0.01);
		val learningMethod = learning.MomentumLearning(0.001, 0.9);
		//val learningMethod = learning.Adagrad(0.1);
		
		val regularizer = learning.ClockRegularizer(100, learning.L2(0.0001));
		//val regularizer = learning.ClockRegularizer(0, learning.L2(0.001));
		
		val memorySize = 16;
		val vectorSize = 8;
		
		// Example of a line: "O, farewell, honest soldier:/Who hath relieved you?_"
		val endOfLine = '_'; // Special end-of-line character
		val breakLine = '/'; // Break the line
		
		// Load the lines
		val lines: Array[String] = {
			val tmp = mutable.ArrayBuffer[String]();
			
			val file = io.Source.fromResource("Hamlet.txt");
			var buffer = new mutable.StringBuilder();
			for(line <- file.getLines) {
				if(line.startsWith("    ")) {
					if(buffer.nonEmpty) buffer += breakLine;
					val l = line.drop(4);
					assert(!(l.contains(endOfLine) || l.contains(breakLine))); // Simply check that the special characters are not already used as normal characters
					buffer ++= l;
				}
				else if(buffer.nonEmpty) {
					buffer += endOfLine; // Add the end-of-line character
					
					tmp += buffer.toString;
					Console.println(buffer.toString);
					buffer.clear;
				}
			}
			
			tmp.toArray;
		}
		Console.println("nb lines: " + lines.length);
		io.StdIn.readLine;
		
		val model = {
			val (characters, dictionary) = { // Char <--> id conversion: characters(dictionary(c)) = c & dictionary(characters(i)) = i
				val tmpCharacters = new mutable.StringBuilder();
				val tmpDictionary = mutable.Map[Char, Int]();
				
				for(line <- lines; char <- line) if(!tmpDictionary.contains(char)) {
					tmpDictionary(char) = tmpCharacters.length;
					tmpCharacters += char;
				}
				
				(tmpCharacters.toString, tmpDictionary.toMap);
			}
			
			val lstm = LSTMFactory(memorySize, vectorSize);
			
			val cellStateInit = Array.fill(memorySize)(util.Random.nextGaussian);
			val outputInit = cellStateInit.map(x => math.tanh(x) * util.Random.nextDouble);
			
			val endNN = TransformNNFactory(Array("ML:SoftMax"), Array(characters.length, memorySize));
			
			val characterVectors = Array.fill(characters.length)(Array.fill(vectorSize)(util.Random.nextGaussian)); // The character-embeddings
			
			new Model(endOfLine, characters, dictionary, characterVectors, lstm, cellStateInit, outputInit, endNN);
		}
		
		val learner = learningMethod.instantiate(model);
		val nbEpoch = 5;
		for(epoch <- 0 until nbEpoch) {
			var sumLoss = 0.0;
			for(iter <- 0 until lines.length) {
				val line = lines(util.Random.nextInt(lines.length));
				
				val (gradient, loss) = model.instanceToGrad(line);
				sumLoss += loss;
				learner.updateModel(gradient);
				regularizer(model);
			}
			
			Console.println("loss at epoch " + epoch + ": " + (sumLoss / lines.length));
			Console.println("model parameters (max, avg): " + model.parameters.statsAvg);
		}
		
		model.generate;
		
		io.StdIn.readLine("File in which to write the network (empty string for none)? ") match {
			case "" => ();
			case fileName => {
				val writer = new java.io.BufferedWriter(new java.io.FileWriter(fileName));
				
				writer.append(model.customSerialize);
				
				writer.close;
			}
		}
	}
	
	// Definition of the model (contains all the parameters)
	class Model(val endOfLine: Char, val characters: String, val dictionary: Map[Char, Int], val characterVectors: Array[Array[Double]], val lstm: LearningLSTM, val cellStateInit: Array[Double], val outputInit: Array[Double], val endNN: TransformNN) extends neuralNetwork.Model {
		val parameters: CompoundParameter = CompoundParameter(characterVectors, endNN.param, lstm.param, cellStateInit, outputInit);
		
		// Compute the gradient and the loss for a given input line
		def instanceToGrad(line: String): (CompoundParameter, Double) = {
			val dEndNNParam = ParamList(endNN.emptyParam);
			
			var cellState = cellStateInit;
			var output = outputInit;
			
			var dFs: List[((Array[Double], Array[Double])) => (List[ParameterBrick], Array[Double])] = Nil;
			var dOutputs: List[Array[Double]] = Nil;
			var chars: List[Char] = Nil;
			
			var loss: Double = 0.0;
			
			var i = 0;
			while(i < line.length - 1) {
				val input = characterVectors(dictionary(line(i)));
				chars ::= line(i);
				
				val tmp = lstm.transformAndGradient(cellState ++ output ++ input);
				cellState = tmp._1._1;
				output = tmp._1._2;
				dFs ::= tmp._2;
				
				val (v, dF) = endNN.transformAndGradient(output);
				
				// endNN gradient
				val dOut = CrossEntropy.derivativeOp(dictionary(line(i + 1)), v);
				val (dEndNN, dTmp) = dF(dOut);
				
				//(dEndNNParam, dEndNN).zipped.map((p, q) => p.addTo(q));
				dEndNNParam.addTo(dEndNN);
				
				dOutputs ::= dTmp;
				
				loss += CrossEntropy(dictionary(line(i + 1)), v);
				i += 1;
			}
			
			val dVectors = mutable.Map[Int, Array[Double]]();
			val (dLSTMParam, dCellStateInit, dOutputInit) = backwardPass(dFs, dOutputs, chars, lstm, dVectors);
			
			val gradient = CompoundParameter(dVectors, dEndNNParam, dLSTMParam, dCellStateInit, dOutputInit);
			
			(gradient, loss);
		}
		
		// Auxiliary function for the gradient computation
		protected def backwardPass(dFs: List[((Array[Double], Array[Double])) => (List[ParameterBrick], Array[Double])], dOutputs: List[Array[Double]], chars: List[Char], lstm: LearningLSTM, dVectors: mutable.Map[Int, Array[Double]]): (List[ParameterBrick], Array[Double], Array[Double]) = {
			val dLSTMParam = lstm.emptyParam;
			
			@tailrec
			def loop(_dFs: List[((Array[Double], Array[Double])) => (List[ParameterBrick], Array[Double])], _dOutputs: List[Array[Double]], _chars: List[Char], _dCellState: Array[Double], _dOutput: Array[Double]): (List[ParameterBrick], Array[Double], Array[Double]) = _dFs match {
				case dF :: tF => {
					val dOutput :: tOutput = _dOutputs;
					val char :: tChars = _chars;
					
					Math.addTo(_dOutput, dOutput);
					
					val tmp = dF((_dCellState, _dOutput));
					
					(dLSTMParam, tmp._1).zipped.map((p, q) => p.addTo(q));
					
					var pos = 0;
					val nextDCellState = java.util.Arrays.copyOfRange(tmp._2, pos, (pos + lstm.memorySize));
					pos += lstm.memorySize;
					val nextDOutput = java.util.Arrays.copyOfRange(tmp._2, pos, (pos + lstm.memorySize));
					pos += lstm.memorySize;
					val dVector = java.util.Arrays.copyOfRange(tmp._2, pos, (pos + lstm.inputSize));
					pos += lstm.inputSize;
					assert(pos == tmp._2.length);
					
					val charId = dictionary(char);
					dVectors.get(charId) match {
						case Some(v) => Math.addTo(v, dVector);
						case None => dVectors(charId) = dVector;
					}
					
					loop(tF, tOutput, tChars, nextDCellState, nextDOutput);
				}
				
				case Nil => (dLSTMParam, _dCellState, _dOutput);
			}
			
			loop(dFs, dOutputs, chars, Array.ofDim[Double](lstm.memorySize), Array.ofDim[Double](lstm.memorySize));
		}
		
		// Predicts a line given its beginning (once the model is trained)
		def predict(seed: String): Iterator[Char] = {
			var cellState = cellStateInit;
			var output = outputInit;
			
			val seedLength = {
				var i = 0; // The seed is cut on the first unseen character
				var continue = true;
				while(i < seed.length && continue) dictionary.get(seed(i)) match {
					case Some(charId) => {
						val input = characterVectors(charId);
						val tmp = lstm.execute(cellState ++ output ++ input);
						
						cellState = tmp._1;
						output = tmp._2;
						
						i += 1;
					}
					
					case None => continue = false;
				}
				
				i;
			}
			
			var i = 0;
			var continue = true;
			new Iterator[Char] {
				def hasNext: Boolean = continue;
				
				def next: Char = if(i == seedLength) {
					val v = endNN.execute(output);
					val index = Math.selectFromProb(v);
					val char = characters(index);
					
					if(char == endOfLine) continue = false;
					else {
						val input = characterVectors(index);
						val tmp = lstm.execute(cellState ++ output ++ input);
						
						cellState = tmp._1;
						output = tmp._2;
					}
					
					char;
				}
				else {
					i += 1;
					seed(i - 1);
				}
			}
		}
		
		// Generates lines (once the model is trained)
		def generate: Unit = {
			var stop = false;
			while(!stop) {
				val seed = io.StdIn.readLine("Start at sentence: ");
				if(seed == "_") stop = true;
				else {
					val prediction = predict(seed);
					var i = 0;
					while(prediction.hasNext) {
						Console.print(prediction.next)
						i += 1;
						
						if(i == 500) { // To "break" a potential infinite loop of the LSTM
							io.StdIn.readLine;
							i = 0;
						}
					}
					Console.println;
				}
			}
		}
		
		// Serialization (useful to transfer to JavaScript)
		def customSerialize: mutable.StringBuilder = {
			val stringBuilder = new mutable.StringBuilder();
			customSerialize(stringBuilder);
			
			stringBuilder;
		}
		
		// Serialization (useful to transfer to JavaScript)
		def customSerialize(stringBuilder: mutable.StringBuilder): Unit = {
			stringBuilder ++= "examples.LSTM.Shakespeare.Model\n";
			stringBuilder += (endOfLine, '\n');
			stringBuilder ++= characters + '\n';
			
			// The dictionary can be infered from characters
			
			structureHandlers.Array2DHandler.customSerialize(characterVectors, stringBuilder);
			lstm.customSerialize(stringBuilder);
			structureHandlers.ArrayHandler.customSerialize(cellStateInit, stringBuilder);
			structureHandlers.ArrayHandler.customSerialize(outputInit, stringBuilder);
			endNN.customSerialize(stringBuilder);
		}
	}
	
	// (useful to transfer to JavaScript)
	object Model {
		// Inverse of the custom serialization
		def read(str: String): Model = read(str.split('\n'), 1)._1; // 1 because we know it is a Model (so we skip the corresponding line)
		def read(data: Array[String], i: Int): (Model, Int) = {
			var j = i;
			
			val endOfLine = data(j)(0);
			j += 1;
			
			val characters = data(j);
			j += 1;
			
			val dictionary = mutable.Map[Char, Int]() ++ characters.zipWithIndex;
			
			val characterVectors = {
				val tmp = structureHandlers.Array2DHandler.read(data, j);
				j = tmp._2;
				
				tmp._1;
			}
			
			val lstm = {
				val tmp = LearningLSTM.read(data, j + 1); // +1 because we know it is a LSTM (so we skip the corresponding line)
				j = tmp._2;
				
				tmp._1;
			}
			
			val cellStateInit = {
				val tmp = structureHandlers.ArrayHandler.read(data, j);
				j = tmp._2;
				
				tmp._1;
			}
			
			val outputInit = {
				val tmp = structureHandlers.ArrayHandler.read(data, j);
				j = tmp._2;
				
				tmp._1;
			}
			
			val endNN = {
				val tmp = TransformNN.read(data, j + 1); // +1 because we know it is a TransformNN (so we skip the corresponding line)
				j = tmp._2;
				
				tmp._1;
			}
			
			(new Model(endOfLine, characters, dictionary, characterVectors, lstm, cellStateInit, outputInit, endNN), j);
		}
	}
}
