// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neuralNetwork;

import annotation.tailrec;
import collection._;

class Array2DLayer(val p: Array2DLayerParam, val nonLinearity: Option[NonLinearity with Differentiable]) extends Layer {
	def apply(in: Array[Double]): Array[Double] = nonLinearity match {
		case Some(f) => f(Math.add(Math.matApply(p.m, in), p.t));
		case None => Math.add(Math.matApply(p.m, in), p.t);
	}
	
	def gradient(in: Array[Double]): (Array[Double], (Array[Double] => (Array2DLayerParam, Array[Double]))) = nonLinearity match { // (out[i], (dc/dout[i] => (dc/dparam, dc/din[i])))
		case Some(f) => {
			val u = Math.add(Math.matApply(p.m, in), p.t);
			
			val dF = (deltaV: Array[Double]) => {
				val tmp = f.derivative(u, deltaV); // dc/d(A.in+B)[i]
				
				val dP = Array2DLayerParam(Math.tensorProduct2D(tmp, in), tmp); // dc/dparam
				val dIn = Math.matApplyTransposed(p.m, tmp); // dc/din[i]
				
				(dP, dIn);
			};
			
			(f(u), dF);
			
			/*val u = Math.add(Math.matApply(p.m, in), p.t);
		
			val x = f.derivative(u);
		
			val dpCondensed = Math.tensorProduct2D(x, in);
			val dpF = ((deltaV: Array[Double]) => Array2DLayerParam(Math.matApplyLineWise(deltaV, dpCondensed), Math.matApply(deltaV, x)));
		
			val din = Math.matApplyLineWise(x, p.m);
		
			(f(u), dpF, din);*/
		}
		
		case None => protoGradient(in);
	}
	
	// Here the gradient does not take the non-linearity into account (as if f.derivative = 1)
	def protoGradient(in: Array[Double]): (Array[Double], (Array[Double] => (Array2DLayerParam, Array[Double]))) = { // (out[i], (dc/dout[i] => (dparam, dc/din[i])))
		val out = nonLinearity match {
			case Some(f) => f(Math.add(Math.matApply(p.m, in), p.t));
			case None => Math.add(Math.matApply(p.m, in), p.t);
		}
		
		val dF = (deltaV: Array[Double]) => {
			val dP = Array2DLayerParam(Math.tensorProduct2D(deltaV, in), deltaV); // dc/dparam
			val dIn = Math.matApplyTransposed(p.m, deltaV); // dc/din[i]
			
			(dP, dIn);
		};
		
		(out, dF);
		
		/*val dpF = ((deltaV: Array[Double]) => Array2DLayerParam(Math.tensorProduct2D(deltaV, in), deltaV));
		
		val din = p.m;
		
		(out, dpF, din);*/
	}
	
	// A copy of the layer with a copy of the parameter
	def deepCopy: Array2DLayer = new Array2DLayer(p.deepCopy, nonLinearity);
	
	// A copy of the layer with q (not a copy) as parameter
	def duplicate(q: ParameterBrick): Array2DLayer = q match {
		case q: Array2DLayerParam => new Array2DLayer(q, nonLinearity);
		case _ => {throw IncompatibleParameters; this;}
	}
	
	// Custom serialization
	override def customSerialize(stringBuilder: mutable.StringBuilder): Unit = {
		stringBuilder ++= "Array2DLayer\n";
		stringBuilder ++= p.height.toString + '\n';
		
		structureHandlers.Array2DHandler.customSerialize(p.m, stringBuilder);
		structureHandlers.ArrayHandler.customSerialize(p.t, stringBuilder);
		
		stringBuilder ++= Serializer(nonLinearity) + '\n';
	}
	
	override def toString: String = "Array2DLayer((" + p.height + ", " + p.width + "), " + nonLinearity + ")";
}

object Array2DLayer {
	def apply(m: Array[Array[Double]], t: Array[Double], nonLinearity: Option[NonLinearity with Differentiable] = None): Array2DLayer = new Array2DLayer(Array2DLayerParam(m, t), nonLinearity);
	
	def apply(m: Array[Array[Double]], t: Array[Double], f: NonLinearity with Differentiable): Array2DLayer = new Array2DLayer(Array2DLayerParam(m, t), Some(f));
	
	// Custom deserialization
	def read(data: Array[String], i: Int): (Array2DLayer, Int) = {
		var j = i;
		
		// read p
		val m = {
			val tmp = structureHandlers.Array2DHandler.read(data, j);
			j = tmp._2;
			
			tmp._1;
		}
		
		val t = {
			val tmp = structureHandlers.ArrayHandler.read(data, j);
			j = tmp._2;
			
			tmp._1;
		}
		
		val p = Array2DLayerParam(m, t);
		
		// read the non-linearity
		val nonLinearity = StringToFunction(data(j));
		j += 1;
		
		val layer = new Array2DLayer(p, nonLinearity);
		
		(layer, j);
	}
}

case class Array2DLayerParam(val m: Array[Array[Double]], val t: Array[Double]) extends LayerParam {
	def height: Int = m.length;
	def width: Int = m.head.length;
	
	def addTo(p: ParameterBrick): Unit = p match {
		case Array2DLayerParam(m2, t2) => {
			Math.addTo(m, m2);
			Math.addTo(t, t2);
		}
		
		case _ => throw IncompatibleParameterBricks("addTo", List(this, p));
	}
	
	def substractTo(p: ParameterBrick): Unit = p match {
		case Array2DLayerParam(m2, t2) => {
			Math.substractTo(m, m2);
			Math.substractTo(t, t2);
		}
		
		case _ => throw IncompatibleParameterBricks("substractTo", List(this, p));
	}
	
	def multiplyTo(x: Double): Unit = {
		Math.multiplyTo(m, x);
		Math.multiplyTo(t, x);
	}
	
	def multiply(x: Double): Array2DLayerParam = Array2DLayerParam(Math.multiply(m, x), Math.multiply(t, x));
	
	def equateTo(p: ParameterBrick): Unit = p match {
		case Array2DLayerParam(m2, t2) => {
			Math.equateTo(m, m2);
			Math.equateTo(t, t2);
		}
		
		case _ => throw IncompatibleParameterBricks("equateTo", List(this, p));
	}
	
	def deepCopy: Array2DLayerParam = Array2DLayerParam(m.map(_.clone), t.clone);
	
	def adaUpdate(hist: ParameterBrick, grad: ParameterBrick, adagrad: Double, fudgeFactor: Double): Unit = (hist, grad) match {
		case (Array2DLayerParam(mHist, tHist), Array2DLayerParam(mGrad, tGrad)) => {
			Math.adaUpdate(m, mHist, mGrad, adagrad, fudgeFactor);
			Math.adaUpdate(t, tHist, tGrad, adagrad, fudgeFactor);
		}
		
		case _ => throw IncompatibleParameterBricks("adaUpdate", List(this, hist, grad));
	}
	
	def norm1: Double = structureHandlers.Array2DHandler.norm1(m) + structureHandlers.ArrayHandler.norm1(t);
	
	def norm2Sq: Double = structureHandlers.Array2DHandler.norm2Sq(m) + structureHandlers.ArrayHandler.norm2Sq(t);
	//lazy val norm2Sq_memo: Double = norm2Sq;
	
	def empty: Array2DLayerParam = Array2DLayerParam(Array.ofDim[Double](height, width), Array.ofDim[Double](t.length));
	
	def nanScan: Boolean = structureHandlers.Array2DHandler.nanScan(m) && structureHandlers.ArrayHandler.nanScan(t);
	
	// sum and max (in abs) + number of params
	def stats: (Double, Double, Int) = {
		val statsM = structureHandlers.Array2DHandler.stats(m);
		val statsT = structureHandlers.ArrayHandler.stats(t);
		
		((statsM._1 + statsT._1), math.max(statsM._2, statsT._2), (statsM._3 + statsT._3));
	}
	
	// Get the value of the coeff at index id
	def getCoeff(id: Int): Double = {
		val i = id / width;
		val j = i % width;
		if(i < height) m(i)(j);
		else t(j)
	}
	
	// Add x to the coeff at index id
	def addCoeff(id: Int, x: Double): Unit = {
		val i = id / width;
		val j = i % width;
		if(i < height) m(i)(j) += x;
		else t(j) += x;
	}
	
	// Inplace
	def clip(f: (Double => Double)): Unit = {
		structureHandlers.Array2DHandler.clip(m, f);
		structureHandlers.ArrayHandler.clip(t, f);
	}
	
	override def toString: String = {
		val str = new StringBuilder;
		
		str ++= "Array2DLayerParam (" + height + ", " + width + ")";
		
		for(i <- 0 until height) {
			str += '\n';
			
			str ++= "Array(";
			for(j <- 0 until m(i).length) {
				if(j != 0) str ++= ", ";
				
				str ++= m(i)(j).toString;
			}
			
			str += ')';
		}
		
		str ++= "\n" + t.mkString(", ");
		
		str.toString;
	}
}
