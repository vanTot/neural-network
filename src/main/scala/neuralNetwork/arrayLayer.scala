// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neuralNetwork;

import annotation.tailrec;

/*class ArrayLayer(val p: ArrayLayerParam, val nonLinearity: Option[NonLinearity with Differentiable]) extends Layer {
	def apply(in: Array[Double]): Array[Double] = nonLinearity match {
		case Some(f) =>	Math.add(Math.multiply(p.m, in), p.t).map(f.apply);
		case None => Math.add(Math.multiply(p.m, in), p.t);
	}
	
	def gradient(in: Array[Double]): (Array[Double], Array[Double] => ArrayLayerParam, Array[Array[Double]]) = nonLinearity match { // (out[i], dc/dout[i] => dparam, dout[i]/din[j])
		case Some(f) => {
			val u = Math.add(Math.multiply(p.m, in), p.t);
		
			val x = u.map(f.derivative);
		
			val dpCondensed = Math.tensorProduct(x, in);
			val dpF = ((deltaV: Array[Double]) => ArrayLayerParam(Math.multiplyLineWise(deltaV, dpCondensed), Math.multiply(deltaV, x)));
		
			val din = Math.multiplyLineWise(x, p.m);
		
			(u.map(f.apply), dpF, din);
		}
		
		case None => protoGradient(in);
	}
	
	// gradient without taking the non-linearity into account (as if f.derivative = 1)
	def protoGradient(in: Array[Double]): (Array[Double], Array[Double] => ArrayLayerParam, Array[Array[Double]]) = { // (out[i], dc/dout[i] => dparam, dout[i]/din[j])
		val out = nonLinearity match {
			case Some(f) => Math.add(Math.multiply(p.m, in), p.t).map(f.apply);
			case None => Math.add(Math.multiply(p.m, in), p.t);
		}
		
		val dpF = ((deltaV: Array[Double]) => ArrayLayerParam(Math.tensorProduct(deltaV, in), deltaV));
		
		val din = p.m;
		
		(out, dpF, din);
	}
	
	override def toString: String = "ArrayLayer((" + p.width + "), " + nonLinearity + ")";
}

object ArrayLayer {
	def apply(m: Array[Array[Double]], t: Array[Double], nonLinearity: Option[NonLinearity with Differentiable] = None): ArrayLayer = new ArrayLayer(ArrayLayerParam(m, t), nonLinearity);
	
	def apply(m: Array[Array[Double]], t: Array[Double], f: NonLinearity with Differentiable): ArrayLayer = new ArrayLayer(ArrayLayerParam(m, t), Some(f));
}*/

case class ArrayLayerParam(val m: Array[Double], val t: Array[Double]) extends LayerParam {
	// t is an array of length 1, but we do not use a Double for mutability reasons
	
	def height: Int = 1;
	def width: Int = m.length;
	
	def addTo(p: ParameterBrick): Unit = p match {
		case ArrayLayerParam(m2, t2) => {
			Math.addTo(m, m2);
			Math.addTo(t, t2);
		}
		
		case _ => throw IncompatibleParameterBricks("addTo", List(this, p));
	}
	
	def substractTo(p: ParameterBrick): Unit = p match {
		case ArrayLayerParam(m2, t2) => {
			Math.substractTo(m, m2);
			Math.substractTo(t, t2);
		}
		
		case _ => throw IncompatibleParameterBricks("substractTo", List(this, p));
	}
	
	def multiplyTo(x: Double): Unit = {
		Math.multiplyTo(m, x);
		Math.multiplyTo(t, x);
	}
	
	def multiply(x: Double): ArrayLayerParam = ArrayLayerParam(Math.multiply(m, x), Math.multiply(t, x));
	
	def equateTo(p: ParameterBrick): Unit = p match {
		case ArrayLayerParam(m2, t2) => {
			Math.equateTo(m, m2);
			Math.equateTo(t, t2);
		}
		
		case _ => throw IncompatibleParameterBricks("equateTo", List(this, p));
	}
	
	def deepCopy: ArrayLayerParam = ArrayLayerParam(m.clone, t.clone);
	
	def adaUpdate(hist: ParameterBrick, grad: ParameterBrick, adagrad: Double, fudgeFactor: Double): Unit = (hist, grad) match {
		case (ArrayLayerParam(mHist, tHist), ArrayLayerParam(mGrad, tGrad)) => {
			Math.adaUpdate(m, mHist, mGrad, adagrad, fudgeFactor);
			Math.adaUpdate(t, tHist, tGrad, adagrad, fudgeFactor);
		}
		
		case _ => throw IncompatibleParameterBricks("adaUpdate", List(this, hist, grad));
	}
	
	def norm1: Double = structureHandlers.ArrayHandler.norm1(m) + structureHandlers.ArrayHandler.norm1(t);
	
	def norm2Sq: Double = structureHandlers.ArrayHandler.norm2Sq(m) + structureHandlers.ArrayHandler.norm2Sq(t);
	//lazy val norm2Sq_memo: Double = norm2Sq;
	
	def empty: ArrayLayerParam = ArrayLayerParam(Array.ofDim[Double](width), Array.ofDim[Double](1));
	
	def nanScan: Boolean = structureHandlers.ArrayHandler.nanScan(m) && structureHandlers.ArrayHandler.nanScan(t);
	
	// Get the value of the coeff at index id
	def getCoeff(id: Int): Double = {
		if(id < width) m(id);
		else t(id - width);
	}
	
	// Add x to the coeff at index id
	def addCoeff(id: Int, x: Double): Unit = {
		if(id < width) m(id) += x;
		else t(id - width) += x;
	}
	
	// sum and max (in abs) + number of params
	def stats: (Double, Double, Int) = {
		val statsM = structureHandlers.ArrayHandler.stats(m);
		val statsT = structureHandlers.ArrayHandler.stats(t);
		
		((statsM._1 + statsT._1), math.max(statsM._2, statsT._2), (statsM._3 + statsT._3));
	}
	
	// Inplace
	def clip(f: (Double => Double)): Unit = {
		structureHandlers.ArrayHandler.clip(m, f);
		structureHandlers.ArrayHandler.clip(t, f);
	}
	
	override def toString: String = {
		val str = new StringBuilder;
		
		str ++= "ArrayLayerParam (" + width + ")\n";
			
		str ++= "Array(";
		for(i <- 0 until width) {
			if(i != 0) str ++= ", ";
			
			str ++= m(i).toString;
		}
		
		str += ')';
		
		str ++= "\n" + t.mkString(", ");
		
		str.toString;
	}
}
