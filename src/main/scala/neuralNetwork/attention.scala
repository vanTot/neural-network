// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neuralNetwork;

import annotation.tailrec;
import collection._;

// TODO
// faire un type de ScoreNN trivial qui fait seulement un produit scalaire

class Attention(val scorer: ScoreNN, val contextSize: Int) extends TestableLearningSystem {
	def param: List[ParameterBrick] = scorer.param;
	
	val vectorSize: Int = (scorer.width - contextSize); // Size of the individual vectors

	protected val testWidth = {
		val sequenceLength = 5; // Testing is done on sequences of length 5
		
		(contextSize + (sequenceLength * vectorSize));
	}

	def attender(sequence: Array[Array[Double]]): LearningNeuralNetwork[Array[Double]] = {
		val reference = this;
		
		class NN(val sequence: Array[Array[Double]]) extends LearningNeuralNetwork[Array[Double]] {	
			def duplicateFeed(p: List[ParameterBrick]) = ???

			def execute(in: Array[Double]): Array[Double] = reference.attend(in, sequence);
			def executeArray(in: Array[Double]): Array[Double] = execute(in);

			val height: Int = reference.vectorSize;
			val width: Int = reference.contextSize;

			val param = (Array2D(sequence) :: reference.param);
			
			def gradient(in: Array[Double]): (Array[Double] => (List[ParameterBrick], Array[Double])) = outputAndGradient(in)._2;
			def outputAndGradient(in: Array[Double]): (Array[Double], (Array[Double] => (List[ParameterBrick], Array[Double]))) = {
				val tmp = reference.attendAndGradient(in, sequence);

				(
					tmp._1,
					(dOutput: Array[Double]) => {
						val tmp2 = tmp._2(dOutput);

						((Array2D(tmp2._2) :: tmp2._1), tmp2._3);
					}
				);
			}
		}

		new NN(sequence);
	}
	
	def decomposeInput(in: Array[Double]): (Array[Double], Array[Array[Double]]) = {
		var pos = 0;

		val context = java.util.Arrays.copyOfRange(in, pos, (pos + contextSize));
		pos += contextSize;

		val sequence = (0 until ((in.length - pos) / vectorSize)).toArray.map(i => {
			val tmp = (pos + (i * vectorSize));
			java.util.Arrays.copyOfRange(in, tmp, (tmp + vectorSize));
		});
		pos += (sequence.length * vectorSize);

		assert(pos == in.length);

		(context, sequence);
	}

	def attend(context: Array[Double], sequence: Array[Array[Double]]): Array[Double] = {
		val scores = sequence.map(v => scorer.execute(context ++ v));
		val weights = SoftMax(scores);

		Math.weightedSum(sequence, weights);
	}

	// Like attend, but with a single Array as input and a single Array as ouput
	def executeArray(in: Array[Double]): Array[Double] = {
		val (context, sequence) = decomposeInput(in);

		attend(context, sequence);
	}

	def attendAndGradient(context: Array[Double], sequence: Array[Array[Double]]): (
		Array[Double],
		(Array[Double] => (
			List[ParameterBrick],     // dScorer
			Array[Array[Double]], // dSequence
			Array[Double]         // dContext
		))
	) = {
		val (scores, gradL) = sequence.map(v => scorer.scoreAndGradient(context ++ v)).unzip;
		
		val weights: Array[Double] = SoftMax(scores);
		val output = Math.weightedSum(sequence, weights);

		(
			output,
			(dOutput: Array[Double]) => {
				val dScorer = ParamList(scorer.emptyParam);
				val dContext = Array.ofDim[Double](context.length);

				val dWeights = sequence.map(Math.dotProduct(dOutput, _));
				val dScores: Array[Double] = SoftMax.derivativeFromOutput(weights, dWeights);
				
				val dSequence = (weights, gradL, dScores).zipped.map((weight, dF, dScore) => {
					val dV = Math.multiply(dOutput, weight);

					val (dS, dIn) = dF(dScore);

					dScorer.addTo(dS);

					// Split dIn
					Math.addTo(dContext, 0, dIn);
					Math.addTo(dV, dIn, context.length);
					
					dV;
				});

				(dScorer.l, dSequence, dContext);
			}
		)
	}
	
	// Like attendAndGradient, but with a single Array as input and a single Array as output
	def outputAndGradient(in: Array[Double]): (
		Array[Double], 
		(Array[Double] => (
			List[ParameterBrick], // dScorer
			Array[Double]     // dContext ++ dSequence
		))
	) = {
		val (context, sequence) = decomposeInput(in);
		val tmp = attendAndGradient(context, sequence);
		
		(
			tmp._1,
			(dOutput: Array[Double]) => {
				val tmp2 = tmp._2(dOutput);

				(tmp2._1, (tmp2._3 +: tmp2._2).flatten);
			}
		);
	}
}

object Attention {
	def apply(contextSize: Int, vectorSize: Int): Attention = new Attention(ScoreNNFactory(Array("ML:None", "ML:ReLU"), Array((contextSize + vectorSize), (contextSize + vectorSize))), contextSize);
}
