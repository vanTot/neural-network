// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*package neuralNetwork;

import breeze.linalg.DenseMatrix;
import breeze.linalg.DenseVector;

trait BreezeNonLinearity {
	def apply(v: DenseVector[Double]): DenseVector[Double];
}

trait BreezeDifferentiable {
	// Computes dc/din[i] from in[i'] and dc/dOut[i''] (= dOut)
	def derivative(in: DenseVector[Double], dOut:DenseVector[Double]): DenseVector[Double];
}

object BreezeTanh extends BreezeNonLinearity with BreezeDifferentiable {
	def apply(v: DenseVector[Double]): DenseVector[Double] = v.map(apply(_));
	
	def apply(x: Double): Double = math.tanh(x);
	
	def derivative(in: DenseVector[Double], dOut:DenseVector[Double]): DenseVector[Double] = in.map(derivative(_)) * dOut;
	
	def derivative(x: Double): Double = {
		val y = apply(x);
		
		1.0 - math.pow(y, 2);
	}
	
	override def toString: String = "Tanh";
}

object BreezeMath {
	def tensorProduct(u: DenseVector[Double], v: DenseVector[Double]): DenseMatrix[Double] = u * v.t;
}

class BreezeMatLayer(val p: BreezeMatLayerParam, val nonLinearity: Option[BreezeNonLinearity with BreezeDifferentiable]) {
	def apply(in: DenseVector[Double]): DenseVector[Double] = nonLinearity match {
		case Some(f) => f((p.m * in) + p.t);
		case None => ((p.m * in) + p.t);
	}
	
	def gradient(in: DenseVector[Double]): (DenseVector[Double], (DenseVector[Double] => (BreezeMatLayerParam, DenseVector[Double]))) = nonLinearity match { // (out[i], (dc/dout[i] => (dc/dparam, dc/din[i])))
		case Some(f) => {
			val u = ((p.m * in) + p.t);
			
			val dF = (deltaV: DenseVector[Double]) => {
				val tmp = f.derivative(u, deltaV); // dc/d(A.in+B)[i]
				
				val dP = BreezeMatLayerParam(BreezeMath.tensorProduct(tmp, in), tmp); // dc/dparam
				val dIn = p.m.t * tmp; // dc/din[i]
				
				(dP, dIn);
			};
			
			(f(u), dF);
		}
		
		case None => protoGradient(in);
	}
	
	// Here the gradient does not take the non-linearity into account (as if f.derivative = 1)
	def protoGradient(in: DenseVector[Double]): (DenseVector[Double], (DenseVector[Double] => (BreezeMatLayerParam, DenseVector[Double]))) = { // (out[i], (dc/dout[i] => (dparam, dc/din[i])))
		val out = nonLinearity match {
			case Some(f) => f((p.m * in) + p.t);
			case None => ((p.m * in) + p.t);
		}
		
		val dF = (deltaV: DenseVector[Double]) => {
			val dP = BreezeMatLayerParam(BreezeMath.tensorProduct(deltaV, in), deltaV); // dc/dparam
			val dIn = p.m.t * deltaV; // dc/din[i]
			
			(dP, dIn);
		};
		
		(out, dF);
	}
	
	def duplicate(q: BreezeMatLayerParam): BreezeMatLayer = q match {
		case q: BreezeMatLayerParam => new BreezeMatLayer(q, nonLinearity);
		case _ => {throw IncompatibleParameters; this;}
	}
	
	override def toString: String = "BreezeMatLayer((" + p.height + ", " + p.width + "), " + nonLinearity + ")";
}

object BreezeMatLayer {
	def apply(m: DenseMatrix[Double], t: DenseVector[Double], nonLinearity: Option[BreezeNonLinearity with BreezeDifferentiable] = None): BreezeMatLayer = new BreezeMatLayer(BreezeMatLayerParam(m, t), nonLinearity);
	
	def apply(m: DenseMatrix[Double], t: DenseVector[Double], f: BreezeNonLinearity with BreezeDifferentiable): BreezeMatLayer = new BreezeMatLayer(BreezeMatLayerParam(m, t), Some(f));
}

case class BreezeMatLayerParam(val m: DenseMatrix[Double], val t: DenseVector[Double]) {
	def height: Int = m.rows;
	def width: Int = m.cols;
	
	def addTo(p: BreezeMatLayerParam): Boolean = p match {
		case BreezeMatLayerParam(m2, t2) => {
			m :+= m2;
			t :+= t2;
			true;
		}
		
		case _ => false;
	}
	
	def substractTo(p: BreezeMatLayerParam): Boolean = p match {
		case BreezeMatLayerParam(m2, t2) => {
			m :-= m2;
			t :-= t2;
			true;
		}
		
		case _ => false;
	}
	
	def multiplyTo(x: Double): Unit = {
		m :*= x;
		t :*= x;
	}
	
	def multiply(x: Double): BreezeMatLayerParam = BreezeMatLayerParam((m *:* x), (t *:* x));
	
	def equateTo(p: BreezeMatLayerParam): Boolean = ???/*p match {
		case BreezeMatLayerParam(m2, t2) => {
			Math.equateTo(m, m2);
			Math.equateTo(t, t2);
			true;
		}
		
		case _ => false;
	}*/
	
	def deepCopy: BreezeMatLayerParam = BreezeMatLayerParam(m.copy, t.copy);
	
	def adaUpdate(hist: LayerParam, grad: LayerParam, adagrad: Double, fudgeFactor: Double): Boolean = ???/*(hist, grad) match {
		case (BreezeMatLayerParam(mHist, tHist), BreezeMatLayerParam(mGrad, tGrad)) => {
			Math.adaUpdate(m, mHist, mGrad, adagrad, fudgeFactor);
			Math.adaUpdate(t, tHist, tGrad, adagrad, fudgeFactor);
			
			true;
		}
		
		case _ => false;
	}*/
	
	def norm1: Double = ???//structureHandlers.BreezeMatHandler.norm1(m) + structureHandlers.ArrayHandler.norm1(t);
	
	def norm2Sq: Double = ???//structureHandlers.BreezeMatHandler.norm2Sq(m) + structureHandlers.ArrayHandler.norm2Sq(t);
	//lazy val norm2Sq_memo: Double = norm2Sq;
	
	def empty: BreezeMatLayerParam = BreezeMatLayerParam(DenseMatrix.zeros[Double](height, width), DenseVector.zeros[Double](t.length));
	
	def nanScan: Boolean = ???//structureHandlers.BreezeMatHandler.nanScan(m) && structureHandlers.ArrayHandler.nanScan(t);
	
	// sum and max (in abs) + number of params
	def stats: (Double, Double, Int) = ???/*{
		val statsM = structureHandlers.BreezeMatHandler.stats(m);
		val statsT = structureHandlers.ArrayHandler.stats(t);
		
		((statsM._1 + statsT._1), math.max(statsM._2, statsT._2), (statsM._3 + statsT._3));
	}*/
	
	// Inplace
	def clip(f: (Double => Double)): Unit = ???/*{
		structureHandlers.BreezeMatHandler.clip(m, f);
		structureHandlers.ArrayHandler.clip(t, f);
	}*/
	
	override def toString: String = ???/*{
		val str = new StringBuilder;
		
		str ++= "BreezeMatLayerParam (" + height + ", " + width + ")";
		
		for(i <- 0 until height) {
			str += '\n';
			
			str ++= "Array(";
			for(j <- 0 until m(i).length) {
				if(j != 0) str ++= ", ";
				
				str ++= m(i)(j).toString;
			}
			
			str += ')';
		}
		
		str ++= "\n" + t.mkString(", ");
		
		str.toString;
	}*/
}*/
