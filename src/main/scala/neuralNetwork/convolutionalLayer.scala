// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neuralNetwork;

import annotation.tailrec;

// pixelSize is the number of doubles per pixel in the input
// inputX and inputY are respectively the width and height (in pixels) of the input images
// The filters should all have the same width (input size, filtersXSize * filtersYSize * pixelSize) and have a height of 1 (output size)
class ConvolutionalLayer(val filters: Array[Layer], val filtersXSize: Int, val filtersYSize: Int, val pixelSize: Int, val inputX: Int, val inputY: Int) extends Layer {
	val nbFilters = filters.length;
	
	val nbPassesX = (inputX - filtersXSize + 1);
	val nbPassesY = (inputY - filtersYSize + 1);
	
	val p = {
		val height = nbPassesX * nbPassesY * nbFilters;
		val width = inputX * inputY * pixelSize;
		
		ConvolutionalLayerParam(filters.map(_.p), height, width);
	}
	
	def apply(in: Array[Double]): Array[Double] = {
		val output = Array.ofDim[Double](nbPassesX * nbPassesY * nbFilters);
		
		// TODO A more efficient way to do that would be to have the outer loop for the X axis instead of the Y axis, so that most subIn could be build more rapidly from the previous one (cf MaxPoolingConvolutionalLayer)
		var i = 0; // Y axis (image)
		while(i < nbPassesY) {
			var j = 0; // X axis (image)
			while(j < nbPassesX) {
				val subIn = Array.ofDim[Double](filtersXSize * filtersYSize * pixelSize);
				
				{var y = 0; // Y axis (filter)
				while(y < filtersYSize) {
					var offset = ((y + i) * inputX + j) * pixelSize; // Offset in in
					val offset2 = (y * filtersXSize * pixelSize); // Offset in subIn
					
					var k = 0;
					while(k < (filtersXSize * pixelSize)) {
						subIn(offset2 + k) = in(offset + k);
						subIn(k) = in(offset + k);
						
						k += 1;
					}
					
					y += 1;
				}}
				
				var offset = ((i * nbPassesX) + j) * nbFilters; // Offset in output
				var k = 0;
				while(k < nbFilters) {
					output(offset + k) = filters(k)(subIn).head;
					
					k += 1;
				}
				
				j += 1;
			}
			
			i += 1;
		}
		
		output;
	}
	
	def gradient(in: Array[Double]): (Array[Double], (Array[Double] => (ConvolutionalLayerParam, Array[Double]))) = {
		val output = Array.ofDim[Double](nbPassesX * nbPassesY * nbFilters);
		
		val gradFs = Array.ofDim[(Array[Double] => (LayerParam, Array[Double]))](nbPassesX * nbPassesY * nbFilters);
		
		// TODO A more efficient way to do that would be to have the outer loop for the X axis instead of the Y axis, so that most subIn could be build more rapidly from the previous one (cf MaxPoolingConvolutionalLayer)
		{var i = 0; // Y axis (image)
		while(i < nbPassesY) {
			var j = 0; // X axis (image)
			while(j < nbPassesX) {
				val subIn = Array.ofDim[Double](filtersXSize * filtersYSize * pixelSize);
				
				{var y = 0; // Y axis (filter)
				while(y < filtersYSize) {
					var offset = (((y + i) * inputX) + j) * pixelSize; // Offset in in
					val offset2 = (y * filtersXSize * pixelSize); // Offset in subIn
					
					var k = 0;
					while(k < (filtersXSize * pixelSize)) {
						subIn(offset2 + k) = in(offset + k);
						
						k += 1;
					}
					
					y += 1;
				}}
				
				var offset = ((i * nbPassesX) + j) * nbFilters; // Offset in output
				//var subGrads = Array.ofDim[(Array[Double] => (LayerParam, Array[Double]))](nbFilters);
				var k = 0;
				while(k < nbFilters) {
					val tmp = filters(k).gradient(subIn);
					
					output(offset + k) = tmp._1.head;
					//subGrads(k) = tmp._2;
					gradFs(offset + k) = tmp._2;
					
					k += 1;
				}
				
				//gradFs((i * nbPassesX) + j) = subGrads;
				
				j += 1;
			}
			
			i += 1;
		}}
		
		val dF = ((deltaV: Array[Double]) => {
			val gradLayers = filters.map(_.p.empty);
			val dIn = Array.ofDim[Double](inputX * inputY * pixelSize);
			
			var i = 0; // Y axis (image)
			while(i < nbPassesY) {
				var j = 0;
				while(j < nbPassesX){ // X axis (image)
					val offset = ((i * nbPassesX) + j) * nbFilters; // Offset in dOut
					
					var k = 0;
					while(k < nbFilters) {
						val tmp = gradFs(offset + k)(Array(deltaV(offset + k)));
						
						gradLayers(k).addTo(tmp._1);
						val dSubIn = tmp._2;
						
						// Redistribution of dSubIn in dIn
						{var y = 0; // Y axis (filter)
						while(y < filtersYSize) {
							val offset2 = ((y + i) * inputX + j) * pixelSize; // Offset in dIn
							val offset3 = y * (filtersXSize * pixelSize); // Offset in dSubIn
							
							var l = 0;
							while(l < (filtersXSize * pixelSize)) {
								dIn(offset2 + l) = dSubIn(offset3 + l);
								
								l += 1;
							}
							
							y += 1;
						}}
						
						k += 1;
					}
					
					
					j += 1;
				}
				
				
				i += 1;
			}
			
			(ConvolutionalLayerParam(gradLayers, p.height, p.width), dIn);
		});
		
		(output, dF);
	}
	
	// Here the gradient does not take the non-linearity into account (as if f.derivative = 1)
	def protoGradient(in: Array[Double]): (Array[Double], (Array[Double] => (LayerParam, Array[Double]))) = gradient(in); // (out[i], (dc/dout[i] => (dparam, dc/din[i])))
	
	// A copy of the layer with a copy of the parameter
	def deepCopy: ConvolutionalLayer = new ConvolutionalLayer(filters.map(_.deepCopy), filtersXSize, filtersYSize, pixelSize, inputX, inputY);
	
	// A copy of the layer with q (not a copy) as LayerParam
	def duplicate(q: ParameterBrick): ConvolutionalLayer = q match {
		case ConvolutionalLayerParam(layers, _, _) => new ConvolutionalLayer((filters, layers).zipped.map((x, y) => x.duplicate(y)), filtersXSize, filtersYSize, pixelSize, inputX, inputY);
		case _ => {throw IncompatibleParameters; this;}
	}
}

case class ConvolutionalLayerParam(val layers: Array[LayerParam], val height: Int, val width: Int) extends LayerParam {
	def addTo(p: ParameterBrick): Unit = p match {
		case ConvolutionalLayerParam(layers2, _, _) => (layers, layers2).zipped.foreach((x, y) => x.addTo(y));
		case _ => throw IncompatibleParameterBricks("addTo", List(this, p));
	}
	
	def substractTo(p: ParameterBrick): Unit = p match {
		case ConvolutionalLayerParam(layers2, _, _) => (layers, layers2).zipped.foreach((x, y) => x.substractTo(y));
		case _ => throw IncompatibleParameterBricks("substractTo", List(this, p));
	}
	
	def multiplyTo(x: Double): Unit = {
		layers.foreach(_.multiplyTo(x));
	}
	
	def multiply(x: Double): ConvolutionalLayerParam = ConvolutionalLayerParam(layers.map(_.multiply(x)), height, width);
	
	def equateTo(p: ParameterBrick): Unit = p match {
		case ConvolutionalLayerParam(layers2, _, _) => (layers, layers2).zipped.foreach((x, y) => x.equateTo(y));
		case _ => throw IncompatibleParameterBricks("equateTo", List(this, p));
	}
	
	def deepCopy: ConvolutionalLayerParam = ConvolutionalLayerParam(layers.map(_.deepCopy), height, width);
	
	def adaUpdate(hist: ParameterBrick, grad: ParameterBrick, adagrad: Double, fudgeFactor: Double): Unit = (hist, grad) match {
		case (ConvolutionalLayerParam(layersHist, _, _), ConvolutionalLayerParam(layersGrad, _, _)) => (layers, layersHist, layersGrad).zipped.foreach((l, lHist, lGrad) => l.adaUpdate(lHist, lGrad, adagrad, fudgeFactor));
		case _ => throw IncompatibleParameterBricks("adaUpdate", List(this, hist, grad));
	}
	
	def norm1: Double = layers.map(_.norm1).sum;
	
	def norm2Sq: Double = layers.map(_.norm2Sq).sum;
	//lazy val norm2Sq_memo: Double = norm2Sq;
	
	def empty: ConvolutionalLayerParam = ConvolutionalLayerParam(layers.map(_.empty), height, width);
	
	def nanScan: Boolean = layers.map(_.nanScan).reduce(_ & _);
	
	// sum and max (in abs) + number of params
	def stats: (Double, Double, Int) = {
		val tmp = layers.map(_.stats).unzip3;
		
		val sum = tmp._1.sum;
		val max = tmp._2.max;
		val nb = tmp._3.sum;
		
		(sum, max, nb);
	}
	
	// Get the value of the coeff at index id
	def getCoeff(id: Int): Double = {
		@tailrec
		def loop(count: Int, i: Int): Double = {
			val layerP = layers(i);
			val tmp = layerP.nbParams;
			if(tmp > count) layerP.getCoeff(count);
			else loop((count - tmp), (i + 1));
		}
		
		loop(id, 0);
	}
	
	// Add x to the coeff at index id
	def addCoeff(id: Int, x: Double): Unit = {
		@tailrec
		def loop(count: Int, i: Int): Unit = {
			val layerP = layers(i);
			val tmp = layerP.nbParams;
			if(tmp > count) layerP.addCoeff(count, x);
			else loop((count - tmp), (i + 1));
		}
		
		loop(id, 0);
	}
	
	// Inplace
	def clip(f: (Double => Double)): Unit = {
		layers.foreach(_.clip(f));
	}
}
