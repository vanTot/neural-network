// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//package neuralNetwork;

//class DerivationLayer(val layers: List[Layer]) extends Layer {
//	val p = DerivationLayerParam(layers.map(_.p));
//	
//	def apply(in: Array[Double]): Array[Double] = {
//		val v = layers.head.apply(in);
//		
//		var tmp = layers.tail;
//		while(tmp.nonEmpty) {
//			Math.addTo(v, tmp.head.apply(in));
//			
//			tmp = tmp.tail;
//		}
//		
//		v;
//	}
//	
//	def gradient(in: Array[Double]): (Array[Double], Array[Double] => DerivationLayerParam, Array[Array[Double]]) = { // (out[i], dc/dout[i] => dparam, dout[i]/din[j])
//		val (v, dpFHead, din) = layers.head.gradient(in);
//		
//		var dpFPartial = List(dpFHead);
//		
//		{var tmp = layers.tail;
//		while(tmp.nonEmpty) {
//			val (vTmp, dpFTmp, dinTmp) = tmp.head.gradient(in);
//			
//			Math.addTo(v, vTmp);
//			dpFPartial = dpFTmp :: dpFPartial;
//			Math.addTo(din, dinTmp);
//			
//			tmp = tmp.tail;
//		}}
//		
//		val dpF = ((deltaV: Array[Double]) => {
//			var params = List(dpFPartial.head(deltaV));
//			
//			var tmp = dpFPartial.tail;
//			while(tmp.nonEmpty) {
//				params = tmp.head(deltaV) :: params;
//				
//				tmp = tmp.tail;
//			}
//			
//			DerivationLayerParam(params);
//		});
//		
//		(v, dpF, din);
//	}
//	
//	// gradient without taking the non-linearity into account (as if f.derivative = 1)
//	def protoGradient(in: Array[Double]): (Array[Double], Array[Double] => DerivationLayerParam, Array[Array[Double]]) = { // (out[i], dc/dout[i] => dparam, dout[i]/din[j])
//		val (v, dpFHead, din) = layers.head.gradient(in);
//		
//		var dpFPartial = List(dpFHead);
//		
//		{var tmp = layers.tail;
//		while(tmp.nonEmpty) {
//			val (vTmp, dpFTmp, dinTmp) = tmp.head.protoGradient(in);
//			
//			Math.addTo(v, vTmp);
//			dpFPartial = dpFTmp :: dpFPartial;
//			Math.addTo(din, dinTmp);
//			
//			tmp = tmp.tail;
//		}}
//		
//		val dpF = ((deltaV: Array[Double]) => {
//			var params = List(dpFPartial.head(deltaV));
//			
//			var tmp = dpFPartial.tail;
//			while(tmp.nonEmpty) {
//				params = tmp.head(deltaV) :: params;
//				
//				tmp = tmp.tail;
//			}
//			
//			DerivationLayerParam(params);
//		});
//		
//		(v, dpF, din);
//	}
//}

//case class DerivationLayerParam(val params: List[LayerParam]) extends LayerParam {
//	def width = params.head.width;
//	
//	def addTo(p: LayerParam): Boolean = p match {
//		case DerivationLayerParam(l) => (params, l).zipped.map((p, q) => p.addTo(q)).reduce(_ & _);
//		
//		case _ => false;
//	}
//	
//	def substractTo(p: LayerParam): Boolean = p match {
//		case DerivationLayerParam(l) => (params, l).zipped.map((p, q) => p.substractTo(q)).reduce(_ & _);
//		
//		case _ => false;
//	}
//	
//	def multiplyTo(x: Double): Unit = params.foreach { _.multiplyTo(x) };
//	
//	def multiply(x: Double): DerivationLayerParam = DerivationLayerParam(params.map(_.multiply(x)));
//	
//	def adaUpdate(hist: LayerParam, grad: LayerParam, adagrad: Double, fudgeFactor: Double): Boolean = (hist, grad) match {
//		case (DerivationLayerParam(mHist), DerivationLayerParam(mGrad)) => (params, mHist, mGrad).zipped.map((p, h, g) => p.adaUpdate(h, g, adagrad, fudgeFactor)).reduce(_ & _);
//		
//		case _ => false;
//	}
//	
//	def norm1: Double = params.map(_.norm1).reduce(_ + _);
//	
//	def norm2Sq: Double = params.map(_.norm2Sq).reduce(_ + _);
//	
//	def empty: DerivationLayerParam = DerivationLayerParam(params.map(_.empty));
//	
//	def nanScan: Boolean = params.map(_.nanScan).reduce(_ & _);
//	
//	// sum and max (in abs) + number of params
//	def stats: (Double, Double, Int) = {
//		var sum = 0.0;
//		var max = 0.0;
//		var nb = 0;
//		
//		for(param <- params) {
//			val tmp = param.stats;
//			
//			sum += tmp._1;
//			if(tmp._2 > max) max = tmp._2;
//			nb += tmp._3;
//		}
//		
//		(sum, max, nb);
//	}
//	
//	// Inplace
//	def clip(f: (Double => Double)): Unit = {
//		for(p <- params) p.clip(f);
//	}
//	
//	override def toString: String = {
//		"DerivationLayerParam (" + params.length + ")\n" + params.map(_.toString).mkString("\n");
//	}
//}
