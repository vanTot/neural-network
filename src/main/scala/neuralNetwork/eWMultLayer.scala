// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neuralNetwork;

import annotation.tailrec;

// Element-wise multiplicative layer
class EWMultLayer(val left: Layer, val right: Layer, val nonLinearity: Option[NonLinearity with Differentiable]) extends Layer {
	val p = EWMultLayerParam(left.p, right.p);
	
	def apply(in: Array[Double]): Array[Double] = nonLinearity match {
		case Some(f) => f(Math.multiply(left.apply(in), right.apply(in)));
		case None => Math.multiply(left.apply(in), right.apply(in));
	}
	
	def gradient(in: Array[Double]): (Array[Double], (Array[Double] => (EWMultLayerParam, Array[Double]))) = nonLinearity match { // (out[i],  (dc/dout[i] => (dparam, dc/din[i])))
		case Some(f) => {
			val (outLeft, dLeft) = left.gradient(in);
			val (outRight, dRight) = right.gradient(in);
			
			val u = Math.multiply(outLeft, outRight);
			
			val dF = ((deltaV: Array[Double]) => {
				val tmp = f.derivative(u, deltaV);
				
				val doutLeft = Math.multiply(tmp, outRight); // dc/doutLeft
				val doutRight = Math.multiply(tmp, outLeft); // dc/doutRight
				
				val (dParamLeft, dInLeft) = dLeft(doutLeft);
				val (dParamRight, dInRight) = dRight(doutRight);
				
				val dParam = EWMultLayerParam(dParamLeft, dParamRight);
				
				val dIn = Math.add(dInLeft, dInRight);
				
				(dParam, dIn);
			});
			
			(f(u), dF);
			
			/*val (yLeft, gLeft, dinLeft) = left.gradient(in);
			val (yRight, gRight, dinRight) = right.gradient(in);
		
			val u = Math.multiply(yLeft, yRight);
			val x = f.derivative(u);
		
			val dyLeft = Math.multiply(x, yRight); // dout/dyLeft
			val dyRight = Math.multiply(x, yLeft); // dout/dyRight
		
			val dpF = ((deltaV: Array[Double]) => {
				val dLeft = Math.multiply(deltaV, dyLeft); // dc/dyLeft
				val dRight = Math.multiply(deltaV, dyRight); // dc/dyRight
			
				EWMultLayerParam(gLeft(dLeft), gRight(dRight));
			});
		
			val din = Math.add(Math.multiplyLineWise(dyLeft, dinLeft), Math.multiplyLineWise(dyRight, dinRight));
		
			(f(u), dpF, din);*/
		}
		
		case None => protoGradient(in);
	}
	
	// gradient without taking the non-linearity into account (as if f.derivative = 1)
	def protoGradient(in: Array[Double]): (Array[Double], (Array[Double] => (EWMultLayerParam, Array[Double]))) = { // (out[i], (dc/dout[i] => (dparam, dc/din[i])))
		val (outLeft, dLeft) = left.gradient(in);
		val (outRight, dRight) = right.gradient(in);
		
		val out = nonLinearity match {
			case Some(f) => f(Math.multiply(outLeft, outRight));
			case None => Math.multiply(outLeft, outRight);
		}
		
		val dF = ((deltaV: Array[Double]) => {
			val doutLeft = Math.multiply(deltaV, outRight); // dc/doutLeft
			val doutRight = Math.multiply(deltaV, outLeft); // dc/doutRight
			
			val (dParamLeft, dInLeft) = dLeft(doutLeft);
			val (dParamRight, dInRight) = dRight(doutRight);
			
			val dParam = EWMultLayerParam(dParamLeft, dParamRight);
			
			val dIn = Math.add(dInLeft, dInRight);
			
			(dParam, dIn);
		});
		
		(out, dF);
		
		/*val (yLeft, gLeft, dinLeft) = left.gradient(in);
		val (yRight, gRight, dinRight) = right.gradient(in);
		
		val out = nonLinearity match {
			case Some(f) => f(Math.multiply(yLeft, yRight));
			case None => Math.multiply(yLeft, yRight);
		}
		
		val dpF = ((deltaV: Array[Double]) => {
			val dLeft = Math.multiply(deltaV, yRight); // dc/dyLeft
			val dRight = Math.multiply(deltaV, yLeft); // dc/dyRight
			
			EWMultLayerParam(gLeft(dLeft), gRight(dRight));
		});
		
		val din = Math.add(Math.multiplyLineWise(yRight, dinLeft), Math.multiplyLineWise(yLeft, dinRight));
		
		(out, dpF, din);*/
	}
	
	// A copy of the layer with a copy of the parameter
	def deepCopy: EWMultLayer = new EWMultLayer(left.deepCopy, right.deepCopy, nonLinearity);
	
	// A copy of the layer with q (not a copy) as parameter
	def duplicate(q: ParameterBrick): EWMultLayer = q match {
		case q: EWMultLayerParam => new EWMultLayer(left.duplicate(q.left), right.duplicate(q.right), nonLinearity);
		case _ => {throw IncompatibleParameters; this;}
	}
	
	override def toString: String = "EWMultLayer(" + left + ", " + right + ", " + nonLinearity + ")";
}

object EWMultLayer {
	def apply(left: Layer, right: Layer, nonLinearity: Option[NonLinearity with Differentiable] = None): EWMultLayer = new EWMultLayer(left, right, nonLinearity);
	
	def apply(left: Layer, right: Layer, f: NonLinearity with Differentiable): EWMultLayer = new EWMultLayer(left, right, Some(f));
}

case class EWMultLayerParam(val left: LayerParam, val right: LayerParam) extends LayerParam {
	def height: Int = left.height;
	def width: Int = left.width;
	
	// Add p to this (inplace)
	def addTo(p: ParameterBrick): Unit = p match {
		case EWMultLayerParam(left2, right2) => {left.addTo(left2); right.addTo(right2);}
		case _ => throw IncompatibleParameterBricks("addTo", List(this, p));
	}
	
	// Substract p to this (inplace)
	def substractTo(p: ParameterBrick): Unit = p match {
		case EWMultLayerParam(left2, right2) => {left.substractTo(left2); right.substractTo(right2);}
		case _ => throw IncompatibleParameterBricks("substractTo", List(this, p));
	}
	
	// inplace
	def multiplyTo(x: Double): Unit = {
		left.multiplyTo(x);
		right.multiplyTo(x);
	}
	
	def multiply(x: Double): EWMultLayerParam = {
		EWMultLayerParam(left.multiply(x), right.multiply(x));
	}
	
	def equateTo(p: ParameterBrick): Unit = p match {
		case EWMultLayerParam(left2, right2) => {left.equateTo(left2); right.equateTo(right2);}
		case _ => throw IncompatibleParameterBricks("equateTo", List(this, p));
	}
	
	def deepCopy: EWMultLayerParam = EWMultLayerParam(left.deepCopy, right.deepCopy);
	
	def adaUpdate(hist: ParameterBrick, grad: ParameterBrick, adagrad: Double, fudgeFactor: Double): Unit = (hist, grad) match {
		case (EWMultLayerParam(leftHist, rightHist), EWMultLayerParam(leftGrad, rightGrad)) => {
			left.adaUpdate(leftHist, leftGrad, adagrad, fudgeFactor);
			right.adaUpdate(rightHist, rightGrad, adagrad, fudgeFactor);
		}
		
		case _ => throw IncompatibleParameterBricks("adaUpdate", List(this, hist, grad));
	}
	
	def norm1: Double = left.norm1 + right.norm1;
	def norm2Sq: Double = left.norm2Sq + right.norm2Sq;
	//lazy val norm2Sq_memo: Double = norm2Sq;
	
	// New empty param
	def empty: EWMultLayerParam = EWMultLayerParam(left.empty, right.empty);
	
	def nanScan: Boolean = left.nanScan && right.nanScan;
	
	// sum and max (in abs) + number of params
	def stats: (Double, Double, Int) = {
		val statsLeft = left.stats;
		val statsRight = right.stats;
		
		((statsLeft._1 + statsRight._1), math.max(statsLeft._2, statsRight._2), (statsLeft._3 + statsRight._3));
	}
	
	// Get the value of the coeff at index id
	def getCoeff(id: Int): Double = {
		if(id < left.nbParams) left.getCoeff(id);
		else right.getCoeff(id - left.nbParams);
	}
	
	// Add x to the coeff at index id
	def addCoeff(id: Int, x: Double): Unit = {
		if(id < left.nbParams) left.addCoeff(id, x);
		else right.addCoeff((id - left.nbParams), x);
	}
	
	// Inplace
	def clip(f: (Double => Double)): Unit = {
		left.clip(f);
		right.clip(f);
	}
}
