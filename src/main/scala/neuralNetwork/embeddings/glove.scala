// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neuralNetwork.embeddings;

import java.io.BufferedReader;
import java.io.FileReader;

import collection.mutable;

// GloVe: https://nlp.stanford.edu/projects/glove/

class GloVeDictionary(fileName: String) extends Dictionary {
	val dictionary: mutable.Map[String, Array[Double]] = {
		val reader: BufferedReader = new BufferedReader(new FileReader(fileName));
		
		val data = mutable.Map[String, Array[Double]]();
		
		var continue: Boolean = true;
		while(continue) {
			val line = reader.readLine;
			
			if(line != null) {
				val a = line.split(' ');
				
				val word = a(0);
				val vector = a.tail.map(_.toDouble);
				
				data(word) = vector;
			}
			else continue = false;
		}
		
		reader.close();
		
		data;
	}
	
	val vectorSize = dictionary.head._2.length;
	
	override def toString: String = "GloVeDictionary(" + vectorSize + ")";
}

object GloVe {
	def test(file: String): Unit = {
		val dictionary = new GloVeDictionary(file).dictionary;
		
		Console.println("dictionary size: " + dictionary.size);
		
		val iter = dictionary.iterator;
		while(iter.hasNext && io.StdIn.readLine("Wanna see an vector? ") != "n") {
			val (w, v) = iter.next;
			Console.println(w + ": " + v.mkString(", "));
		}
	}
}
