// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neuralNetwork.learning;

import neuralNetwork._;

import annotation.tailrec;
import collection._;

trait LearningInstance {
	def updateModel(t: Int, gradient: CompoundParameter): Unit;
	
	// Using iter as an interal clock
	def iter: Int;
	def iter_=(x: Int): Unit;
	def updateModel(gradient: CompoundParameter): Unit = {
		updateModel(iter, gradient);
		iter = iter + 1;
	}
}

trait AdaptableLearningInstance extends LearningInstance {
	def adapt(score: Double): Double;
}

trait LearningMethod {
	def instantiate(model: Model): LearningInstance;
}

class NormalLearning(val a: Double, val k: Double, val p: Double) extends LearningMethod {
	class Instance(val model: Model) extends LearningInstance {
		private def learningRate(t: Int): Double = a / math.pow((1 + k * t), p);
		
		def updateModel(t: Int, gradient: CompoundParameter): Unit = {
			gradient.multiplyTo(learningRate(t));
			
			model.parameters.addTo(gradient);
		}
		
		var iter = 0;
	}
	
	def instantiate(model: Model): Instance = new Instance(model);
	
	override def toString = "Normal(" + a + ", " + k + ", " + p + ")";
}

object NormalLearning {
	def apply(a: Double, k: Double, p: Double): NormalLearning = new NormalLearning(a, k, p);
	def apply(a: Double): NormalLearning = NormalLearning(a, 0, 0);
	
	def unapply(l: NormalLearning): Option[(Double, Double, Double)] = Some((l.a, l.k, l.p));
}

// Simple normal learning that is able to adapt its learning rate against instability with "adapt"
class AdaptableLearning(val a: Double) extends LearningMethod {
	class Instance(val model: Model, var rate: Double) extends AdaptableLearningInstance {
		private def learningRate(t: Int): Double = rate;
		
		private var prevScore: Double = 0.0;
		def adapt(score: Double): Double = {
			if(score < prevScore) rate = (rate / 4);
			else if(score > prevScore) rate = (rate * 2);
			
			prevScore = score;

			rate;
		}
		
		def updateModel(t: Int, gradient: CompoundParameter): Unit = {
			gradient.multiplyTo(learningRate(t));
			
			model.parameters.addTo(gradient);
		}
		
		var iter = 0;
	}
	
	def instantiate(model: Model): Instance = new Instance(model, a);
	
	override def toString = "Adaptable(" + a + ")";
}

object AdaptableLearning {
	def apply(a: Double): AdaptableLearning = new AdaptableLearning(a);
	
	def unapply(l: AdaptableLearning): Option[Double] = Some(l.a);
}

// Normal learning that is able to adapt its learning rate against instability with "adapt" using rewind capacities
class SuperAdaptableLearning(val a: Double, val thr1: Double, val thr2: Double) extends LearningMethod {
	class Instance(val model: Model, var rate: Double) extends AdaptableLearningInstance {
		private def learningRate(t: Int): Double = rate;
		
		private val prevParameters = model.parameters.deepCopy;
		private var prevScore: Double = 0.0;
		def adapt(score: Double): Double = {
			val rewind: Boolean = if(score > (prevScore + thr1)) false;
				else if(score > prevScore) {rate = (rate * 2); false;} // Increase learning rate
				else if(score == prevScore) false; // This is useful for the beginning, when the score stays at 0
				else if(score > (prevScore - thr2)) {rate = (rate / 4); false} // Decrease learning rate
				else {rate = (rate / 4); true;} // Rewind + decrease learning rate
			
			if(rewind) model.parameters.equateTo(prevParameters);
			else {
				prevParameters.equateTo(model.parameters);
				prevScore = score;
			}

			rate;
		}
		
		def updateModel(t: Int, gradient: CompoundParameter): Unit = {
			gradient.multiplyTo(learningRate(t));
			
			model.parameters.addTo(gradient);
		}
		
		var iter = 0;
	}
	
	def instantiate(model: Model): Instance = new Instance(model, a);
	
	override def toString = "SuperAdaptable(" + a + ", " + thr1 + ", " + thr2 + ")";
}

object SuperAdaptableLearning {
	def apply(a: Double, thr1: Double, thr2: Double): SuperAdaptableLearning = new SuperAdaptableLearning(a, thr1, thr2);
	
	def unapply(l: SuperAdaptableLearning): Option[(Double, Double, Double)] = Some((l.a, l.thr1, l.thr2));
}

// ASDG
/*case class AverageLearning(val a: Double, val k: Double, val p: Double) extends LearningMethod {
	class Instance(val model: Model, val sum: CompoundParameter) extends LearningInstance {
		private def learningRate(t: Int): Double = a / math.pow((1 + k * t), p);
		
		def updateModel(t: Int, gradient: CompoundParameter): Unit = {
			gradient.multiplyTo(learningRate(t));
			
			model.parameters.addTo(gradient);
			
			sum.addTo(model.parameters);
		}
		
		var iter = 0;
		
		def finalise(t: Int, model: Model): Model = model.duplicateWith(sum.multiply(1.0 / t));
	}
	
	def instantiate(model: Model): Instance = new Instance(model, model.parameters.empty);
}*/

class MomentumLearning(val a: Double, val k: Double, val p: Double, val m: Double) extends LearningMethod {
	class Instance(val model: Model, val momentum: CompoundParameter) extends LearningInstance {
		private def learningRate(t: Int): Double = a / math.pow((1 + k * t), p);
		
		def updateModel(t: Int, gradient: CompoundParameter): Unit = {
			gradient.multiplyTo(learningRate(t));
			momentum.multiplyTo(m);
			momentum.addTo(gradient);
			
			model.parameters.addTo(momentum);
		}
		
		var iter = 0;
	}
	
	def instantiate(model: Model): Instance = new Instance(model, model.parameters.empty);
	
	override def toString = "Momentum(" + a + ", " + k + ", " + p + ", " + m + ")";
}

object MomentumLearning {
	def apply(a: Double, k: Double, p: Double, m: Double): MomentumLearning = new MomentumLearning(a, k, p, m);
	def apply(a: Double, m: Double): MomentumLearning = MomentumLearning(a, 0, 0, m);
	
	def unapply(l: MomentumLearning): Option[(Double, Double, Double, Double)] = Some((l.a, l.k, l.p, l.m));
}

case class Adagrad(val a: Double) extends LearningMethod {
	val fudgeFactor = 0.00001;
	
	class Instance(val model: Model, val history: CompoundParameter) extends LearningInstance {
		def updateModel(t: Int, gradient: CompoundParameter): Unit = {
			model.parameters.adaUpdate(history, gradient, a, fudgeFactor);
		}
		
		var iter = 0;
	}
	
	def instantiate(model: Model): Instance = new Instance(model, model.parameters.empty);
}

sealed trait Regularization;
case class L2(x: Double) extends Regularization;

trait Regularizer {
	def isTime(t: Int): Boolean;
	def method: Regularization;
	
	def apply(t: Int, model: Model): Unit = if(isTime(t)) model.regularize(method);
	
	// Using iter as an internal clock
	def iter: Int;
	def iter_=(x: Int): Unit;
	def apply(model: Model): Unit = {
		apply(iter, model);
		iter = iter + 1;
	}

	def copy: Regularizer;
}

case class ClockRegularizer(val period: Int, val method: Regularization) extends Regularizer {
	def isTime(t: Int): Boolean = (period != 0) && ((t + 1) % period == 0);
	
	var iter = 0;

	def copy: ClockRegularizer = ClockRegularizer(period, method);
}
