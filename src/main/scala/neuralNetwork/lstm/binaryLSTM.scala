// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neuralNetwork.lstm;

import annotation.tailrec;

import neuralNetwork._;

// BinaryLSTM with a joint forget-memorize gate
// As in Bowman et al. 2016, "A Fast Unified Model for Parsing and Sentence Understanding":
// 	forgetGateLeft is fl
// 	forgetGateRight is fr
// 	memorizeGate is i
// 	candidate is g
// 	filter is o
// 	(in addition, we tanh the cellState before output)
@SerialVersionUID(3941150803677520723L)
trait BinaryLSTM extends NeuralNetwork[(Array[Double], Array[Double])] {
	def forgetGateLeftNN: NeuralNetwork[Array[Double]]; // Output should be between 0 and 1
	def forgetGateRightNN: NeuralNetwork[Array[Double]]; // Output should be between 0 and 1
	def memorizeGateNN: NeuralNetwork[Array[Double]]; // Output should be between 0 and 1
	def candidateNN: NeuralNetwork[Array[Double]]; // I don't think this one need a final non-linearity
	def filterNN: NeuralNetwork[Array[Double]]; // Output should be between 0 and 1
	
	val param: List[ParameterBrick] = List(forgetGateLeftNN.param, forgetGateRightNN.param, memorizeGateNN.param, candidateNN.param, filterNN.param).flatten;
	
	val memorySize = forgetGateLeftNN.height; // Size of the cellState
	val inputSize = forgetGateLeftNN.width - (2 * memorySize); // Size of the "input" (in the LSTM sense)
	
	val height: Int = 2 * memorySize; // Size of the output
	val width: Int = (4 * memorySize) + inputSize; // Size of the input
	
	final protected  def decomposeInput(in: Array[Double]): (Array[Double], Array[Double], Array[Double], Array[Double], Array[Double]) = {
		var pos = 0;
		val prevCellStateLeft = java.util.Arrays.copyOfRange(in, pos, (pos + memorySize));
		pos += memorySize;
		val prevOutputLeft = java.util.Arrays.copyOfRange(in, pos, (pos + memorySize));
		pos += memorySize;
		val prevCellStateRight = java.util.Arrays.copyOfRange(in, pos, (pos + memorySize));
		pos += memorySize;
		val prevOutputRight = java.util.Arrays.copyOfRange(in, pos, (pos + memorySize));
		pos += memorySize;
		val currentIn = java.util.Arrays.copyOfRange(in, pos, (pos + inputSize));
		pos += inputSize;
		
		assert(pos == in.length);
		
		(prevCellStateLeft, prevOutputLeft, prevCellStateRight, prevOutputRight, currentIn);
	}
	
	final protected def feed(prevCellStateLeft: Array[Double], prevOutputLeft: Array[Double], prevCellStateRight: Array[Double], prevOutputRight: Array[Double], currentIn: Array[Double]): (Array[Double], Array[Double], Array[Double], Array[Double], Array[Double], Array[Double], Array[Double], Array[Double], Array[Double]) = {
		val bigIn = prevOutputLeft ++ prevOutputRight ++ currentIn; // TODO System.arraycopy is faster than ++
		
		val forgetGateLeft = forgetGateLeftNN.execute(bigIn);
		val forgetGateRight = forgetGateRightNN.execute(bigIn);
		val memorizeGate: Array[Double] = memorizeGateNN.execute(bigIn);
		val candidate: Array[Double] = candidateNN.execute(bigIn);
		val filter: Array[Double] = filterNN.execute(bigIn);
		
		val newCellState: Array[Double] = new Array[Double](memorySize);
		val tanhNCS: Array[Double] = new Array[Double](memorySize); // Tanh of the N(ew)C(ell)S(tate)
		val newOutput: Array[Double] = new Array[Double](memorySize);
		
		var i = 0;
		while(i < memorySize) {
			val tmp: Double = forgetGateLeft(i) * prevCellStateLeft(i) + forgetGateRight(i) * prevCellStateRight(i) + memorizeGate(i) * candidate(i);
			val tmpTanh = math.tanh(tmp);
			
			newCellState(i) = tmp;
			tanhNCS(i) = tmpTanh;
			newOutput(i) = filter(i) * tmpTanh;
			
			i += 1;
		}
		
		(newCellState, newOutput, forgetGateLeft, forgetGateRight, memorizeGate, candidate, filter, tanhNCS, bigIn);
	}
	
	def execute(in: Array[Double]): (Array[Double], Array[Double]) = {
		val (prevCellStateLeft, prevOutputLeft, prevCellStateRight, prevOutputRight, currentIn) = decomposeInput(in);
		
		val tmp = feed(prevCellStateLeft, prevOutputLeft, prevCellStateRight, prevOutputRight, currentIn);
		
		(tmp._1, tmp._2);
	}
	
	def executeArray(in: Array[Double]): Array[Double] = {
		val (cellState, output) = execute(in);
		
		cellState ++ output;
	}
}

class LearningBinaryLSTM(
	val forgetGateLeftNN: TransformNN, // Output should be between 0 and 1
	val forgetGateRightNN: TransformNN, // Output should be between 0 and 1
	val memorizeGateNN: TransformNN, // Output should be between 0 and 1
	val candidateNN: TransformNN, // I don't think this one need a final non-linearity
	val filterNN: TransformNN // Output should be between 0 and 1
) extends BinaryLSTM with LearningNeuralNetwork[(Array[Double], Array[Double])] {
	def gradient(in: Array[Double]): (((Array[Double], Array[Double])) => (List[ParameterBrick], Array[Double])) = transformAndGradient(in)._2; // (List[dc/dparam], dc/din[i])
	
	def transformAndGradient(in: Array[Double]): ((Array[Double], Array[Double]), ((Array[Double], Array[Double])) => (List[ParameterBrick], Array[Double])) = { // (List[dc/dparam], dc/din[i])
		val (prevCellStateLeft, prevOutputLeft, prevCellStateRight, prevOutputRight, currentIn) = decomposeInput(in);
		
		val (newCellState, newOutput, forgetGateLeft, forgetGateRight, memorizeGate, candidate, filter, tanhNCS, bigIn) = feed(prevCellStateLeft, prevOutputLeft, prevCellStateRight, prevOutputRight, currentIn);
		
		(
			(newCellState, newOutput),
			((d: (Array[Double], Array[Double])) => { // d is dc/dvn[i]
				val (dCellState, dOutput) = d;
				val dInput = new Array[Double](width); // Gradient w.r.t. the whole input
				
				val dMemorizeGate = new Array[Double](memorySize);
				val dFilter = new Array[Double](memorySize);
				val dCandidate = new Array[Double](memorySize);
				val dForgetGateLeft = new Array[Double](memorySize);
				val dForgetGateRight = new Array[Double](memorySize);
				
				val tmp = new Array[Double](memorySize);
				{var i = 0;
				while(i < memorySize) {
					dFilter(i) = dOutput(i) * tanhNCS(i);
					
					tmp(i) = dCellState(i) + dOutput(i) * filter(i) * (1.0 - Math.square(tanhNCS(i)));
					
					dMemorizeGate(i) = tmp(i) * candidate(i);
					dCandidate(i) = tmp(i) * memorizeGate(i);
					dInput(i) = tmp(i) * forgetGateLeft(i);
					dForgetGateLeft(i) = tmp(i) * prevCellStateLeft(i);
					dInput(i + (2 * memorySize)) = tmp(i) * forgetGateRight(i);
					dForgetGateRight(i) = tmp(i) * prevCellStateRight(i);
					
					i += 1;
				}}
				
				val forgetGateLeftGrad = forgetGateLeftNN.gradient(bigIn)(dForgetGateLeft);
				val forgetGateRightGrad = forgetGateRightNN.gradient(bigIn)(dForgetGateRight);
				val memorizeGateGrad = memorizeGateNN.gradient(bigIn)(dMemorizeGate);
				val candidateGrad = candidateNN.gradient(bigIn)(dCandidate);
				val filterGrad = filterNN.gradient(bigIn)(dFilter);
				
				System.arraycopy(filterGrad._2, 0, dInput, memorySize, memorySize); // Copy of prevOutputLeft
				System.arraycopy(filterGrad._2, memorySize, dInput, (3 * memorySize), (memorySize + inputSize)); // Copy of prevOutputRight and currentIn
				
				{var i = 0; // filterGrad does not appear because it wath copied right away
				while(i < memorySize) { // Copy of prevOutputLeft
					dInput(i + memorySize) += forgetGateLeftGrad._2(i) + forgetGateRightGrad._2(i) + memorizeGateGrad._2(i) + candidateGrad._2(i);
					
					i += 1;
				}
				while(i < ((2 * memorySize) + inputSize)) { // Copy of prevOutputRight and currentIn
					dInput(i + (2 * memorySize)) += forgetGateLeftGrad._2(i) + forgetGateRightGrad._2(i) + memorizeGateGrad._2(i) + candidateGrad._2(i);
					
					i += 1;
				}}
				
				(List(forgetGateLeftGrad._1, forgetGateRightGrad._1, memorizeGateGrad._1, candidateGrad._1, filterGrad._1).flatten, dInput);
			})
		);
	}
	
	final def decomposeOutput(out: Array[Double]): (Array[Double], Array[Double]) = {
		var pos = 0;
		val cellState = java.util.Arrays.copyOfRange(out, pos, (pos + memorySize));
		pos += memorySize;
		val output = java.util.Arrays.copyOfRange(out, pos, (pos + memorySize));
		pos += memorySize;
		
		assert(pos == out.length);
		
		(cellState, output);
	}
	
	def outputAndGradient(in: Array[Double]): (Array[Double], Array[Double] => (List[ParameterBrick], Array[Double])) = { // (out, dc/dout => (List[dc/dparam], dc/din))
		val (out, dF) = transformAndGradient(in);
		
		(
			(out._1 ++ out._2),
			((d: Array[Double]) => dF(decomposeOutput(d)))
		);
	}
	
	// A copy of the network with p (not a copy) as parameters
	override def duplicate(p: List[ParameterBrick]): LearningBinaryLSTM = duplicateFeed(p) match {
		case (x, Nil) => x;
		case _ => {throw IncompatibleParameters; this;}
	}
	
	// Similar as duplicate, but also returns the unused LayerParam
	override def duplicateFeed(p: List[ParameterBrick]): (LearningBinaryLSTM, List[ParameterBrick]) = {
		val tmpFGL = forgetGateLeftNN.duplicateFeed(p);
		val tmpFGR = forgetGateRightNN.duplicateFeed(tmpFGL._2);
		val tmpMG = memorizeGateNN.duplicateFeed(tmpFGR._2);
		val tmpC = candidateNN.duplicateFeed(tmpMG._2);
		val tmpF = filterNN.duplicateFeed(tmpC._2);

		(new LearningBinaryLSTM(tmpFGL._1, tmpFGR._1, tmpMG._1, tmpC._1, tmpF._1), tmpF._2);
	}

	override def toString: String = "LearningBinaryLSTM(" + List(forgetGateLeftNN, forgetGateRightNN, memorizeGateNN, candidateNN, filterNN).mkString(", ") + ")";
}

object BinaryLSTMFactory {
	def apply(memorySize: Int, inSize: Int): LearningBinaryLSTM = {
		val forgetGateLeftNN = TransformNNFactory(Array("ML:Sigmoid"), Array(memorySize, ((2 * memorySize) + inSize)));
		val forgetGateRightNN = TransformNNFactory(Array("ML:Sigmoid"), Array(memorySize, ((2 * memorySize) + inSize)));
		val memorizeGateNN = TransformNNFactory(Array("ML:Sigmoid"), Array(memorySize, ((2 * memorySize) + inSize)));
		val candidateNN = TransformNNFactory(Array("ML:Tanh"), Array(memorySize, ((2 * memorySize) + inSize)));
		val filterNN = TransformNNFactory(Array("ML:Sigmoid"), Array(memorySize, ((2 * memorySize) + inSize)));
		
		new LearningBinaryLSTM(
			forgetGateLeftNN,
			forgetGateRightNN,
			memorizeGateNN,
			candidateNN,
			filterNN
		);
	}
}
