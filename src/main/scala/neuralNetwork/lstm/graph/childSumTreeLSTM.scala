// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neuralNetwork.lstm.graph;

import annotation.tailrec;

import neuralNetwork._;
import ParameterBrickConversions._;

/*trait ChildSumTreeLSTM {
	protected def forgetGateNN: NeuralNetwork[Array[Double]]; // Output should be between 0 and 1
	protected def memorizeGateNN: NeuralNetwork[Array[Double]]; // Output should be between 0 and 1
	protected def candidateNN: NeuralNetwork[Array[Double]]; // I don't think this one need a final non-linearity
	protected def filterNN: NeuralNetwork[Array[Double]]; // Output should be between 0 and 1
	
	def cellState: Array[Double];
	def output: Array[Double]; // Between -1 and 1 (because we use tanh)
	
	def update(in: Array[Double]): ChildSumTreeLSTM = update(Nil, in);
	def update(siblings: List[ChildSumTreeLSTM], in: Array[Double]): ChildSumTreeLSTM;
}*/

// Kind of abstract structure that needs to be instantiated with toLeaf to be used
@SerialVersionUID(3941150803677520723L)
class ChildSumTreeLSTMCore(
	val forgetGateNN: TransformNN, // Output should be between 0 and 1
	val memorizeGateNN: TransformNN, // Output should be between 0 and 1
	val candidateNN: TransformNN, // I don't think this one needs a final non-linearity
	val filterNN: TransformNN, // Output should be between 0 and 1
	
	val cellState: Array[Double],
	val output: Array[Double] // Between -1 and 1 (because we use tanh)
) extends java.io.Serializable {
	val gatesParam: ChildSumTreeLSTMGatesParam = new ChildSumTreeLSTMGatesParam(
		forgetGateNN.param, 
		memorizeGateNN.param, 
		candidateNN.param, 
		filterNN.param
	);
	
	val param: ChildSumTreeLSTMParam = new ChildSumTreeLSTMParam(
		gatesParam, 
		Array1D(cellState), 
		Array1D(output)
	);
	
	def emptyGatesParam: ChildSumTreeLSTMGatesParam = gatesParam.empty;
	def emptyParam: CompoundParameter = param.empty;
	
	def toLeaf: LeafLChildSumTreeLSTM = new LeafLChildSumTreeLSTMImp(
		forgetGateNN,
		memorizeGateNN,
		candidateNN,
		filterNN,
		cellState,
		output,
		new Array[Double](cellState.size), // dCellState
		new Array[Double](cellState.size) // dOutput
	);
	
	def adaUpdate(hist: ChildSumTreeLSTMParam, grad: ChildSumTreeLSTMParam, adagrad: Double, fudgeFactor: Double): Unit = param.adaUpdate(hist, grad, adagrad, fudgeFactor);
	
	/*def duplicate(p: ChildSumTreeLSTMParam): ChildSumTreeLSTMCore = new ChildSumTreeLSTMCore(
		forgetGateNN.duplicate(p.forgetGate),
		memorizeGateNN.duplicate(p.memorizeGate),
		candidateNN.duplicate(p.candidate),
		filterNN.duplicate(p.filter),
		
		p.cellState,
		p.output
	);*/
}

object ChildSumTreeLSTMCore {
	def apply(cellStateSize: Int, inSize: Int): ChildSumTreeLSTMCore = {
		val forgetGateNN = TransformNNFactory(Array("ML:Sigmoid"), Array(cellStateSize, (cellStateSize + inSize)));
		val memorizeGateNN = TransformNNFactory(Array("ML:Sigmoid"), Array(cellStateSize, (cellStateSize + inSize)));
		val candidateNN = TransformNNFactory(Array("ML:Tanh"), Array(cellStateSize, (cellStateSize + inSize)));
		val filterNN = TransformNNFactory(Array("ML:Sigmoid"), Array(cellStateSize, (cellStateSize + inSize)));
		
		val cellState = Array.fill(cellStateSize)(util.Random.nextGaussian);
		val output = cellState.map(x => math.tanh(x) * util.Random.nextDouble);
		
		new ChildSumTreeLSTMCore(
			forgetGateNN,
			memorizeGateNN,
			candidateNN,
			filterNN,
			cellState,
			output
		);
	}
}

@SerialVersionUID(3941150803677520723L)
class ChildSumTreeLSTMGatesParam(
	val forgetGate: ParamList,
	val memorizeGate: ParamList,
	val candidate: ParamList,
	val filter: ParamList
) extends CompoundParameter {
	val bricks = Array(forgetGate, memorizeGate, candidate, filter);
	
	def toLSTMParam(cellState: Array[Double], output: Array[Double]): ChildSumTreeLSTMParam = new ChildSumTreeLSTMParam(this, Array1D(cellState), Array1D(output));
	
	def deepCopy: ChildSumTreeLSTMGatesParam = new ChildSumTreeLSTMGatesParam(forgetGate.deepCopy, memorizeGate.deepCopy, candidate.deepCopy, filter.deepCopy)
	def empty: ChildSumTreeLSTMGatesParam = new ChildSumTreeLSTMGatesParam(forgetGate.empty, memorizeGate.empty, candidate.empty, filter.empty);
}

@SerialVersionUID(3941150803677520723L)
class ChildSumTreeLSTMParam(
	val gatesParam: ChildSumTreeLSTMGatesParam,
	val cellState: Array1D,
	val output: Array1D
) extends CompoundParameter {
	val bricks = Array(gatesParam, cellState, output);
	
	def deepCopy: ChildSumTreeLSTMParam = new ChildSumTreeLSTMParam(gatesParam.deepCopy, cellState.deepCopy, output.deepCopy)
	def empty: ChildSumTreeLSTMParam = new ChildSumTreeLSTMParam(gatesParam.empty, cellState.empty, output.empty)
}

// LSTM for backprop (build an LSTM graph)
//trait LearningChildSumTreeLSTM extends ChildSumTreeLSTM 
trait LearningChildSumTreeLSTM extends java.io.Serializable {
	def parents: List[LearningChildSumTreeLSTM];	// Parent LSTM which lead to this node (or null)
	
	protected def forgetGateNN: TransformNN; // Output should be between 0 and 1
	protected def memorizeGateNN: TransformNN; // Output should be between 0 and 1
	protected def candidateNN: TransformNN; // I don't think this one need a final non-linearity
	protected def filterNN: TransformNN; // Output should be between 0 and 1
	
	def cellState: Array[Double];
	def output: Array[Double]; // Between -1 and 1 (because we use tanh)
	
	def dCellState: Array[Double];	// Buffer for backprop
	def dOutput: Array[Double];	// Buffer for backprop
	
	protected def feed(children: List[LearningChildSumTreeLSTM], in: Array[Double]): (Array[Double], Array[Double], List[Array[Double]], Array[Double], Array[Double], Array[Double], Array[Double]) = { // (cellState, output, forgetGates, memorizeGate, candidate, filter, tanhNCS)
		val bigIn = Math.sum(children.map(_.output)) ++ in; // TODO System.arraycopy is faster than ++
		
		val forgetGates = children.map(child => forgetGateNN.execute(child.output ++ in));
		val memorizeGate: Array[Double] = memorizeGateNN.execute(bigIn); // sometimes noted "u"
		val candidate: Array[Double] = candidateNN.execute(bigIn); // sometimes called "input"
		val filter: Array[Double] = filterNN.execute(bigIn); // sometimes called "output"
		
		val newCellState: Array[Double] = new Array[Double](cellState.length); // sometimes called "memory cell"
		val tanhNCS: Array[Double] = new Array[Double](cellState.length); // Tanh of the N(ew)C(ell)S(tate)
		val newOutput: Array[Double] = new Array[Double](cellState.length); // sometimes called "hidden state"
		
		val remembered = Math.sum((children, forgetGates).zipped.map((child, forgetGate) => Math.multiply(forgetGate, child.cellState)));
		
		var i = 0;
		while(i < cellState.length) {
			val tmp: Double = remembered(i) + memorizeGate(i) * candidate(i);
			val tmpTanh = math.tanh(tmp);
			
			newCellState(i) = tmp;
			tanhNCS(i) = tmpTanh;
			newOutput(i) = filter(i) * tmpTanh;
			
			i += 1;
		}
		
		(newCellState, newOutput, forgetGates, memorizeGate, candidate, filter, tanhNCS);
	}
	
	def update(siblings: List[LearningChildSumTreeLSTM], in: Array[Double]): NodeLChildSumTreeLSTM = {
		val children = (this :: siblings);
		
		val (newCellState, newOutput, forgetGates, memorizeGate, candidate, filter, tanhNCS) = feed(children, in);
		
		new NodeLChildSumTreeLSTMImp(
			children,	// parents
			in, 
			forgetGateNN, 
			memorizeGateNN, 
			candidateNN, 
			filterNN, 
			newCellState, 
			newOutput,
			new Array[Double](newCellState.length),	// dCellState
			new Array[Double](newCellState.length),	// dOutput
			forgetGates, 
			memorizeGate, 
			candidate, 
			filter, 
			tanhNCS
		);
	}
}

trait LeafLChildSumTreeLSTM extends LearningChildSumTreeLSTM {
	def parents: List[LearningChildSumTreeLSTM] = null;
}

@SerialVersionUID(3941150803677520723L)
class LeafLChildSumTreeLSTMImp(
	protected val forgetGateNN: TransformNN,
	protected val memorizeGateNN: TransformNN,
	protected val candidateNN: TransformNN,
	protected val filterNN: TransformNN,
	
	val cellState: Array[Double], // Initial cellState
	val output: Array[Double],    // Initial output
	
	val dCellState: Array[Double], // Buffer for backprop
	val dOutput: Array[Double]     // Buffer for backprop
) extends LeafLChildSumTreeLSTM;

// Not really immutable
trait NodeLChildSumTreeLSTM extends LearningChildSumTreeLSTM {
	protected def in: Array[Double]; // Input vector which lead to this node
	
	protected def forgetGates: List[Array[Double]]; // Memoized vectors for backprop
	protected def memorizeGate: Array[Double];      // Memoized vector for backprop
	protected def candidate: Array[Double];         // Memoized vector for backprop
	protected def filter: Array[Double];            // Memoized vector for backprop
	protected def tanhNCS: Array[Double];           // Memoized vector for backprop
	
	// d is dc/dout[i]
	def gradient: (ChildSumTreeLSTMGatesParam, Array[Double]) = { // (dc/dparam, dc/din)
		val dMemorizeGate = new Array[Double](cellState.length);
		val dFilter = new Array[Double](cellState.length);
		val dCandidate = new Array[Double](cellState.length);
		
		val tmp = new Array[Double](cellState.length);
		{var i = 0;
		while(i < cellState.length) {
			dFilter(i) = dOutput(i) * tanhNCS(i);
			
			tmp(i) = dCellState(i) + dOutput(i) * filter(i) * (1.0 - (tanhNCS(i) * tanhNCS(i)));
			
			dMemorizeGate(i) = tmp(i) * candidate(i);
			dCandidate(i) = tmp(i) * memorizeGate(i);
			
			// Buffer are reinitialized
			//dOutput(i) = 0.0;
			//dCellState(i) = 0.0;
			
			i += 1;
		}}
		
		val (forgetGateGradParam, dIn) = {
			val parent = parents.head;
			val forgetGate = forgetGates.head;
			
			Math.addTo(parent.dCellState, Math.multiply(tmp, forgetGate));
			
			val dForgetGate = Math.multiply(tmp, parent.cellState);
			val forgetGateGrad = forgetGateNN.gradient((parent.output ++ in))(dForgetGate);
			
			Math.addTo(parent.dOutput, forgetGateGrad._2, 0);
			
			(forgetGateGrad._1, java.util.Arrays.copyOfRange(forgetGateGrad._2, cellState.length, forgetGateGrad._2.length));
		}
		
		(parents.tail, forgetGates.tail).zipped.foreach((parent, forgetGate) => {
			Math.addTo(parent.dCellState, Math.multiply(tmp, forgetGate));
			
			val dForgetGate = Math.multiply(tmp, parent.cellState);
			val forgetGateGrad = forgetGateNN.gradient((parent.output ++ in))(dForgetGate);
			
			Math.addTo(parent.dOutput, forgetGateGrad._2, 0);
			
			(forgetGateGradParam, forgetGateGrad._1).zipped.map((p, q) => p.addTo(q));
			Math.addTo(dIn, forgetGateGrad._2, cellState.length);
		});
		
		val bigIn = Math.sum(parents.map(_.output)) ++ in; // TODO System.arraycopy is faster than ++ // And also, the sum could have been memoized
		
		val memorizeGateGrad = memorizeGateNN.gradient(bigIn)(dMemorizeGate);
		val candidateGrad = candidateNN.gradient(bigIn)(dCandidate);
		val filterGrad = filterNN.gradient(bigIn)(dFilter);
		
		parents.foreach(parent => {
			Math.addTo(parent.dOutput, memorizeGateGrad._2, 0);
			Math.addTo(parent.dOutput, candidateGrad._2, 0);
			Math.addTo(parent.dOutput, filterGrad._2, 0);
			// Forget gate has already been done above
		});
		
		Math.addTo(dIn, memorizeGateGrad._2, cellState.length);
		Math.addTo(dIn, candidateGrad._2, cellState.length);
		Math.addTo(dIn, filterGrad._2, cellState.length);
		// Forget gate has already been done above
		
//		Console.println("in: " + in.mkString(", "));
//		Console.println("forgetGate: " + forgetGate.mkString(", "));
//		Console.println("candidate: " + candidate.mkString(", "));
//		Console.println("filter: " + filter.mkString(", "));
//		Console.println("tanhNCS: " + tanhNCS.mkString(", "));
//		Console.println("dFilter: " + dFilter.mkString(", "));
//		Console.println("dCandidate: " + dCandidate.mkString(", "));
//		Console.println("dForgetGate: " + dForgetGate.mkString(", "));
//		Console.println("filterGrad: " + filterGrad._2.mkString(", "));
//		Console.println("forgetGateGrad: " + forgetGateGrad._2.mkString(", "));
//		Console.println("candidateGrad: " + candidateGrad._2.mkString(", "));
		
		(new ChildSumTreeLSTMGatesParam(forgetGateGradParam, memorizeGateGrad._1, candidateGrad._1, filterGrad._1), dIn);
	}
}

@SerialVersionUID(3941150803677520723L)
class NodeLChildSumTreeLSTMImp(
	val parents: List[LearningChildSumTreeLSTM], // Parents LSTM which lead to this node
	protected val in: Array[Double],             // Input vector which lead to this node
	
	protected val forgetGateNN: TransformNN,
	protected val memorizeGateNN: TransformNN,
	protected val candidateNN: TransformNN,
	protected val filterNN: TransformNN,
	
	val cellState: Array[Double],
	val output: Array[Double],
	
	val dCellState: Array[Double], // Buffer for backprop
	val dOutput: Array[Double],    // Buffer for backprop
	
	protected val forgetGates: List[Array[Double]], // Memoized vectors for backprop
	protected val memorizeGate: Array[Double],      // Memoized vector for backprop
	protected val candidate: Array[Double],         // Memoized vector for backprop
	protected val filter: Array[Double],            // Memoized vector for backprop
	protected val tanhNCS: Array[Double]            // Memoized vector for backprop
) extends NodeLChildSumTreeLSTM;
