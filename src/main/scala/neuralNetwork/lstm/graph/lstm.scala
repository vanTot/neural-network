// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neuralNetwork.lstm.graph;

import annotation.tailrec;

import neuralNetwork._;
import ParameterBrickConversions._;

// LSTM with a joint forget-memorize gate
trait LSTM extends java.io.Serializable {
	protected def forgetGateNN: NeuralNetwork[Array[Double]]; // Output should be between 0 and 1
	protected def candidateNN: NeuralNetwork[Array[Double]]; // I don't think this one need a final non-linearity
	protected def filterNN: NeuralNetwork[Array[Double]]; // Output should be between 0 and 1
	
	def cellState: Array[Double];
	def output: Array[Double]; // Between -1 and 1 (because we use tanh)
	
	def update(in: Array[Double]): LSTM;
}

@SerialVersionUID(3941150803677520723L)
class LSTMImp(
	protected val forgetGateNN: NeuralNetwork[Array[Double]],
	protected val candidateNN: NeuralNetwork[Array[Double]],
	protected val filterNN: NeuralNetwork[Array[Double]],
	val cellState: Array[Double],
	val output: Array[Double]
) extends LSTM {
	protected def feed(in: Array[Double]): (Array[Double], Array[Double]) = { // (cellState, output)
		val bigIn = output ++ in; // TODO System.arraycopy is faster than ++
		
		val forgetGate: Array[Double] = forgetGateNN.execute(bigIn);
		val candidate: Array[Double] = candidateNN.execute(bigIn);
		val filter: Array[Double] = filterNN.execute(bigIn);
		
		val newCellState: Array[Double] = new Array[Double](cellState.length);
		val newOutput: Array[Double] = new Array[Double](cellState.length);
		var i = 0;
		while(i < cellState.length) {
			val tmp: Double = forgetGate(i) * cellState(i) + (1.0 - forgetGate(i)) * candidate(i);
			
			newCellState(i) = tmp;
			newOutput(i) = filter(i) * math.tanh(tmp);
			
			i += 1;
		}
		
		(newCellState, newOutput);
	}
	
	def update(in: Array[Double]): LSTM = {
		val (newCellState, newOutput) = feed(in);
		
		new LSTMImp(forgetGateNN, candidateNN, filterNN, newCellState, newOutput);
	}
}

// Kind of abstract structure that needs to be instantiated with toLeaf to be used
@SerialVersionUID(3941150803677520723L)
class LSTMCore(
	val forgetGateNN: TransformNN, // Output should be between 0 and 1
	val candidateNN: TransformNN, // I don't think this one need a final non-linearity
	val filterNN: TransformNN, // Output should be between 0 and 1
	
	val cellState: Array[Double],
	val output: Array[Double] // Between -1 and 1 (because we use tanh)
) extends java.io.Serializable {
	val gatesParam: LSTMGatesParam = new LSTMGatesParam(
		forgetGateNN.param, 
		candidateNN.param, 
		filterNN.param
	);
	
	val param: LSTMParam = new LSTMParam(
		gatesParam, 
		Array1D(cellState), 
		Array1D(output)
	);
	
	def emptyGatesParam: LSTMGatesParam = gatesParam.empty;
	def emptyParam: LSTMParam = param.empty;
	
	def toLeaf: LeafLLSTM = new LeafLLSTMImp(
		forgetGateNN,
		candidateNN,
		filterNN,
		cellState,
		output,
		new Array[Double](cellState.size), // dCellState
		new Array[Double](cellState.size) // dOutput
	);
	
	def adaUpdate(hist: LSTMParam, grad: LSTMParam, adagrad: Double, fudgeFactor: Double): Unit = param.adaUpdate(hist, grad, adagrad, fudgeFactor);
	
	// A copy of this structure, with exactly the parameters given (not a copy of them)
	def duplicate(p: LSTMParam): LSTMCore = new LSTMCore(
		forgetGateNN.duplicate(p.gatesParam.forgetGate.l),
		candidateNN.duplicate(p.gatesParam.candidate.l),
		filterNN.duplicate(p.gatesParam.filter.l),
		
		p.cellState.a,
		p.output.a
	);
}

object LSTMCore {
	def apply(cellStateSize: Int, inSize: Int): LSTMCore = {
		val forgetGateNN = TransformNNFactory(Array("ML:Sigmoid"), Array(cellStateSize, (cellStateSize + inSize)));
		val candidateNN = TransformNNFactory(Array("ML:Tanh"), Array(cellStateSize, (cellStateSize + inSize)));
		val filterNN = TransformNNFactory(Array("ML:Sigmoid"), Array(cellStateSize, (cellStateSize + inSize)));
		
		val cellState = Array.fill(cellStateSize)(util.Random.nextGaussian);
		val output = cellState.map(x => math.tanh(x) * util.Random.nextDouble);
		
		new LSTMCore(
			forgetGateNN,
			candidateNN,
			filterNN,
			cellState,
			output
		);
	}
}

@SerialVersionUID(3941150803677520723L)
class LSTMGatesParam(
	val forgetGate: ParamList,
	val candidate: ParamList,
	val filter: ParamList
) extends CompoundParameter {
	val bricks = Array(forgetGate, candidate, filter);
	
	def toLSTMParam(cellState: Array[Double], output: Array[Double]): LSTMParam = new LSTMParam(this, Array1D(cellState), Array1D(output));
	
	def deepCopy: LSTMGatesParam = new LSTMGatesParam(forgetGate.deepCopy, candidate.deepCopy, filter.deepCopy);
	def empty: LSTMGatesParam = new LSTMGatesParam(forgetGate.empty, candidate.empty, filter.empty);
}

@SerialVersionUID(3941150803677520723L)
class LSTMParam(
	val gatesParam: LSTMGatesParam,
	val cellState: Array1D,
	val output: Array1D
) extends CompoundParameter {
	val bricks = Array(gatesParam, cellState, output);

	def deepCopy: LSTMParam = new LSTMParam(gatesParam.deepCopy, cellState.deepCopy, output.deepCopy);
	def empty: LSTMParam = new LSTMParam(gatesParam.empty, cellState.empty, output.empty);
}

// LSTM for backprop (build an LSTM graph)
trait LearningLSTM extends LSTM {
	def parent: LearningLSTM; // Parent LSTM which lead to this node (or null)
	
	override protected def forgetGateNN: TransformNN; // Output should be between 0 and 1
	override protected def candidateNN: TransformNN; // I don't think this one need a final non-linearity
	override protected def filterNN: TransformNN; // Output should be between 0 and 1

	def dCellState: Array[Double]; // Buffer for backprop
	def dOutput: Array[Double];    // Buffer for backprop
	
	protected def feed(in: Array[Double]): (Array[Double], Array[Double], Array[Double], Array[Double], Array[Double], Array[Double]) = { // (cellState, output, forgetGate, candidate, filter, tanhNCS)
		val bigIn = output ++ in; // TODO System.arraycopy is faster than ++
		
		val forgetGate: Array[Double] = forgetGateNN.execute(bigIn);
		val candidate: Array[Double] = candidateNN.execute(bigIn);
		val filter: Array[Double] = filterNN.execute(bigIn);
		
		val newCellState: Array[Double] = new Array[Double](cellState.length);
		val newOutput: Array[Double] = new Array[Double](cellState.length);
		val tanhNCS: Array[Double] = new Array[Double](cellState.length);
		var i = 0;
		while(i < cellState.length) {
			val tmp: Double = forgetGate(i) * cellState(i) + (1.0 - forgetGate(i)) * candidate(i);
			val tmpTanh = math.tanh(tmp);
			
			newCellState(i) = tmp;
			tanhNCS(i) = tmpTanh;
			newOutput(i) = filter(i) * tmpTanh;
			
			i += 1;
		}
		
		(newCellState, newOutput, forgetGate, candidate, filter, tanhNCS);
	}
	
	def update(in: Array[Double]): NodeLLSTM = {
		val (newCellState, newOutput, forgetGate, candidate, filter, tanhNCS) = feed(in);
		
		new NodeLLSTMImp(
			this, // parent
			in,
			forgetGateNN, 
			candidateNN, 
			filterNN, 
			newCellState, 
			newOutput,
			new Array[Double](newCellState.length), // dCellState
			new Array[Double](newCellState.length), // dOutput
			forgetGate, 
			candidate, 
			filter, 
			tanhNCS
		);
	}
}

trait LeafLLSTM extends LearningLSTM {
	def parent: LearningLSTM = null;
}

@SerialVersionUID(3941150803677520723L)
class LeafLLSTMImp(
	protected val forgetGateNN: TransformNN,
	protected val candidateNN: TransformNN,
	protected val filterNN: TransformNN,
	
	val cellState: Array[Double], // Initial cellState
	val output: Array[Double],    // Initial output
	
	val dCellState: Array[Double], // Buffer for backprop
	val dOutput: Array[Double]     // Buffer for backprop
) extends LeafLLSTM;

// Not really immutable
trait NodeLLSTM extends LearningLSTM {
	protected def in: Array[Double]; // Input vector which lead to this node
	
	protected def forgetGate: Array[Double]; // Memoized vector for backprop
	protected def candidate: Array[Double];  // Memoized vector for backprop
	protected def filter: Array[Double];     // Memoized vector for backprop
	protected def tanhNCS: Array[Double];    // Memoized vector for backprop
	
	// d is dc/dout[i]
	def gradient: (LSTMGatesParam, Array[Double]) = { // (dc/dparam, dc/din)
		val dFilter = new Array[Double](cellState.length);
		val dCandidate = new Array[Double](cellState.length);
		val dForgetGate = new Array[Double](cellState.length);
		
		{var i = 0;
		while(i < cellState.length) {
			dFilter(i) = dOutput(i) * tanhNCS(i);
			
			val tmp = dCellState(i) + dOutput(i) * filter(i) * (1.0 - (tanhNCS(i) * tanhNCS(i)));
			
			val tmpDCL = forgetGate(i) * tmp;
			
			parent.dCellState(i) += tmpDCL;
			dCandidate(i) = tmp - tmpDCL; // = (1.0 - forgetGate(i)) * tmp;
			dForgetGate(i) = (parent.cellState(i) - candidate(i)) * tmp;
			
			// Buffer are reinitialized
			//dOutput(i) = 0.0;
			//dCellState(i) = 0.0;
			
			i += 1;
		}}
		
		val bigIn = parent.output ++ in; // TODO System.arraycopy is faster than ++
		
		val forgetGateGrad = forgetGateNN.gradient(bigIn)(dForgetGate);
		val candidateGrad = candidateNN.gradient(bigIn)(dCandidate);
		val filterGrad = filterNN.gradient(bigIn)(dFilter);
		
		val dIn = java.util.Arrays.copyOfRange(forgetGateGrad._2, cellState.length, forgetGateGrad._2.length);
		
		{var i = 0;
		while(i < cellState.length) {
			parent.dOutput(i) += forgetGateGrad._2(i) + candidateGrad._2(i) + filterGrad._2(i);
			
			i += 1;
		}
		
		var j = 0;
		while(i < forgetGateGrad._2.length) {
			dIn(j) += candidateGrad._2(i) + filterGrad._2(i); // And not forgetGateGrad because it was copied right away
			
			i += 1;
			j += 1;
		}}
		
//		Console.println("in: " + in.mkString(", "));
//		Console.println("forgetGate: " + forgetGate.mkString(", "));
//		Console.println("candidate: " + candidate.mkString(", "));
//		Console.println("filter: " + filter.mkString(", "));
//		Console.println("tanhNCS: " + tanhNCS.mkString(", "));
//		Console.println("dFilter: " + dFilter.mkString(", "));
//		Console.println("dCandidate: " + dCandidate.mkString(", "));
//		Console.println("dForgetGate: " + dForgetGate.mkString(", "));
//		Console.println("filterGrad: " + filterGrad._2.mkString(", "));
//		Console.println("forgetGateGrad: " + forgetGateGrad._2.mkString(", "));
//		Console.println("candidateGrad: " + candidateGrad._2.mkString(", "));
		
		(new LSTMGatesParam(forgetGateGrad._1, candidateGrad._1, filterGrad._1), dIn);
	}
}

@SerialVersionUID(3941150803677520723L)
class NodeLLSTMImp(
	val parent: LearningLSTM,        // Parent LSTM which lead to this node
	protected val in: Array[Double], // Input vector which lead to this node
	
	protected val forgetGateNN: TransformNN,
	protected val candidateNN: TransformNN,
	protected val filterNN: TransformNN,
	
	val cellState: Array[Double],
	val output: Array[Double],
	
	val dCellState: Array[Double], // Buffer for backprop
	val dOutput: Array[Double],    // Buffer for backprop
	
	protected val forgetGate: Array[Double], // Memoized vector for backprop
	protected val candidate: Array[Double],  // Memoized vector for backprop
	protected val filter: Array[Double],     // Memoized vector for backprop
	protected val tanhNCS: Array[Double]     // Memoized vector for backprop
) extends NodeLLSTM;
