// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neuralNetwork.lstm;

import annotation.tailrec;
import collection._;

import neuralNetwork._;

// LSTM with a joint forget-memorize gate
@SerialVersionUID(3941150803677520723L)
trait LSTM extends NeuralNetwork[(Array[Double], Array[Double])] {
	def forgetGateNN: NeuralNetwork[Array[Double]]; // Output should be between 0 and 1
	def candidateNN: NeuralNetwork[Array[Double]]; // I don't think this one needs a final non-linearity
	def filterNN: NeuralNetwork[Array[Double]]; // Output should be between 0 and 1
	
	val param: List[ParameterBrick] = List(forgetGateNN.param, candidateNN.param, filterNN.param).flatten;
	
	val memorySize = forgetGateNN.height; // Size of the cellState
	val inputSize = forgetGateNN.width - memorySize; // Size of the "input" (in the LSTM sense)
	
	val height: Int = 2 * memorySize; // Size of the output
	val width: Int = (2 * memorySize) + inputSize; // Size of the input
	
	final protected def decomposeInput(in: Array[Double]): (Array[Double], Array[Double], Array[Double]) = {
		var pos = 0;
		val prevCellState = java.util.Arrays.copyOfRange(in, pos, (pos + memorySize));
		pos += memorySize;
		val prevOutput = java.util.Arrays.copyOfRange(in, pos, (pos + memorySize));
		pos += memorySize;
		val currentIn = java.util.Arrays.copyOfRange(in, pos, (pos + inputSize));
		pos += inputSize;
		
		assert(pos == in.length);
		
		(prevCellState, prevOutput, currentIn);
	}
	
	final protected def feed(prevCellState: Array[Double], prevOutput: Array[Double], currentIn: Array[Double]): (Array[Double], Array[Double], Array[Double], Array[Double], Array[Double], Array[Double], Array[Double]) = {
		val bigIn = prevOutput ++ currentIn; // TODO System.arraycopy is faster than ++
		
		val forgetGate: Array[Double] = forgetGateNN.execute(bigIn);
		val candidate: Array[Double] = candidateNN.execute(bigIn);
		val filter: Array[Double] = filterNN.execute(bigIn);
		
		val newCellState: Array[Double] = new Array[Double](memorySize);
		val newOutput: Array[Double] = new Array[Double](memorySize);
		val tanhNCS: Array[Double] = new Array[Double](memorySize);
		var i = 0;
		while(i < memorySize) {
			val tmp: Double = forgetGate(i) * prevCellState(i) + (1.0 - forgetGate(i)) * candidate(i);
			val tmpTanh = math.tanh(tmp);
			
			newCellState(i) = tmp;
			tanhNCS(i) = tmpTanh;
			newOutput(i) = filter(i) * tmpTanh;
			
			i += 1;
		}
		
		(newCellState, newOutput, forgetGate, candidate, filter, tanhNCS, bigIn);
	}
	
	def execute(in: Array[Double]): (Array[Double], Array[Double]) = {
		val (prevCellState, prevOutput, currentIn) = decomposeInput(in);
		
		val tmp = feed(prevCellState, prevOutput, currentIn);
		
		(tmp._1, tmp._2);
	}
	
	def executeArray(in: Array[Double]): Array[Double] = {
		val (cellState, output) = execute(in);
		
		cellState ++ output;
	}
}

class LearningLSTM(
	val forgetGateNN: TransformNN, // Output should be between 0 and 1
	val candidateNN: TransformNN, // I don't think this one needs a final non-linearity
	val filterNN: TransformNN // Output should be between 0 and 1
) extends LSTM with LearningNeuralNetwork[(Array[Double], Array[Double])] {
	def gradient(in: Array[Double]): (((Array[Double], Array[Double])) => (List[ParameterBrick], Array[Double])) = transformAndGradient(in)._2; // (List[dc/dparam], dc/din[i])
	
	def transformAndGradient(in: Array[Double]): ((Array[Double], Array[Double]), ((Array[Double], Array[Double])) => (List[ParameterBrick], Array[Double])) = { // (List[dc/dparam], dc/din[i])
		val (prevCellState, prevOutput, currentIn) = decomposeInput(in);
		
		val (newCellState, newOutput, forgetGate, candidate, filter, tanhNCS, bigIn) = feed(prevCellState, prevOutput, currentIn);
		
		(
			(newCellState, newOutput),
			((d: (Array[Double], Array[Double])) => { // d is dc/dvn[i]
				val (dCellState, dOutput) = d;
				val dInput = new Array[Double](width); // Gradient w.r.t. the whole input
				
				val dFilter = new Array[Double](memorySize);
				val dCandidate = new Array[Double](memorySize);
				val dForgetGate = new Array[Double](memorySize);
				
				{var i = 0;
				while(i < memorySize) {
					dFilter(i) = dOutput(i) * tanhNCS(i);
					
					val tmp = dCellState(i) + dOutput(i) * filter(i) * (1.0 - Math.square(tanhNCS(i)));
					
					val tmpDCL = forgetGate(i) * tmp;
					
					dInput(i) += tmpDCL;
					dCandidate(i) = tmp - tmpDCL; // = (1.0 - forgetGate(i)) * tmp;
					dForgetGate(i) = (prevCellState(i) - candidate(i)) * tmp;
					
					i += 1;
				}}
				
				val forgetGateGrad = forgetGateNN.gradient(bigIn)(dForgetGate);
				val candidateGrad = candidateNN.gradient(bigIn)(dCandidate);
				val filterGrad = filterNN.gradient(bigIn)(dFilter);
				
				System.arraycopy(forgetGateGrad._2, 0, dInput, memorySize, (memorySize + inputSize)); // Copy of prevOutput and currentIn
				
				{var i = 0;
				while(i < (memorySize + inputSize)) {
					dInput(i + memorySize) += candidateGrad._2(i) + filterGrad._2(i); // And not forgetGateGrad because it was copied right away
					
					i += 1;
				}}
				
				(List(forgetGateGrad._1, candidateGrad._1, filterGrad._1).flatten, dInput);
			})
		);
	}
	
	final protected def decomposeOutput(out: Array[Double]): (Array[Double], Array[Double]) = {
		var pos = 0;
		val cellState = java.util.Arrays.copyOfRange(out, pos, (pos + memorySize));
		pos += memorySize;
		val output = java.util.Arrays.copyOfRange(out, pos, (pos + memorySize));
		pos += memorySize;
		
		assert(pos == out.length);
		
		(cellState, output);
	}
	
	def outputAndGradient(in: Array[Double]): (Array[Double], Array[Double] => (List[ParameterBrick], Array[Double])) = { // (out, dc/dout => (List[dc/dparam], dc/din))
		val (out, dF) = transformAndGradient(in);
		
		(
			(out._1 ++ out._2),
			((d: Array[Double]) => dF(decomposeOutput(d)))
		);
	}
	
	def customSerialize: mutable.StringBuilder = {
		val stringBuilder = new mutable.StringBuilder();
		customSerialize(stringBuilder);
		
		stringBuilder;
	}
	
	def customSerialize(stringBuilder: mutable.StringBuilder): Unit = {
		stringBuilder ++= "lstm.LearningLSTM\n";
		forgetGateNN.customSerialize(stringBuilder);
		candidateNN.customSerialize(stringBuilder);
		filterNN.customSerialize(stringBuilder);
	}
	
	// A copy of the network with p (not a copy) as parameters
	override def duplicate(p: List[ParameterBrick]): LearningLSTM = duplicateFeed(p) match {
		case (x, Nil) => x;
		case _ => {throw IncompatibleParameters; this;}
	}
	
	// Similar as duplicate, but also returns the unused ParameterBrick
	override def duplicateFeed(p: List[ParameterBrick]): (LearningLSTM, List[ParameterBrick]) = {
		val tmpFG = forgetGateNN.duplicateFeed(p);
		val tmpC = candidateNN.duplicateFeed(tmpFG._2);
		val tmpF = filterNN.duplicateFeed(tmpC._2);

		(new LearningLSTM(tmpFG._1, tmpC._1, tmpF._1), tmpF._2);
	}

	override def toString: String = "LearningLSTM(" + List(forgetGateNN, candidateNN, filterNN).mkString(", ") + ")";
}

object LearningLSTM {
	// Inverse of the custom serialization
	def read(str: String): LearningLSTM = read(str.split('\n'), 1)._1; // The first line is for the type of network
	def read(data: Array[String], i: Int): (LearningLSTM, Int) = {
		var j = i;
		
		var tmp = TransformNN.read(data, j + 1); // +1 because we know it's gonna be a TransformNN
		val forgetGateNN = tmp._1;
		j = tmp._2;
		
		tmp = TransformNN.read(data, j + 1); // +1 because we know it's gonna be a TransformNN
		val candidateNN = tmp._1;
		j = tmp._2;
		
		tmp = TransformNN.read(data, j + 1); // +1 because we know it's gonna be a TransformNN
		val filterNN = tmp._1;
		j = tmp._2;
		
		(new LearningLSTM(
			forgetGateNN,
			candidateNN,
			filterNN
		), j);
	}
}

object LSTMFactory {
	def apply(memorySize: Int, inSize: Int): LearningLSTM = {
		val forgetGateNN = TransformNNFactory(Array("ML:Sigmoid"), Array(memorySize, (memorySize + inSize)));
		val candidateNN = TransformNNFactory(Array("ML:None"), Array(memorySize, (memorySize + inSize)));
		val filterNN = TransformNNFactory(Array("ML:Sigmoid"), Array(memorySize, (memorySize + inSize)));
		
		new LearningLSTM(
			forgetGateNN,
			candidateNN,
			filterNN
		);
	}
}

// Remark: in fact, this can be turned into a general sequence encoder; one is not restricted to using a LSTM
class LSTMEncoder(val lstm: LearningLSTM) extends TestableLearningSystem {
	def param: List[ParameterBrick] = lstm.param;

	val outputSize: Int = lstm.memorySize; // Size of the output (encoding)
	val inputSize: Int = lstm.inputSize; // Size of the input (in the LSTM sense)

	protected val testWidth = {
		val sequenceLength = 5; // Testing is done on sequences of length 5
		
		((2 * outputSize) + (sequenceLength * inputSize));
	}
	
	final protected def decomposeInput(in: Array[Double]): (Array[Double], IndexedSeq[Array[Double]]) = {
		var pos = 0;

		val initState = java.util.Arrays.copyOfRange(in, pos, (pos + lstm.height));
		pos += lstm.height;

		val sequence = (0 until ((in.length - pos) / inputSize)).map(i => {
			val tmp = (pos + (i * inputSize));
			java.util.Arrays.copyOfRange(in, tmp, (tmp + inputSize));
		});
		pos += (sequence.length * inputSize);

		assert(pos == in.length);

		(initState, sequence);
	}

	def encode(initState: Array[Double], sequence: Seq[Array[Double]]): Array[Double] = {
		val finalState = sequence.foldLeft(initState){ case (state, v) => lstm.executeArray(state ++ v)};

		java.util.Arrays.copyOfRange(finalState, lstm.memorySize, lstm.height);
	}

	def encodeAndGradient(initState: Array[Double], sequence: Seq[Array[Double]]): (
		Array[Double], 
		(Array[Double] => (
			List[ParameterBrick], // dParam
			List[Array[Double]],  // dSequence
			Array[Double]         // dInitState
		))
	) = {
		val (finalState, gradL) = sequence.foldLeft((initState, Nil: List[Array[Double] => (List[ParameterBrick], Array[Double])])){ case ((state, gradList), v) => {
			val (output, gradF) = lstm.outputAndGradient(state ++ v);

			(output, (gradF :: gradList));
		}};
		
		val encoding = java.util.Arrays.copyOfRange(finalState, lstm.memorySize, lstm.height);

		(
			encoding,
			(dEncoding: Array[Double]) => {
				val dParam = ParamList(lstm.emptyParam);
				val (dInitState, dSequence) = gradL.foldLeft(((Array.ofDim[Double](lstm.memorySize) ++ dEncoding), Nil: List[Array[Double]])){ case ((dOut, dSeq), gradF) => {
					val (dP, dIn) = gradF(dOut);
					
					dParam.addTo(dP);

					// Split dIn
					val dState = java.util.Arrays.copyOfRange(dIn, 0, lstm.height);
					val dV = java.util.Arrays.copyOfRange(dIn, lstm.height, lstm.width);

					(dState, (dV :: dSeq));
				}};

				(dParam.l, dSequence, dInitState);
			}
		);
	}

	def encodeFull(initState: Array[Double], sequence: Seq[Array[Double]]): List[Array[Double]] = {
		val (_, stateL) = sequence.foldLeft((initState, Nil: List[Array[Double]])){ case ((state, stateList), v) => {
			val output = lstm.executeArray(state ++ v);

			(output, (output :: stateList));
		}}
		
		stateL.reverseMap(state => java.util.Arrays.copyOfRange(state, lstm.memorySize, lstm.height));
	}

	// Like encodeFull, but with a single Array as input and a single Array as ouput
	def executeArray(in: Array[Double]): Array[Double] = {
		val (initState, sequence) = decomposeInput(in); 

		encodeFull(initState, sequence).toArray.flatten;
	}

	def encodeFullAndGradient(initState: Array[Double], sequence: Seq[Array[Double]]): (
		List[Array[Double]], 
		(Seq[Array[Double]] => (
			List[ParameterBrick],    // dParam
			List[Array[Double]], // dSequence
			Array[Double]        // dInitState
		))
	) = {
		val (_, stateL, gradL) = sequence.foldLeft((initState, Nil: List[Array[Double]], Nil: List[Array[Double] => (List[ParameterBrick], Array[Double])])){ case ((state, stateList, gradList), v) => {
			val (output, gradF) = lstm.outputAndGradient(state ++ v);

			(output, (output :: stateList), (gradF :: gradList));
		}};
		
		val encodings = stateL.reverseMap(state => java.util.Arrays.copyOfRange(state, lstm.memorySize, lstm.height));
		
		(
			encodings,
			(dEncodings: Seq[Array[Double]]) => {
				val dParam = ParamList(lstm.emptyParam);
				val dEncodingsTmp = dEncodings.reverseMap(dEncoding => (Array.ofDim[Double](lstm.memorySize) ++ dEncoding));
				val (dInitState, dSequence) = (gradL, dEncodingsTmp).zipped.foldLeft(Array.ofDim[Double](lstm.height), Nil: List[Array[Double]]){ case ((dOut, dSeq), (gradF, dEncoding)) => {
					Math.addTo(dOut, dEncoding);
					val (dP, dIn) = gradF(dOut);

					dParam.addTo(dP);

					// Split dIn
					val dState = java.util.Arrays.copyOfRange(dIn, 0, lstm.height);
					val dV = java.util.Arrays.copyOfRange(dIn, lstm.height, lstm.width);

					(dState, (dV :: dSeq));
				}}
				
				(dParam.l, dSequence, dInitState);
			}
		);
	}

	// Like encodeFullAndGradient, but with a single Array as input and a single Array as output
	def outputAndGradient(in: Array[Double]): (
		Array[Double], 
		(Array[Double] => (
			List[ParameterBrick], // dParam
			Array[Double]     // dInitState ++ dSequence
		))
	) = {
		val (initState, sequence) = decomposeInput(in);
		val tmp = encodeFullAndGradient(initState, sequence);
		
		val output = tmp._1.toArray.flatten;

		(
			output,
			(dOutput: Array[Double]) => {
				val dEncodings = dOutput.sliding(outputSize, outputSize).toSeq;
				val tmp2 = tmp._2(dEncodings);

				(tmp2._1, (tmp2._3 :: tmp2._2).toArray.flatten);
			}
		);
	}
}

object LSTMEncoder {
	def apply(inputSize: Int, outputSize: Int): LSTMEncoder = new LSTMEncoder(LSTMFactory(outputSize, inputSize));
}

class BiLSTMEncoder(val forwardLSTM: LearningLSTM, val backwardLSTM: LearningLSTM) extends TestableLearningSystem {
	private val forwardEncoder = new LSTMEncoder(forwardLSTM);
	private val backwardEncoder = new LSTMEncoder(backwardLSTM);

	def param: List[ParameterBrick] = forwardEncoder.param ::: backwardEncoder.param;

	val forwardSize: Int = forwardEncoder.outputSize;
	val backwardSize: Int = backwardEncoder.outputSize;

	val outputSize: Int = forwardEncoder.outputSize + backwardEncoder.outputSize; // Size of the output (encoding)
	val inputSize: Int = forwardEncoder.inputSize; // Size of the input (in the LSTM sense)

	protected val testWidth = {
		val sequenceLength = 5; // Testing is done on sequences of length 5
		
		((2 * outputSize) + (sequenceLength * inputSize));
	}

	final protected def decomposeInput(in: Array[Double]): (Array[Double], Array[Double], IndexedSeq[Array[Double]]) = {
		var pos = 0;

		val forwardInitState = java.util.Arrays.copyOfRange(in, pos, (pos + forwardEncoder.lstm.height));
		pos += forwardEncoder.lstm.height;
		val backwardInitState = java.util.Arrays.copyOfRange(in, pos, (pos + backwardEncoder.lstm.height));
		pos += backwardEncoder.lstm.height;

		val sequence = (0 until ((in.length - pos) / inputSize)).map(i => {
			val tmp = (pos + (i * inputSize));
			java.util.Arrays.copyOfRange(in, tmp, (tmp + inputSize));
		});
		pos += (sequence.length * inputSize);

		assert(pos == in.length);

		(forwardInitState, backwardInitState, sequence);
	}

	def encode(forwardInitState: Array[Double], backwardInitState: Array[Double], sequence: Seq[Array[Double]]): List[Array[Double]] = {
		val forwardEncodings = forwardEncoder.encodeFull(forwardInitState, sequence);
		val backwardEncodings = backwardEncoder.encodeFull(backwardInitState, sequence.reverse);

		(forwardEncodings, backwardEncodings.reverse).zipped.map(_ ++ _);
	}
	
	// Like encode, but with a single Array as input and a single Array as ouput
	def executeArray(in: Array[Double]): Array[Double] = {
		val (forwardInitState, backwardInitState, sequence) = decomposeInput(in);
		
		encode(forwardInitState, backwardInitState, sequence).toArray.flatten;
	}

	def encodeAndGradient(forwardInitState: Array[Double], backwardInitState: Array[Double], sequence: Seq[Array[Double]]): (
		List[Array[Double]], 
		(Seq[Array[Double]] => (
			List[ParameterBrick],    // dParam
			List[Array[Double]], // dSequence
			Array[Double],       // dForwardInitState
			Array[Double]        // dBackwardInitState
		))                 
	) = {
		val (forwardEncodings, gradLForward) = forwardEncoder.encodeFullAndGradient(forwardInitState, sequence);
		val (backwardEncodings, gradLBackward) = backwardEncoder.encodeFullAndGradient(backwardInitState, sequence.reverse);

		val encodings = (forwardEncodings, backwardEncodings.reverse).zipped.map(_ ++ _);

		(
			encodings,
			(dEncodings: Seq[Array[Double]]) => {
				val dForwardEncodings = dEncodings.map(dEncoding => java.util.Arrays.copyOfRange(dEncoding, 0, forwardLSTM.memorySize));
				val dBackwardEncodings = dEncodings.reverseMap(dEncoding => java.util.Arrays.copyOfRange(dEncoding, forwardLSTM.memorySize, dEncoding.length));
				
				val (dForwardLSTM, dSequence, dForwardInitState) = gradLForward(dForwardEncodings);
				val (dBackwardLSTM, dSequence2, dBackwardInitState) = gradLBackward(dBackwardEncodings);
				
				(dSequence, dSequence2.reverse).zipped.foreach((dVForward, dVBackward) => Math.addTo(dVForward, dVBackward));

				((dForwardLSTM ::: dBackwardLSTM), dSequence, dForwardInitState, dBackwardInitState);
			}
		);
	}

	// Like encodeAndGradient, but with a single Array as input and a single Array as output
	def outputAndGradient(in: Array[Double]): (
		Array[Double], 
		(Array[Double] => (
			List[ParameterBrick], // dParam
			Array[Double]     // dInitState ++ dSequence
		))
	) = {
		val (forwardInitState, backwardInitState, sequence) = decomposeInput(in);
		val tmp = encodeAndGradient(forwardInitState, backwardInitState, sequence);

		val output = tmp._1.toArray.flatten;

		(
			output,
			(dOutput: Array[Double]) => {
				val dEncodings = dOutput.sliding(outputSize, outputSize).toSeq;
				val tmp2 = tmp._2(dEncodings);

				(tmp2._1, (tmp2._3 :: tmp2._4 :: tmp2._2).toArray.flatten);
			}
		);
	}
}

object BiLSTMEncoder {
	def apply(inputSize: Int, size: Int): BiLSTMEncoder = apply(inputSize, size, size);
	def apply(inputSize: Int, forwardSize: Int, backwardSize: Int): BiLSTMEncoder = new BiLSTMEncoder(LSTMFactory(forwardSize, inputSize), LSTMFactory(backwardSize, inputSize));
}

class LSTMDecoder(val lstm: LearningLSTM, val preprocessor: LearningNeuralNetwork[Array[Double]], val probabilizer: TransformNN, val selector: Selector, private val maxLength: Int) extends TestableLearningSystem {
	val param: List[ParameterBrick] = List(lstm.param, preprocessor.param, probabilizer.param, selector.param).flatten;

	protected def testWidth: Int = {
		val sequenceLength = 8;
		
		(2 * lstm.memorySize) + (sequenceLength * probabilizer.height);
	}

	final protected def decomposeInput(in: Array[Double]): (Array[Double], Array[Double], IndexedSeq[Array[Double]]) = {
		var pos = 0;

		val initCS = java.util.Arrays.copyOfRange(in, pos, (pos + lstm.memorySize));
		pos += lstm.memorySize;

		val initO = java.util.Arrays.copyOfRange(in, pos, (pos + lstm.memorySize));
		pos += lstm.memorySize;

		val probSequence = (0 until ((in.length - pos) / probabilizer.height)).map(i => {
			val tmp = (pos + (i * probabilizer.height));
			java.util.Arrays.copyOfRange(in, tmp, (tmp + probabilizer.height));
		});
		pos += (probSequence.length * probabilizer.height);

		assert(pos == in.length);

		(initCS, initO, probSequence);
	}	
	
	private def decodeAux(initCS: Array[Double], initO: Array[Double], fIndex: (Int, Array[Double]) => Int): (List[Int], List[Array[Double]]) = {
		val resIndex = mutable.ListBuffer[Int]();
		val resProbabilities = mutable.ListBuffer[Array[Double]]();

		@tailrec
		def loop(cellState: Array[Double], output: Array[Double], i: Int): Unit = if(i != maxLength) {
			val preprocessed = preprocessor.execute(output);
			val probabilities = probabilizer.execute(preprocessed);
			val index = fIndex(i, probabilities);

			resIndex += index;
			resProbabilities += probabilities;

			selector.get(index) match {
				case Some(vector) => {
					val (newCellState, newOutput) = lstm.execute(cellState ++ output ++ vector);

					loop(newCellState, newOutput, (i + 1));
				}

				case None => (); // Index corresponds to the end-of-sequence symbol
			}
		}

		loop(initCS, initO, 0);

		(resIndex.toList, resProbabilities.toList);
	}

	def decode(initCS: Array[Double], initO: Array[Double]): List[Int] = {
		def fIndex = ((i: Int), (p: Array[Double])) => selector.select(p);

		decodeAux(initCS, initO, fIndex)._1;
	}
	
	// Returns the cost of a given sequence of probability distributions 
	// Only works if the selector only selects the end-of-senquence symbol at the end
	def executeArray(in: Array[Double]): Array[Double] = {
		val (initCS, initO, probSequence) = decomposeInput(in);
		
		def fIndex = ((i: Int), (p: Array[Double])) => selector.select(probSequence(i));

		val crossEntropy = (probSequence, decodeAux(initCS, initO, fIndex)._2).zipped.map((p,q) => CrossEntropy(p, q)).sum;

		Array(crossEntropy);
	}

	private def deriveAux(initCS: Array[Double], initO: Array[Double], fIndex: (Int, Array[Double]) => Int, fCost: (Int, Array[Double]) => (Double, Array[Double])): (Double, List[ParameterBrick], Array[Double], Array[Double]) = {
		val resIndex = mutable.ListBuffer[Int]();
		val resProbabilities = mutable.ListBuffer[Array[Double]]();
		//val preprocessorFs = mutable.ListBuffer[(Array[Double] => (List[ParameterBrick], Array[Double]))]();
		//val probabilizerFs = mutable.ListBuffer[(Array[Double] => (List[ParameterBrick], Array[Double]))]();
		val lstmFs = mutable.ListBuffer[(((Array[Double], Array[Double])) => (List[ParameterBrick], Array[Double]))]();
		val dOutputs = mutable.ListBuffer[Array[Double]]();

		val dPreprocessorParam = ParamList(preprocessor.emptyParam);
		val dProbabilizerParam = ParamList(probabilizer.emptyParam);

		var cost: Double = 0.0;

		@tailrec
		def loop(cellState: Array[Double], output: Array[Double], i: Int): Unit = if(i != maxLength) {
			val (preprocessed, preprocessorF) = preprocessor.outputAndGradient(output);
			val (probabilities, probabilizerF) = probabilizer.transformAndGradient(preprocessed);
			val index = fIndex(i, probabilities);

			resIndex += index;
			resProbabilities += probabilities;
			//preprocessorFs += preprocessorF;
			//probabilizerFs += probabilizerF;

			val (currentCost, dProb) = fCost(i, probabilities);
			cost += currentCost;

			val (dProbabilizer, dPreprocessed) = probabilizerF(dProb);
			dProbabilizerParam.addTo(dProbabilizer);
			
			val (dPreprocessor, dOutput) = preprocessorF(dPreprocessed);
			dPreprocessorParam.addTo(dPreprocessor);
			dOutputs += dOutput

			selector.get(index) match {
				case Some(vector) => {
					val ((newCellState, newOutput), lstmF) = lstm.transformAndGradient(cellState ++ output ++ vector);

					lstmFs += lstmF;

					loop(newCellState, newOutput, (i + 1));
				}

				case None => (); // Index corresponds to the end-of-sequence symbol
			}
		}

		loop(initCS, initO, 0);

		// TODO backwardPass
		???
	}

	def derive(initCS: Array[Double], initO: Array[Double], outputSequence: IndexedSeq[Int]): (Double, List[ParameterBrick], Array[Double], Array[Double]) = {
		def fIndex = ((i: Int), (p: Array[Double])) => outputSequence(i);
		def fCost = ((i: Int), (p: Array[Double])) => (CrossEntropy(outputSequence(i),p), CrossEntropy.derivativeOp(outputSequence(i), p));

		deriveAux(initCS, initO, fIndex, fCost);
	}

	def outputAndGradient(in: Array[Double]): (Array[Double], Array[Double] => (List[ParameterBrick], Array[Double])) = { // (out, dc/dout => (List[dc/dparam], dc/din))
		val (initCS, initO, probSequence) = decomposeInput(in);
		
		def fIndex = ((i: Int), (p: Array[Double])) => selector.select(probSequence(i));
		def fCost = ((i: Int), (p: Array[Double])) => (CrossEntropy(probSequence(i), p), CrossEntropy.derivativeOp(probSequence(i), p));

		val tmp = deriveAux(initCS, initO, fIndex, fCost);

		// There is one thing to take into account in the test procedure: the probSequence part of the input should not be tested; maybe each TestableLearningSystem should come with a testInput: Array[Double] and a testWidth <= testInput.length defining the part of this input to test
		???
	}
}
