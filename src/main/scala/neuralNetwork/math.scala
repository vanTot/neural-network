// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neuralNetwork;

import annotation.tailrec;
import collection.mutable;

/** Usual linear-algebra and arithmetic functions
*
* This object contains a lot of useful mathematic functions, mainly concerning linear-algebra and arithmetic.
*/
object Math {
	/** Calculates the square of the given number
	*
	* @param x the Double to square
	* @return the result of squaring x
	*/
	@inline
	def square(x: Double): Double = x * x;
	
	/** Calculates the cross entropy between two given probability distributions
	*
	* @param p the first probability distribution
	* @param q the second probability distribution
	* @return the cross entropy between p and q
	*/
	@inline
	def crossEntropy(p: Array[Double], q: Array[Double]): Double = {
		var tmp = 0.0;
		
		var i = 0; 
		while(i < p.length) {
			if(p(i) > 0.0) tmp += p(i) * math.log(q(i));
			
			i += 1;
		}
		
		-tmp;
	}
	
	/** Finds the index of the maximum value of a given array
	*
	* @param v the array of values
	* @return the smallest index in v containing the hight value
	*/
	def indexOfMax(v: Array[Double]): Int = {
		var maxVal = v(0);
		var maxId = 0;
		
		var i = 1;
		while(i < v.length) {
			if(v(i) > maxVal) {
				maxVal = v(i);
				maxId = i;
			}
			
			i += 1;
		}
		
		maxId;
	}
	
	/** Computes a weighted sum of arrays
	*
	* @param vectors the arrays to sum
	* @param weights the corresponding weights
	* @return the sum of vectors weighted with weights
	*/
	def weightedSum(vectors: Array[Array[Double]], weights: Array[Double]): Array[Double] = {
		val res = multiply(vectors(0), weights(0));
		
		var i = 1;
		while(i < weights.length) {
			addToWeighted(res, vectors(i), weights(i));
			
			i += 1;
		}
		
		res;
	}
	
	/** Computes a weighted sum of arrays
	*
	* @param vectors the arrays to sum
	* @param weights the corresponding weights
	* @return the sum of vectors weighted with weights
	*/
	def weightedSum(vectors: Seq[Array[Double]], weights: Seq[Double]): Array[Double] = {
		val res = Array.ofDim[Double](vectors.head.length);
		
		(vectors, weights).zipped.foreach((v, w) => addToWeighted(res, v, w));
		
		res;
	}
	
	/** Computes a sum of arrays
	*
	* @param vectors the arrays to sum
	* @return the sum of vectors
	*/
	def sum(vectors: Array[Array[Double]]): Array[Double] = {
		val res = vectors.head.clone;
		
		var i = 1;
		while(i < vectors.length) {
			addTo(res, vectors(i));
			
			i += 1;
		}
		
		res;
	}
	
	/** Computes a sum of arrays
	*
	* @param vectors the arrays to sum
	* @return the sum of vectors
	*/
	def sum(vectors: List[Array[Double]]): Array[Double] = {
		val res = vectors.head.clone;
		
		@tailrec
		def loop(l: List[Array[Double]]): Unit = l match {
			case h :: t => {
				addTo(res, h);
				
				loop(t);
			}
			
			case Nil => ();
		}
		
		loop(vectors.tail);
		
		res;
	}
	
	/** Builds a new array of Double by applying a function to each element of a given array
	*
	* @param v the original elements
	* @param f the function to apply to each element
	* @return a new array resulting from applying the given function f to each element of the given array v and collecting the results
	*/
	@inline
	// It should be faster than Array.map because it is specialized (to Array)
	def arrayMap(v: Array[Double], f: (Double => Double)): Array[Double] = {
		val l = v.length;
		
		val res = Array.ofDim[Double](l);
		
		var i = 0;
		while(i < l) {
			res(i) = f(v(i));
			
			i += 1;
		}
		
		res;
	}
	
	/** Computes the square of the norm 2 of a vector
	*
	* @param v the coordinates of the vector
	* @return the square of the norm 2 of v
	*/
	@inline
	def norm2Sq(v: IndexedSeq[Double]): Double = {
		var x = 0.0;
		var i = 0;
		while(i < v.length) {
			x += v(i) * v(i);
			
			i += 1;
		}
		
		x;
	}
	
	/** Selects an index randomly from a probability distribution
	*
	* @param prob the probability distribution
	* @return an random index with a probability equals to the corresponding value is prob
	*/
	@inline
	def selectFromProb(prob: IndexedSeq[Double]): Int = {
		var target = util.Random.nextDouble;
		
		var i = -1;
		while(target >= 0) {
			i += 1;
			
			target -= prob(i);
		}
		
		i;
	}
	
	/** Normalizes a vector according to the l1 norm (inplace)
	*
	* @param v the vector to normalize (modified)
	*/
	@inline
	def normalizeTo(v: Array[Double]): Unit = {
		val l = v.length;
		val total = v.sum;
		
		var i = 0;
		while(i < l) {
			v(i) /= total;
			
			i += 1;
		}
	}
	
	/** Computes the arctangent of a number
	*
	* @param x the Double to compute the arctangent of
	* @return the arctangent of x
	*/
	@inline
	def atanh(x: Double): Double = 0.5 * (math.log(1.0 + x) - math.log(1.0 - x));
	
	/** Computes the softmax of an array
	*
	* @param v the array to compute the softmax of
	* @return the softmax of v
	*/
	@inline
	def softMax(v: Array[Double]): Array[Double] = {
		/*val tmp = v.map(math.exp);
		val tot = tmp.sum;
		
		tmp.map(_ / tot);*/
		
		// Stable version
		val c = -v.max;
		
		var tot = 0.0; // Sum of the exp
		val res = Array.ofDim[Double](v.length);
		{var i = 0;
		while(i < v.length) {
			val tmp = math.exp(v(i) + c);
			res(i) = tmp;
			tot += tmp;
			
			i += 1;
		}}
		{var i = 0;
		while(i < v.length) {
			res(i) /= tot;
			
			i += 1;
		}}
		
//		if(structureHandlers.ArrayHandler.nanScan(res) && !structureHandlers.ArrayHandler.nanScan(v)) io.StdIn.readLine(res.mkString(",") + " " + v.mkString(","));
		
		res;
	}
	
	/** Computes the softmax of a list
	*
	* @param v the list to compute the softmax of
	* @return the softmax of v
	*/
	@inline
	def softMax(v: List[Double]): List[Double] = {
		
		// Stable version
		val c = -v.max;

		@tailrec
		def loopA(l: List[Double], acc: List[Double], sum: Double): List[Double] = l match {
			case h :: t => {
				val x = math.exp(h + c);
				
				loopA(t, (x :: acc), (x + sum));
			}
			
			case _ => loopB(acc, sum, Nil);
		}
		
		@tailrec
		def loopB(l: List[Double], tot: Double, res: List[Double]): List[Double] = l match {
			case h :: t => loopB(t, tot, ((h / tot) :: res));
			case _ => res;
		}
		
		loopA(v, Nil, 0.0);
	}
	
	/** Evaluates the sigmoid function on a given number
	*
	* @param x the Double to evaluate the sigmoid function on
	* @return the image of x by the sigmoid function
	*/
	@inline
	def sigmoid(x: Double): Double = 1.0 / (1.0 + math.pow(math.E, -x));
	
	/** Multiplies an array of Double by a Double (inplace)
	*
	* @param v the array (modified)
	* @param s the multiplicative coefficient
	*/
	@inline
	def multiplyTo(v: Array[Double], s: Double): Unit = {
		val l = v.length;
		
		var i = 0;
		while(i < l) {
			v(i) *= s;
			
			i += 1;
		}
	}
	
	/** Multiplies a 2D array of Double by a Double (inplace)
	*
	* @param m the arrays (modified)
	* @param s the multiplicative coefficient
	*/
	@inline
	def multiplyTo(m: Array[Array[Double]], s: Double): Unit = {
		val nbRows = m.length;
		
		var i = 0;
		while(i < nbRows) {
			multiplyTo(m(i), s);
			
			i += 1;
		}
	}
	
	/** Multiplies a 3D array of Double by a Double (inplace)
	*
	* @param t the arrays (modified)
	* @param s the multiplicative coefficient
	*/
	@inline
	def multiplyTo(t: Array[Array[Array[Double]]], s: Double): Unit = {
		val depth = t.length;
		
		var i = 0;
		while(i < depth) {
			multiplyTo(t(i), s);
			
			i += 1;
		}
	}
	
	/** Multiplies an array of Double by a Double
	*
	* @param v the array
	* @param s the multiplicative coefficient
	* @return the array resulting from multiplying each value of v by s
	*/
	@inline
	def multiply(v: Array[Double], s: Double): Array[Double] = {
		val l = v.length;
		
		val res = Array.ofDim[Double](l);
		
		var i = 0;
		while(i < l) {
			res(i) = v(i) * s;
			
			i += 1;
		}
		
		res;
	}
	
	/** Multiplies a 2D array of Double by a Double
	*
	* @param m the arrays
	* @param s the multiplicative coefficient
	* @return the 2D array resulting from multiplying each value of m by s
	*/
	@inline
	def multiply(m: Array[Array[Double]], s: Double): Array[Array[Double]] = {
		val nbRows = m.length;
		
		val res = Array.ofDim[Array[Double]](nbRows);
		
		var i = 0;
		while(i < nbRows) {
			res(i) = multiply(m(i), s);
			
			i += 1;
		}
		
		res;
	}
	
	/** Multiplies a 3D array of Double by a Double
	*
	* @param t the arrays
	* @param s the multiplicative coefficient
	* @return the 3D array resulting from multiplying each value of m by s
	*/
	@inline
	def multiply(m: Array[Array[Array[Double]]], s: Double): Array[Array[Array[Double]]] = {
		val nbMat = m.length;
		
		val res = Array.ofDim[Array[Array[Double]]](nbMat);
		
		var i = 0;
		while(i < nbMat) {
			res(i) = multiply(m(i), s);
			
			i += 1;
		}
		
		res;
	}
	
	/** Performs a tensor-matrix multiplication
	*
	* @param t the 3D array representing a tensor
	* @param v the array representing the vector
	* @return the array resulting from performing the tensor-matrix multiplication of t and v
	*/
	@inline
	def multiply(t: Array[Array[Array[Double]]], v: Array[Double]): Array[Double] = {
		val depth = t.length;
		
		val res = Array.ofDim[Double](depth);
		
		var i = 0;
		while(i < depth) {
			res(i) = dotProduct(v, matApply(t(i), v));
			
			i+= 1;
		}
		
		res;
	}
	
	/** Multiplies two arrays element-wise
	*
	* @param v1 an array
	* @param v2 an array
	* @return the array resulting from multiplying v1 and v2 element-wise
	*/
	@inline
	def multiply(v1: Array[Double], v2: Array[Double]): Array[Double] = {
		//if(v1.length != v2.length) throw IncompatibleSizes;
		require(v1.length == v2.length);
		
		val l = v1.length;
		
		val res = Array.ofDim[Double](l);
		
		var i = 0;
		while(i < l) {
			res(i) = v1(i) * v2(i);
			
			i += 1;
		}
		
		res;
	}
	
	/** Multiplies two arrays element-wise (inplace)
	*
	* @param v1 an array (modified)
	* @param v2 an array
	*/
	@inline
	def multiplyTo(v1: Array[Double], v2: Array[Double]): Unit = {
		//if(v1.length != v2.length) throw IncompatibleSizes;
		require(v1.length == v2.length);
		
		val l = v1.length;
		
		var i = 0;
		while(i < l) {
			v1(i) *= v2(i);
			
			i += 1;
		}
	}
	
	/** Computes the sum of each column of a 2D array
	*
	* @param m a 2D array
	* @return the array whose ith value is the sum of the values of the ith column of m
	*/
	@inline
	def sumCols(m: Array[Array[Double]]): Array[Double] = {
		val nbRows = m.length;
		val nbCols = m.head.length;
		
		val res = Array.ofDim[Double](nbCols);
		
		var i = 0;
		while(i < nbRows) {
			val mRow = m(i);
			
			var j = 0;
			while(j < nbCols) {
				res(j) += mRow(j);
				
				j += 1;
			}
			
			i += 1;
		}
		
		res;
	}
	
	/** Multiplies each line of a matrix by a Double
	*
	* @param v the array of multiplicative coefficient
	* @param m the 2D array representing the matrix
	* @return the 2D array representing the matrix resulting from multiplying each ith line of m by the ith value of v: (v(i)*m(i))_i
	*/
	@inline
	def multiplyLineWise(v: Array[Double], m: Array[Array[Double]]): Array[Array[Double]] = {
		//if(v.length != m.length) throw IncompatibleSizes;
		require(v.length == m.length);
		
		val M_ROWS = m.length;
		val M_COLS = m.head.length;
		
		val res = Array.ofDim[Double](M_ROWS, M_COLS);
		
		var i = 0;
		while(i < M_ROWS) {
			val resRow = res(i);
			val mRow = m(i);
			
			var j = 0;
			while(j < M_COLS) {
				resRow(j) = v(i) * mRow(j);
				
				j += 1;
			}
			
			i += 1;
		}
		
		res;
	}
	
	/** Multiplies each line of a tensor by a Double
	*
	* @param v the array of multiplicative coefficient
	* @param t the 3D array representing the tensor
	* @return the 3D array representing the tensor resulting from multiplying each ith layer of t by the ith value of v: (v(i)*t(i))_i
	*/
	@inline
	def multiplyLayerWise(v: Array[Double], t: Array[Array[Array[Double]]]): Array[Array[Array[Double]]] = {
		//if(v.length != t.length) throw IncompatibleSizes;
		require(v.length == t.length);
		
		val depth = t.length;
		
		val res = Array.ofDim[Array[Array[Double]]](depth);
		
		var i = 0;
		while(i < depth) {
			res(i) = multiply(t(i), v(i));
			
			i += 1;
		}
		
		res;
	}
	
	/** Performs a matrix-vector multiplication
	*
	* @param m the 2D array representing the matrix
	* @param v the array representing the vector
	* @return the array representing the result of multiplying m and v
	*/
	@inline
	def matApply(m: Array[Array[Double]], v: Array[Double]): Array[Double] = {
		val nbRows = m.length;
		
		val res = Array.ofDim[Double](nbRows);
		
		var i = 0;
		while(i < nbRows) {
			res(i) = dotProduct(m(i), v);
			
			i += 1;
		}

		res;
	}
	
	/** Performs a matrix-vector multiplication
	*
	* @param m the array representing the matrix
	* @param width the width of the matrix
	* @param height the height of the matrix
	* @param v the array representing the vector
	* @return the array representing the result of multiplying m and v
	*/
	@inline
	def matApply(m: Array[Double], width: Int, height: Int, v: Array[Double]): Array[Double] = {
		require(v.length == width, v.length + " !=  " + width);
		
		val res = Array.ofDim[Double](height);
		
		var i = 0; // idx of the line
		var j = 0; // idx of the position in m
		while(i < height) {
			var tmp = 0.0;
			
			var k = 0; // idx of the column
			while(k < width) {
				tmp += m(j) * v(k);
				
				k += 1;
				j += 1;
			}
			
			res(i) = tmp;
			
			i += 1;
		}
		
		res;
	}
	
	/** Multiplies a vector by the transposed of a matrix
	*
	* @param m the 2D array representing the matrix
	* @param v the array representing the vector
	* @return the array representing the result of multiplying the transposed of m and v: m.t * v
	*/
	@inline
	def matApplyTransposed(m: Array[Array[Double]], v: Array[Double]): Array[Double] = {
		//if(m.length != v.length) throw IncompatibleSizes;
		require(m.length == v.length);
		
		val nbRows = m.length;
		val nbCols = m.head.length;
		
		val res = Array.ofDim[Double](nbCols);
		
		var i = 0;
		while(i < nbRows) {
			val mRow = m(i);
			
			var j = 0;
			while(j < nbCols) {
				res(j) += mRow(j) * v(i);
				
				j += 1;
			}
			
			i += 1;
		}

		res;
	}
	
	
	/** Multiplies a vector by the transposed of a matrix
	*
	* @param m the array representing the matrix
	* @param width the width of the matrix
	* @param height the height of the matrix
	* @param v the array representing the vector
	* @return the array representing the result of multiplying the transposed of m and v: m.t * v
	*/
	@inline
	def matApplyTransposed(m: Array[Double], width: Int, height: Int, v: Array[Double]): Array[Double] = {
		require(v.length == height, v.length + " !=  " + height);
		
		val res = Array.ofDim[Double](width);
		
		var i = 0; // idx of the line
		var j = 0; // idx of the position in m
		while(i < height) {
			var k = 0; // id of the column
			while(k < width) {
				res(k) += m(j) * v(i);
				
				k += 1;
				j += 1;
			}
			
			i += 1;
		}

		res;
	}
	
	/** Compute the dot product of two arrays
	*
	* @param v1 an array
	* @param v2 an array
	* @return the dot product of v1 and v2
	*/
	@inline
	def dotProduct(v1: Array[Double], v2: Array[Double]): Double = {
		//if(v1.length != v2.length) throw IncompatibleSizes;
		require(v1.length == v2.length, v1.length + " != " + v2.length);
		
		var res: Double = 0;
		
		var i = 0;
		while(i < v1.length) {
			res += v1(i) * v2(i);
			
			i += 1;
		}
		
		res;
	}
	
	/** Compute the tensor product (AKA "outer product") of two arrays
	*
	* @param v1 an array
	* @param v2 an array
	* @return the 2D array representing tensor product of v1 and v2
	*/
	def tensorProduct2D(v1: Array[Double], v2: Array[Double]): Array[Array[Double]] = {
		val nbRows = v1.length;
		val nbCols = v2.length;
		
		val res = Array.ofDim[Double](nbRows, nbCols);
		
		var i = 0;
		while(i < nbRows) {
			val resRow = res(i);
			
			var j = 0;
			while(j < nbCols) {
				resRow(j) = v1(i) * v2(j);
				
				j+= 1;
			}
			
			i += 1;
		}
		
		res;
	}
	
	/** Compute the tensor product (AKA "outer product") of two arrays
	*
	* @param v1 an array
	* @param v2 an array
	* @return the array representing the tensor product of v1 and v2
	*/
	def tensorProductMat(v1: Array[Double], v2: Array[Double]): Array[Double] = {
		val nbRows = v1.length;
		val nbCols = v2.length;
		
		val res = Array.ofDim[Double](nbRows * nbCols);
		
		var i = 0; // idx of the line
		var j = 0; // idx of the position in m
		while(i < nbRows) {
			var k = 0; // idx of the column
			while(k < nbCols) {
				res(j) = v1(i) * v2(k);
				
				k += 1;
				j += 1;
			}
			
			i += 1;
		}
		
		res;
	}
	
	/** Matrix-matrix multiplication
	*
	* @param m1 a 2D array representing a matrix
	* @param m2 a 2D array representing a matrix
	* @return the 2D array representing the product of m1 and m2
	*/
	def matMultiply(m1: Array[Array[Double]], m2: Array[Array[Double]]): Array[Array[Double]] = {
		//if(m1.head.length != m2.length) throw IncompatibleSizes;
		require(m1.head.length == m2.length);
		
		val nbRows = m1.length;
		val nbCols = m2.head.length;
		val depth = m2.length;
		
		val res = Array.ofDim[Double](nbRows, nbCols);
		
		var i = 0;
		while(i < nbRows) {
			val m1Row = m1(i);
			val resRow = res(i);
			
			var j = 0;
			while(j < nbCols) {
				var k = 0;
				while(k < depth) {
					resRow(j) += m1Row(k) * m2(k)(j);
					
					k += 1;
				}
				
				j += 1;
			}
			
			i += 1;
		}
		
		res;
	}
	
	/** Matrix addition
	*
	* @param m1 a 2D array representing a matrix
	* @param m2 a 2D array representing a matrix
	* @return the 2D array representing the sum of m1 and m2
	*/
	@inline
	def add(m1: Array[Array[Double]], m2: Array[Array[Double]]): Array[Array[Double]] = {
		//if(m1.length != m2.length || m1.head.length != m2.head.length) throw IncompatibleSizes;
		require(m1.length == m2.length);
		
		val nbRows = m1.length;
		
		val res = Array.ofDim[Array[Double]](nbRows);
		
		var i = 0;
		while(i < nbRows) {
			res(i) = add(m1(i), m2(i));
			
			i += 1;
		}
		
		res;
	}
	
	
	/** Matrix addition (inplace)
	*
	* @param m1 a 2D array representing a matrix (modified)
	* @param m2 a 2D array representing a matrix
	*/
	@inline
	def addTo(m1: Array[Array[Double]], m2: Array[Array[Double]]): Unit = {
		//if(m1.length != m2.length || m1.head.length != m2.head.length) throw IncompatibleSizes;
		require(m1.length == m2.length);
		
		val nbRows = m1.length;
		
		var i = 0;
		while(i < nbRows) {
			addTo(m1(i), m2(i));
			
			i += 1;
		}
	}
	
	
	/** Matrix-matrix substraction
	*
	* @param m1 a 2D array representing a matrix (modified)
	* @param m2 a 2D array representing a matrix
	*/
	@inline
	def substractTo(m1: Array[Array[Double]], m2: Array[Array[Double]]): Unit = {
		//if(m1.length != m2.length || m1.head.length != m2.head.length) throw IncompatibleSizes;
		require(m1.length == m2.length);
		
		val nbRows = m1.length;
		
		var i = 0;
		while(i < nbRows) {
			substractTo(m1(i), m2(i));
			
			i += 1;
		}
	}
	
	/** Vector addition
	*
	* @param v1 an array representing a vector
	* @param v2 an array representing a vector
	* @return the array representing the sum of v1 and v2
	*/
	@inline
	def add(v1: Array[Double], v2: Array[Double]): Array[Double] = {
		//if(v1.length != v2.length) throw IncompatibleSizes;
		require(v1.length == v2.length);
		
		val l = v1.length;
		
		val res = Array.ofDim[Double](l); // TODO it would be probably faster to copy v1 with java.util.Arrays.copyOfRange
		
		var i = 0;
		while(i < l) {
			res(i) = v1(i) + v2(i);
			
			i += 1;
		}
		
		res;
	}
	
	/** Vector addition (inplace)
	*
	* @param v1 an array representing a vector (modified)
	* @param v2 an array representing a vector
	*/
	@inline
	def addTo(v1: Array[Double], v2: Array[Double]): Unit = {
		//if(v1.length != v2.length) throw IncompatibleSizes;
		require(v1.length == v2.length);
		
		var i = 0;
		while(i < v1.length) {
			v1(i) += v2(i);
			
			i += 1;
		}
	}

	// TODO à unit tester
	/** Vector partial addition (inplace)
	*
	* @param v1 an array representing a vector (modified)
	* @param pos the index indicating the begging of the part of v1 to be added to v2
	* @param v2 an array representing a vector
	*/
	@inline
	def addTo(v1: Array[Double], pos: Int, v2: Array[Double]): Unit = {
		var i = pos;
		while(i < v1.length) {
			v1(i) += v2(i - pos);
			
			i += 1;
		}
	}	

	/** Vector partial addition (inplace)
	*
	* @param v1 an array representing a vector (modified)
	* @param v2 an array representing a vector
	* @param pos the index indicating the beginning of the part of v2 to add to v1
	*/
	@inline
	def addTo(v1: Array[Double], v2: Array[Double], pos: Int): Unit = {
		var i = 0;
		while(i < v1.length) {
			v1(i) += v2(pos + i);
			
			i += 1;
		}
	}
	
	// TODO à unit tester
	/** Vector partial addition (inplace)
	*
	* @param v1 an array representing a vector (modified)
	* @param pos1 the index indicating the begging of the part of v1 to be added to v2
	* @param v2 an array representing a vector
	* @param pos2 the index indicating the beginning of the part of v2 to add to v1
	*/
	@inline
	def addTo(v1: Array[Double], pos1: Int, v2: Array[Double], pos2: Int): Unit = {
		var i = pos1;
		while(i < v1.length) {
			v1(i) += v2(pos2 + i);
			
			i += 1;
		}
	}
	
	// TODO à unit tester
	/** Vector partial addition (inplace)
	*
	* @param v1 an array representing a vector (modified)
	* @param pos1 the index indicating the begging of the part of v1 to be added to v2
	* @param v2 an array representing a vector
	* @param pos2 the index indicating the beginning of the part of v2 to add to v1
	* @param length the length of the part of v2 to add to v1
	*/
	@inline
	def addTo(v1: Array[Double], pos1: Int, v2: Array[Double], pos2: Int, length: Int): Unit = {
		var i = pos1;
		while(i < (pos1 + length)) {
			v1(i) += v2(pos2 + i);
			
			i += 1;
		}
	}
	
	/** Vector-vector substraction
	*
	* @param v1 an array representing a vector
	* @param v2 an array representing a vector
	* @return the array representing the substraction of v1 and v2
	*/
	@inline
	def substract(v1: Array[Double], v2: Array[Double]): Array[Double] = {
		//if(v1.length != v2.length) throw IncompatibleSizes;
		require(v1.length == v2.length);
		
		val l = v1.length;
		
		val res = Array.ofDim[Double](l);
		
		var i = 0;
		while(i < l) {
			res(i) = v1(i) - v2(i);
			
			i += 1;
		}
		
		res;
	}
	
	/** Vector-vector substraction (inplace)
	*
	* @param v1 an array representing a vector (modified)
	* @param v2 an array representing a vector
	*/
	@inline
	def substractTo(v1: Array[Double], v2: Array[Double]): Unit = {
		//if(v1.length != v2.length) throw IncompatibleSizes;
		require(v1.length == v2.length);
		
		var i = 0;
		while(i < v1.length) {
			v1(i) -= v2(i);
			
			i += 1;
		}
	}
	
	/** Vector addition with multiplicative coefficient (inplace)
	*
	* @param v1 an array representing a vector (modified)
	* @param v2 an array representing a vector
	* @param c the multiplicative coefficient applied to v2
	*/
	@inline
	def addToWeighted(v1: Array[Double], v2: Array[Double], c: Double): Unit = {
		//if(v1.length != v2.length) throw IncompatibleSizes;
		require(v1.length == v2.length);
		
		var i = 0;
		while(i < v1.length) {
			v1(i) += c * v2(i);
			
			i += 1;
		}
	}
	
	/** Equates an array to another (inplace)
	*
	* @param v1 an array (modified)
	* @param v2 an array
	*/
	@inline
	def equateTo[@specialized(Int, Double) T](v1: Array[T], v2: Array[T]): Unit = { // TODO it would probably be faster to use System.arraycopy
		var d = 0;
		while(d < v1.length) {
			v1(d) = v2(d);
			
			d += 1;
		}
	}
	
	/** Equates an 2D array to another (inplace)
	*
	* @param v1 a 2D array (modified)
	* @param v2 a 2D array
	*/
	@inline
	def equateTo[@specialized(Int, Double) T](m1: Array[Array[T]], m2: Array[Array[T]]): Unit = {
		val nbRows = m1.length;
		
		var i = 0;
		while(i < nbRows) {
			equateTo(m1(i), m2(i));
			
			i += 1;
		}
	}
	
	/** Adagrad update for 2D array (inplace)
	*
	* @param m the 2D array of coefficients (modified)
	* @param mHist the adagrad history (modified)
	* @param mGrad the gradient
	* @param adagrad the adagrad parameter
	* @param fudgeFactor the fudge factor
	*/
	@inline
	def adaUpdate(m: Array[Array[Double]], mHist: Array[Array[Double]], mGrad: Array[Array[Double]], adagrad: Double, fudgeFactor: Double): Unit = {
		var i = 0;
		while(i < m.length) {
			val line = m(i);
			val lHist = mHist(i);
			val lGrad = mGrad(i);
			
			var j = 0;
			while(j < line.length) {
				lHist(j) += square(lGrad(j));
				
				line(j) += adagrad * lGrad(j) / (fudgeFactor + math.sqrt(lHist(j)));
				
				j += 1;
			}
			
			i += 1;
		}
	}
	
	/** Adagrad update for array (inplace)
	*
	* @param m the array of coefficients (modified)
	* @param mHist the adagrad history (modified)
	* @param mGrad the gradient
	* @param adagrad the adagrad parameter
	* @param fudgeFactor the fudge factor
	*/
	@inline
	def adaUpdate(t: Array[Double], tHist: Array[Double], tGrad: Array[Double], adagrad: Double, fudgeFactor: Double): Unit = {
		var i = 0;
		while(i < t.length) {
			tHist(i) += square(tGrad(i));
		
			t(i) += adagrad * tGrad(i) / (fudgeFactor + math.sqrt(tHist(i)));
			
			i += 1;
		}
	}
	
	/** Addition of optional arrays
	*
	* @param o1 an Option[Array[Double]]
	* @param o2 an Option[Array[Double]]
	* @return the Option[Array[Double]] representing the addition of o1 and o2
	*/
	// Add two Option[Array[Double]]
	@inline
	def add(o1: Option[Array[Double]], o2: Option[Array[Double]]): Option[Array[Double]] = (o1, o2) match {
		case (Some(v1), Some(v2)) => Some(add(v1, v2));
		case (Some(v1), None) => Some(v1.clone);
		case (None, Some(v2)) => Some(v2.clone);
		case (None, None) => None;
	
	}
	
	/** Substraction of optional arrays
	*
	* @param o1 an Option[Array[Double]]
	* @param o2 an Option[Array[Double]]
	* @return the Option[Array[Double]] representing the substraction of o1 and o2
	*/
	// Add two Option[Array[Double]]
	@inline
	def substract(o1: Option[Array[Double]], o2: Option[Array[Double]]): Option[Array[Double]] = (o1, o2) match {
		case (Some(v1), Some(v2)) => Some(substract(v1, v2));
		case (Some(v1), None) => Some(v1.clone);
		case (None, Some(v2)) => Some(negate(v2));
		case (None, None) => None;
	
	}
	
	/** Adds an array to an optional array
	*
	* @param o1 an Option[Array[Double]]
	* @param v2 an array
	* @return the addition of v2 to o1
	*/
	@inline
	def add(o1: Option[Array[Double]], v2: Array[Double]): Array[Double] = o1 match {
		case Some(v1) => add(v1, v2);
		case None => v2.clone;
	}
	
	/** Substracts an array to an optional array
	*
	* @param o1 an Option[Array[Double]]
	* @param v2 an array
	* @return the substraction of v2 to o1
	*/
	@inline
	def substract(o1: Option[Array[Double]], v2: Array[Double]): Array[Double] = o1 match {
		case Some(v1) => substract(v1, v2);
		case None => negate(v2);
	}
	
	/** Tensor addition (inplace)
	*
	* @param m1 a 3D array representing a tensor (modified)
	* @param m2 a 3D array representing a tensor
	*/
	@inline
	def addTo(t1: Array[Array[Array[Double]]], t2: Array[Array[Array[Double]]]): Unit = {
		//if(t1.length != t2.length) throw IncompatibleSizes;
		require(t1.length == t2.length);
		
		val depth = t1.length;
		
		var i = 0;
		while(i < depth) {
			addTo(t1(i), t2(i));
			
			i += 1;
		}
	}
	
	/** Tensor substraction (inplace)
	*
	* @param m1 a 3D array representing a tensor (modified)
	* @param m2 a 3D array representing a tensor
	*/
	@inline
	def substractTo(t1: Array[Array[Array[Double]]], t2: Array[Array[Array[Double]]]): Unit = {
		//if(t1.length != t2.length) throw IncompatibleSizes;
		require(t1.length == t2.length);
		
		val depth = t1.length;
		
		var i = 0;
		while(i < depth) {
			substractTo(t1(i), t2(i));
			
			i += 1;
		}
	}
	
	/** Opposite of a sequence
	*
	* @param v an indexed sequence of Double
	* @return the array resulting from taking the opposite of all values of v
	*/
	@inline
	def negate(v: IndexedSeq[Double]): Array[Double] = {
		val l = v.length;
		
		val res = Array.ofDim[Double](l);
		
		var i = 0;
		while(i < l) {
			res(i) = -v(i);
			
			i += 1;
		}
		
		res;
	}
	
	/** Opposite of an array (inplace)
	*
	* @param v an array of Double (modified)
	*/
	@inline
	def negateTo(v: Array[Double]): Unit = {
		val l = v.length;
		
		var i = 0;
		while(i < l) {
			v(i) *= -1;
			
			i += 1;
		}
	}
	
	/** Adds two pairs of integers
	*
	* @param p a pair of integers
	* @param q a pair of integers
	* @return the sum (element-wise) of p and q
	*/
	@inline
	def add(p: (Int, Int), q: (Int, Int)): (Int, Int) = ((p._1 + q._1), (p._2 + q._2));
	
	/** Adds two triplets of integers
	*
	* @param p a triplet of integers
	* @param q a triplet of integers
	* @return the sum (element-wise) of p and q
	*/
	@inline
	def add(p: (Int, Int, Int), q: (Int, Int, Int)): (Int, Int, Int) = ((p._1 + q._1), (p._2 + q._2), (p._3 + q._3));
	
	/** Adds pairs of integers
	*
	* @param ps a sequent of pairs of integers
	* @return the sum (element-wise) of the pairs in ps
	*/
	@inline
	def sum(ps: (Int, Int)*): (Int, Int) = ps.reduce[(Int, Int)](add);

	/** Zip two lists of potentially different size and also returns the additional elements
	*
	* @param l1 a list
	* @param l2 a list
	* @return a triplet consisting of l1 zipped with l2, the remaining elements of l1, the remaining elements of l2
	*/
	def zip[A,B](l1: List[A], l2: List[B]): (List[(A,B)], List[A], List[B]) = {
		val tmp = mutable.ListBuffer[(A,B)]();
		
		var remaining1 = l1;
		var remaining2 = l2;

		while(remaining1.nonEmpty && remaining2.nonEmpty) {
			tmp += ((remaining1.head, remaining2.head));
			remaining1 = remaining1.tail;
			remaining2 = remaining2.tail;
		}

		(tmp.toList, remaining1, remaining2);
	}
}
