// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neuralNetwork;

import annotation.tailrec;
import collection._;

class MatrixLayer(val p: MatrixLayerParam, val nonLinearity: Option[NonLinearity with Differentiable]) extends Layer {
	def apply(in: Array[Double]): Array[Double] = nonLinearity match {
		case Some(f) =>	f(Math.add(Math.matApply(p.m, p.width, p.height, in), p.t));
		case None => Math.add(Math.matApply(p.m, p.width, p.height, in), p.t);
	}
	
	def gradient(in: Array[Double]): (Array[Double], (Array[Double] => (MatrixLayerParam, Array[Double]))) = nonLinearity match { // (out[i], (dc/dout[i] => (dc/dparam, dc/din[i])))
		case Some(f) => {
			val u = Math.add(Math.matApply(p.m, p.width, p.height, in), p.t);
			
			val dF = (deltaV: Array[Double]) => {
				val tmp = f.derivative(u, deltaV); // dc/d(A.in+B)[i]
				
				val dP = MatrixLayerParam(Math.tensorProductMat(tmp, in), tmp); // dc/dparam
				val dIn = Math.matApplyTransposed(p.m, p.width, p.height, tmp); // dc/din[i]
				
				(dP, dIn);
			};
			
			(f(u), dF);
		}
		
		case None => protoGradient(in);
	}
	
	// Here the gradient does not take the non-linearity into account (as if f.derivative = 1)
	def protoGradient(in: Array[Double]): (Array[Double], (Array[Double] => (MatrixLayerParam, Array[Double]))) = { // (out[i], (dc/dout[i] => (dparam, dc/din[i])))
		val out = nonLinearity match {
			case Some(f) => f(Math.add(Math.matApply(p.m, p.width, p.height, in), p.t));
			case None => Math.add(Math.matApply(p.m, p.width, p.height, in), p.t);
		}
		
		val dF = (deltaV: Array[Double]) => {
			val dP = MatrixLayerParam(Math.tensorProductMat(deltaV, in), deltaV); // dc/dparam
			val dIn = Math.matApplyTransposed(p.m, p.width, p.height, deltaV); // dc/din[i]
			
			(dP, dIn);
		};
		
		(out, dF);
	}
	
	// A copy of the layer with a copy of the parameter
	def deepCopy: MatrixLayer = new MatrixLayer(p.deepCopy, nonLinearity);
	
	// A copy of the layer with q (not a copy) as parameter
	def duplicate(q: ParameterBrick): MatrixLayer = q match {
		case q: MatrixLayerParam => new MatrixLayer(q, nonLinearity);
		case _ => {throw IncompatibleParameters; this;}
	}
	
	// Custom serialization
	override def customSerialize(stringBuilder: mutable.StringBuilder): Unit = {
		stringBuilder ++= "MatrixLayer\n";
		
		structureHandlers.ArrayHandler.customSerialize(p.m, stringBuilder);
		structureHandlers.ArrayHandler.customSerialize(p.t, stringBuilder);
		
		stringBuilder ++= Serializer(nonLinearity) + '\n';
	}
	
	override def toString: String = "MatrixLayer((" + p.height + ", " + p.width + "), " + nonLinearity + ")";
}

object MatrixLayer {
	def apply(m: Array[Double], t: Array[Double], nonLinearity: Option[NonLinearity with Differentiable] = None): MatrixLayer = new MatrixLayer(MatrixLayerParam(m, t), nonLinearity);
	
	def apply(m: Array[Double], t: Array[Double], f: NonLinearity with Differentiable): MatrixLayer = new MatrixLayer(MatrixLayerParam(m, t), Some(f));
	
	// Custom deserialization
	def read(data: Array[String], i: Int): (MatrixLayer, Int) = {
		var j = i;
		
		// read p
		val m = {
			val tmp = structureHandlers.ArrayHandler.read(data, j);
			j = tmp._2;
			
			tmp._1;
		}
		
		val t = {
			val tmp = structureHandlers.ArrayHandler.read(data, j);
			j = tmp._2;
			
			tmp._1;
		}
		
		val p = MatrixLayerParam(m, t);
		
		// read the non-linearity
		val nonLinearity = StringToFunction(data(j));
		j += 1;
		
		val layer = new MatrixLayer(p, nonLinearity);
		
		(layer, j);
	}
}

case class MatrixLayerParam(val m: Array[Double], val t: Array[Double]) extends LayerParam {
	val height: Int = t.length; // Size of the output
	val width: Int = (m.length / height); // Size of the input
	
//	@inline
//	def apply(in: Array[Double]): Array[Double] = {
//		require(in.length == width, in.length + " !=  " + width);
//		
//		val res = Array.ofDim[Double](height);
//		
//		var i = 0; // idx of the line
//		var j = 0; // idx of the position in m
//		while(i < height) {
//			var tmp = t(i);
//			
//			var k = 0; // idx of the column
//			while(k < width) {
//				tmp += m(j) * in(k)
//				
//				k += 1;
//				j += 1;
//			}
//			
//			res(i) = tmp;
//			
//			i += 1;
//		}
//		
//		res;
//	}
	
	// Add p to this (inplace)
	def addTo(p: ParameterBrick): Unit = p match {
		case MatrixLayerParam(m2, t2) => {
			Math.addTo(m, m2);
			Math.addTo(t, t2);
		}
		
		case _ => throw IncompatibleParameterBricks("addTo", List(this, p));
	}
	
	// Substract p to this (inplace)
	def substractTo(p: ParameterBrick): Unit = p match {
		case MatrixLayerParam(m2, t2) => {
			Math.substractTo(m, m2);
			Math.substractTo(t, t2);
		}
		
		case _ => throw IncompatibleParameterBricks("substractTo", List(this, p));
	}
	
	// inplace
	def multiplyTo(x: Double): Unit = {
		Math.multiplyTo(m, x);
		Math.multiplyTo(t, x);
	}
	
	def multiply(x: Double): MatrixLayerParam = MatrixLayerParam(Math.multiply(m, x), Math.multiply(t, x));
	
	def equateTo(p: ParameterBrick): Unit = p match {
		case MatrixLayerParam(m2, t2) => {
			Math.equateTo(m, m2);
			Math.equateTo(t, t2);
		}
		
		case _ => throw IncompatibleParameterBricks("equateTo", List(this, p));
	}
	
	def deepCopy: MatrixLayerParam = MatrixLayerParam(m.clone, t.clone);
	
	def adaUpdate(hist: ParameterBrick, grad: ParameterBrick, adagrad: Double, fudgeFactor: Double): Unit = (hist, grad) match {
		case (MatrixLayerParam(mHist, tHist), MatrixLayerParam(mGrad, tGrad)) => {
			Math.adaUpdate(m, mHist, mGrad, adagrad, fudgeFactor);
			Math.adaUpdate(t, tHist, tGrad, adagrad, fudgeFactor);
		}
		
		case _ => throw IncompatibleParameterBricks("adaUpdate", List(this, hist, grad));
	}
	
	def norm1: Double = structureHandlers.ArrayHandler.norm1(m) + structureHandlers.ArrayHandler.norm1(t);
	
	def norm2Sq: Double = structureHandlers.ArrayHandler.norm2Sq(m) + structureHandlers.ArrayHandler.norm2Sq(t);
	//lazy val norm2Sq_memo: Double = norm2Sq;
	
	def empty: MatrixLayerParam = MatrixLayerParam(Array.ofDim[Double](m.length), Array.ofDim[Double](t.length));
	
	def nanScan: Boolean = structureHandlers.ArrayHandler.nanScan(m) && structureHandlers.ArrayHandler.nanScan(t);
	
	// sum and max (in abs) + number of params
	def stats: (Double, Double, Int) = {
		val statsM = structureHandlers.ArrayHandler.stats(m);
		val statsT = structureHandlers.ArrayHandler.stats(t);
		
		((statsM._1 + statsT._1), math.max(statsM._2, statsT._2), (statsM._3 + statsT._3));
	}
	
	// Get the value of the coeff at index id
	def getCoeff(id: Int): Double = {
		val tmp = width * height;
		if(id < tmp) m(id);
		else t(id - tmp);
	}
	
	// Add x to the coeff at index id
	def addCoeff(id: Int, x: Double): Unit = {
		val tmp = width * height;
		if(id < tmp) m(id) += x;
		else t(id - tmp) += x;
	}
	
	// Inplace
	def clip(f: (Double => Double)): Unit = {
		structureHandlers.ArrayHandler.clip(m, f);
		structureHandlers.ArrayHandler.clip(t, f);
	}
	
	override def toString: String = {
		val str = new StringBuilder;
		
		str ++= "MatrixLayerParam (" + height + ", " + width + ")";
		str ++= "\n" + m.mkString(", ");
		str ++= "\n" + t.mkString(", ");
		
		str.toString;
	}
}
