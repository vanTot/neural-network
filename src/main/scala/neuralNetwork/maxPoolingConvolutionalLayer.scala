// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neuralNetwork;

import annotation.tailrec;
import collection._;

class MaxPoolingConvolutionalLayer(
	val filters: Array[Layer], // The filters should all have the same width (input size, filtersXSize * filtersYSize * pixelSize) and have a height of 1 (output size)
	val filtersXSize: Int, // X-size of the filters in pixels
	val filtersYSize: Int, // Y-size of the filters in pixels
	val pixelSize: Int, // Number of doubles per pixel in the input
	val inputX: Int, // X-size of the input in pixels
	val inputY: Int, // Y-size of the input in pixels
	val poolingX: Int, // X-size of the pooling
	val poolingY: Int // Y-size of the pooling
) extends Layer {
	val nbFilters = filters.length;
	
	val nbPassesX = (inputX - filtersXSize + 1);
	val nbPassesY = (inputY - filtersYSize + 1);
	
	val p = {
		val height = (nbPassesX * nbPassesY * nbFilters) / (poolingX * poolingY);
		val width = inputX * inputY * pixelSize;
		
		MaxPoolingConvolutionalLayerParam(filters.map(_.p), height, width);
	}
	
	def apply(in: Array[Double]): Array[Double] = {
		val output = Array.fill((nbPassesX * nbPassesY * nbFilters) / (poolingX * poolingY))(Double.NegativeInfinity);
		
		val subIns = generateSubIns(in);
		
		var i = 0; // Y axis (image)
		while(i < (nbPassesY / poolingY)) {
			var j = 0; // X axis (image)
			while(j < (nbPassesX / poolingX)) {
				val offset = ((i * (nbPassesX / poolingX)) + j) * nbFilters; // Offset in output
				
				var y = 0;
				while(y < poolingY) {
					val offset2 = (((i * poolingY) + y) * nbPassesX) + (j * poolingX); // Offset in subIns
					var x = 0;
					while(x < poolingX) {
						val subIn = subIns(offset2 + x);
						
						var k = 0;
						while(k < nbFilters) {
							val tmp = filters(k)(subIn);
							val v = tmp.head;
							if(v > output(offset + k)) output(offset + k) = v;
							
							k += 1;
						}
						
						x += 1;
					}
					
					y += 1;
				}
				
				j += 1;
			}
			
			i += 1;
		}
		
		output;
	}
	
	def generateSubIns(in: Array[Double]): Array[Array[Double]] = {
		val subIns = Array.ofDim[Array[Double]](nbPassesX * nbPassesY);
		
		var j = 0; // X axis (image)
		while(j < nbPassesX) {
			var prev = Array.ofDim[Double](filtersXSize * filtersYSize * pixelSize);
			
			{var y = 0; // Y axis (filter)
			while(y < filtersYSize) {
				val offset = ((y * inputX) + j) * pixelSize; // Offset in in
				val offset2 = (y * filtersXSize * pixelSize); // Offset in subIn
				
				var k = 0;
				while(k < (filtersXSize * pixelSize)) {
					prev(offset2 + k) = in(offset + k);
					
					k += 1;
				}
				
				y += 1;
			}}
			
			subIns(j) = prev;
			
			var i = 1; // Y axis (image)
			while(i < nbPassesY) {
				val subIn = Array.ofDim[Double](filtersXSize * filtersYSize * pixelSize);
				System.arraycopy(prev, (filtersXSize * pixelSize), subIn, 0, ((filtersYSize - 1) * filtersXSize * pixelSize));
				
				val offset = ((((filtersYSize - 1) + i) * inputX) + j) * pixelSize; // Offset in in
				val offset2 = ((filtersYSize - 1) * filtersXSize * pixelSize); // Offset in subIn
				
				var k = 0;
				while(k < (filtersXSize * pixelSize)) {
					subIn(offset2 + k) = in(offset + k);
					
					k += 1;
				}
				
				subIns((i * nbPassesX) + j) = subIn;
				prev = subIn;
				
				i += 1;
			}
			
			j += 1;
		}
		
		subIns;
	}
	
	def gradient(in: Array[Double]): (Array[Double], (Array[Double] => (MaxPoolingConvolutionalLayerParam, Array[Double]))) = {
		val output = Array.fill((nbPassesX * nbPassesY * nbFilters) / (poolingX * poolingY))(Double.NegativeInfinity);
		
		val gradFs = Array.ofDim[(Int, Int, (Array[Double] => (LayerParam, Array[Double])))]((nbPassesX * nbPassesY * nbFilters) / (poolingX * poolingY));
		
		val subIns = generateSubIns(in);
		
		var i = 0; // Y axis (image)
		while(i < (nbPassesY / poolingY)) {
			var j = 0; // X axis (image)
			while(j < (nbPassesX / poolingX)) {
				val offset = ((i * (nbPassesX / poolingX)) + j) * nbFilters; // Offset in output
				
				var y = 0;
				while(y < poolingY) {
					val offset2 = (((i * poolingY) + y) * nbPassesX) + (j * poolingX); // Offset in subIns
					var x = 0;
					while(x < poolingX) {
						val subIn = subIns(offset2 + x);
						
						var k = 0;
						while(k < nbFilters) {
							val tmp = filters(k).gradient(subIn);
							val v = tmp._1.head;
							if(v > output(offset + k)) {
								output(offset + k) = v;
								gradFs(offset + k) = (x, y, tmp._2);
							}
							
							k += 1;
						}
						
						x += 1;
					}
					
					y += 1;
				}
				
				j += 1;
			}
			
			i += 1;
		}
		
		val dF = ((deltaV: Array[Double]) => {
			val gradLayers = filters.map(_.p.empty);
			val dIn = Array.ofDim[Double](inputX * inputY * pixelSize);
			
			var i = 0; // Y axis (image)
			while(i < (nbPassesY / poolingY)) {
				var j = 0; // X axis (image)
				while(j < (nbPassesX / poolingX)) {
					val offset = ((i * (nbPassesX / poolingX)) + j) * nbFilters; // Offset in output
					
					var k = 0;
					while(k < nbFilters) {
						val tmpGradF = gradFs(offset + k);
						val x0 = tmpGradF._1;
						var y0 = tmpGradF._2;
						val tmp = tmpGradF._3(Array(deltaV(offset + k)));
						
						gradLayers(k).addTo(tmp._1);
						val dSubIn = tmp._2;
						
						// Redistribution of dSubIn in dIn
						{var y = 0;
						while(y < filtersYSize) {
							val offset2 = (((i * poolingY) + (y0 + y)) * inputX + (j * poolingX) + x0) * pixelSize; // Offset in dIn
							val offset3 = y * (filtersXSize * pixelSize); // Offset in dSubIn
							
							var l = 0;
							while(l < (filtersXSize * pixelSize)) {
								dIn(offset2 + l) = dSubIn(offset3 + l);
								
								l += 1;
							}
							
							y += 1;
						}}
						
						k += 1;
					}
					
					j += 1;
				}
				
				i += 1;
			}
			
			(MaxPoolingConvolutionalLayerParam(gradLayers, p.height, p.width), dIn);
		});
		
		(output, dF);
	}
	
	// Here the gradient does not take the non-linearity into account (as if f.derivative = 1)
	def protoGradient(in: Array[Double]): (Array[Double], (Array[Double] => (LayerParam, Array[Double]))) = gradient(in); // (out[i], (dc/dout[i] => (dparam, dc/din[i])))
	
	// A copy of the layer with a copy of the parameter
	def deepCopy: MaxPoolingConvolutionalLayer = new MaxPoolingConvolutionalLayer(filters.map(_.deepCopy), filtersXSize, filtersYSize, pixelSize, inputX, inputY, poolingX, poolingY);
	
	// A copy of the layer with q (not a copy) as LayerParam
	def duplicate(q: ParameterBrick): MaxPoolingConvolutionalLayer = q match {
		case MaxPoolingConvolutionalLayerParam(layers, _, _) => new MaxPoolingConvolutionalLayer((filters, layers).zipped.map((x, y) => x.duplicate(y)), filtersXSize, filtersYSize, pixelSize, inputX, inputY, poolingX, poolingY);
		case _ => {throw IncompatibleParameters; this;}
	}
	
	// Custom serialization
	override def customSerialize(stringBuilder: mutable.StringBuilder): Unit = {
		stringBuilder ++= "MaxPoolingConvolutionalLayer\n";
		stringBuilder ++= filtersXSize.toString + '\n';
		stringBuilder ++= filtersYSize.toString + '\n';
		stringBuilder ++= pixelSize.toString + '\n';
		stringBuilder ++= inputX.toString + '\n';
		stringBuilder ++= inputY.toString + '\n';
		stringBuilder ++= poolingX.toString + '\n';
		stringBuilder ++= poolingY.toString + '\n';
		stringBuilder ++= nbFilters.toString + '\n';
		filters.foreach(_.customSerialize(stringBuilder));
	}
}

object MaxPoolingConvolutionalLayer {
	def apply(filters: Array[Layer], filtersXSize: Int, filtersYSize: Int, pixelSize: Int, inputX: Int, inputY: Int, poolingX: Int, poolingY: Int): MaxPoolingConvolutionalLayer = new MaxPoolingConvolutionalLayer(filters, filtersXSize, filtersYSize, pixelSize, inputX, inputY, poolingX, poolingY);
	
	// Custom deserialization
	def read(data: Array[String], i: Int): (MaxPoolingConvolutionalLayer, Int) = {
		var j = i;
		
		// read the parameters
		val filtersXSize = data(j).toInt;
		j += 1;
		val filtersYSize = data(j).toInt;
		j += 1;
		val pixelSize = data(j).toInt;
		j += 1;
		val inputX = data(j).toInt;
		j += 1;
		val inputY = data(j).toInt;
		j += 1;
		val poolingX = data(j).toInt;
		j += 1;
		val poolingY = data(j).toInt;
		j += 1;
		
		// read the filters
		val filters = {
			val tmp = Serializer.readLayers(data, j);
			j = tmp._2;
			
			tmp._1;
		}
		
		val layer = new MaxPoolingConvolutionalLayer(filters, filtersXSize, filtersYSize, pixelSize, inputX, inputY, poolingX, poolingY);
		
		(layer, j);
	}
}

case class MaxPoolingConvolutionalLayerParam(val layers: Array[LayerParam], val height: Int, val width: Int) extends LayerParam {
	def addTo(p: ParameterBrick): Unit = p match {
		case MaxPoolingConvolutionalLayerParam(layers2, _, _) => (layers, layers2).zipped.foreach((x, y) => x.addTo(y));
		case _ => throw IncompatibleParameterBricks("addTo", List(this, p));
	}
	
	def substractTo(p: ParameterBrick): Unit = p match {
		case MaxPoolingConvolutionalLayerParam(layers2, _, _) => (layers, layers2).zipped.foreach((x, y) => x.substractTo(y));	
		case _ => throw IncompatibleParameterBricks("substractTo", List(this, p));
	}
	
	def multiplyTo(x: Double): Unit = {
		layers.foreach(_.multiplyTo(x));
	}
	
	def multiply(x: Double): MaxPoolingConvolutionalLayerParam = MaxPoolingConvolutionalLayerParam(layers.map(_.multiply(x)), height, width);
	
	def equateTo(p: ParameterBrick): Unit = p match {
		case MaxPoolingConvolutionalLayerParam(layers2, _, _) => (layers, layers2).zipped.foreach((x, y) => x.equateTo(y));
		case _ => throw IncompatibleParameterBricks("equateTo", List(this, p));
	}
	
	def deepCopy: MaxPoolingConvolutionalLayerParam = MaxPoolingConvolutionalLayerParam(layers.map(_.deepCopy), height, width);
	
	def adaUpdate(hist: ParameterBrick, grad: ParameterBrick, adagrad: Double, fudgeFactor: Double): Unit = (hist, grad) match {
		case (MaxPoolingConvolutionalLayerParam(layersHist, _, _), MaxPoolingConvolutionalLayerParam(layersGrad, _, _)) => (layers, layersHist, layersGrad).zipped.foreach((l, lHist, lGrad) => l.adaUpdate(lHist, lGrad, adagrad, fudgeFactor));
		case _ => throw IncompatibleParameterBricks("adaUpdate", List(this, hist, grad));
	}
	
	def norm1: Double = layers.map(_.norm1).sum;
	
	def norm2Sq: Double = layers.map(_.norm2Sq).sum;
	//lazy val norm2Sq_memo: Double = norm2Sq;
	
	def empty: MaxPoolingConvolutionalLayerParam = MaxPoolingConvolutionalLayerParam(layers.map(_.empty), height, width);
	
	def nanScan: Boolean = layers.map(_.nanScan).reduce(_ & _);
	
	// sum and max (in abs) + number of params
	def stats: (Double, Double, Int) = {
		val tmp = layers.map(_.stats).unzip3;
		
		val sum = tmp._1.sum;
		val max = tmp._2.max;
		val nb = tmp._3.sum;
		
		(sum, max, nb);
	}
	
	// Get the value of the coeff at index id
	def getCoeff(id: Int): Double = {
		@tailrec
		def loop(count: Int, i: Int): Double = {
			val layerP = layers(i);
			val tmp = layerP.nbParams;
			if(tmp > count) layerP.getCoeff(count);
			else loop((count - tmp), (i + 1));
		}
		
		loop(id, 0);
	}
	
	// Add x to the coeff at index id
	def addCoeff(id: Int, x: Double): Unit = {
		@tailrec
		def loop(count: Int, i: Int): Unit = {
			val layerP = layers(i);
			val tmp = layerP.nbParams;
			if(tmp > count) layerP.addCoeff(count, x);
			else loop((count - tmp), (i + 1));
		}
		
		loop(id, 0);
	}
	
	// Inplace
	def clip(f: (Double => Double)): Unit = {
		layers.foreach(_.clip(f));
	}
}
