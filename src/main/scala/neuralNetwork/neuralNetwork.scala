// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neuralNetwork;

import annotation.tailrec;
import collection._;
import collection.immutable;
import collection.mutable;

object IncompatibleParameters extends Exception;

trait NeuralNetwork[@specialized(Double) TOut] extends java.io.Serializable {
	def height: Int; // Size of the output
	def width: Int; // Size of the input

	def param: List[ParameterBrick];

	def execute(in: Array[Double]): TOut;
}

trait SequentialNN[@specialized(Double) TOut] extends NeuralNetwork[TOut] {
	def layers: List[Layer];
	def param: List[ParameterBrick] = layers.map(_.p);

	val height: Int = layers.last.p.height; // Size of the output
	val width: Int = layers.head.p.width; // Size of the input
	
	// Returns the output as a single Array
	def executeArray(in: Array[Double]): Array[Double] = layers.foldLeft(in)((v: Array[Double], layer: Layer) => layer.apply(v));
	
	def test: Unit = {
		var vec = Array.ofDim[Double](width);
		for(i <- 0 until vec.length) vec(i) = (util.Random.nextDouble - 0.5); // Between -0.5 and 0.5
		
		var i = 0;
		var l = layers;
		while(l.nonEmpty) {
			Console.println(i + ": " + vec.mkString(", "));
			vec = l.head.apply(vec);
			
			i += 1;
			l = l.tail;
		}
		
		Console.println("out: " + vec.mkString(", "));
	}
}

// At some point, the Layer class should be made to extend LearningNeuralNetwork[Array[Double]]
trait Layer extends java.io.Serializable {
	def p: LayerParam;
	
	def apply(in: Array[Double]): Array[Double];
	def gradient(in: Array[Double]): (Array[Double], (Array[Double] => (LayerParam, Array[Double]))); // (out[i], (dc/dout[i] => (dparam, dc/din[i])))
	
	// Here the gradient does not take the non-linearity into account (as if f.derivative = 1)
	def protoGradient(in: Array[Double]): (Array[Double], (Array[Double] => (LayerParam, Array[Double]))); // (out[i], (dc/dout[i] => (dparam, dc/din[i])))
	
	// A copy of the layer with a copy of the parameter
	def deepCopy: Layer;
	
	// A copy of the layer with q (not a copy) as ParameterBrick
	def duplicate(q: ParameterBrick): Layer;
	
	// Custom serialization
	def customSerialize(stringBuilder: mutable.StringBuilder): Unit = ???
}

trait LayerParam extends ParameterBrick with java.io.Serializable {
	def height: Int; // Size of the output
	def width: Int; // Size of the input
	
	//def addTo(p: LayerParam): Boolean; // Add p to this (inplace)
	//def substractTo(p: LayerParam): Boolean; // Substract p to this (inplace)
	def multiplyTo(x: Double): Unit; // inplace
	def multiply(x: Double): LayerParam;
	
	//def equateTo(p: LayerParam): Boolean; // Equate this to p (inplace of course)
	def deepCopy: LayerParam;
	def empty: LayerParam; // New empty param
	
	//def adaUpdate(hist: LayerParam, grad: LayerParam, adagrad: Double, fudgeFactor: Double): Boolean;
	def norm1: Double;
	def norm2Sq: Double;
	//def norm2Sq_memo: Double;
	
	// sum and max (in abs) + number of params
	//def stats: (Double, Double, Int);
	
	//lazy val nbParams = stats._3;
	
	// Get the value of the coeff at index id
	//def getCoeff(id: Int): Double;
	
	// Add x to the coeff at index id
	//def addCoeff(id: Int, x: Double): Unit;
	
	// Inplace
	def clip(f: (Double => Double)): Unit;
}

// To have decoders (typically, LSTM-based one) Testable, outputAndGradient and executeArray must have an additional output, a sequence of decisions, that can be given as an additional argument to these two functions. For instance, a decoder would output the sequence of probabilities as the sequence of decisions.
trait TestableLearningSystem {
	// Returns the output as a single Array	as well as the gradient
	def outputAndGradient(in: Array[Double]): (Array[Double], Array[Double] => (List[ParameterBrick], Array[Double])); // (out, dc/dout => (List[dc/dparam], dc/din))

	// Returns the output as a single Array
	def executeArray(in: Array[Double]): Array[Double];

	def param: List[ParameterBrick];
	
	// sum and max (in abs) + number of params
	def stats: (Double, Double, Int) = {
		var sum = 0.0;
		var max = 0.0;
		var nb = 0;
		
		for(p <- param) {
			val tmp = p.stats;
			
			sum += tmp._1;
			if(tmp._2 > max) max = tmp._2;
			nb += tmp._3;
		}
		
		(sum, max, nb);
	}
	
	lazy val nbParams = stats._3;
	
	// Get the value of the coeff at index id in a given List[ParameterBrick]
	@tailrec
	protected final def getCoeffParam(l: List[ParameterBrick], count: Int): Double = {
		val p :: t = l;
		val tmp = p.nbParams;
		if(tmp > count) p.getCoeff(count);
		else getCoeffParam(t, (count - tmp));
	}
	
	// Add x to the coeff at index id of the system's parameters
	def addCoeff(id: Int, x: Double): Unit = {
		@tailrec
		def loop(l: List[ParameterBrick], count: Int): Unit = {
			val p :: t = l;
			val tmp = p.nbParams;
			if(tmp > count) p.addCoeff(count, x);
			else loop(t, (count - tmp));
		}
		
		loop(param, id);
	}

	// Size of the input when testing (the notion of width is not always defined, think about sequence encoders for example)
	protected def testWidth: Int;
	
	// Check the gradient using the fact that f'(in) = lim_{h -> 0} (f(in + h) - f(in))/h
	def testGradient(h: Double, threshold: Double): Boolean = testGradient((nbParams + testWidth), h, threshold, true);
	def testGradient(nbTest: Int, h: Double, threshold: Double, verbose: Boolean = false): Boolean = {
		val in = Array.fill(testWidth)(util.Random.nextDouble - 0.5);
		
		val tmp = outputAndGradient(in);
		val testHeight = tmp._1.length;
		val costV = Array.fill(testHeight)(util.Random.nextDouble - 0.5);

		val x = Math.dotProduct(costV, tmp._1);
		val grad = tmp._2(costV);
		
		@tailrec
		def loop(i: Int): Boolean = if(i < nbTest) {
			val id = util.Random.nextInt(nbParams + testWidth);
			if(id < nbParams) { // Test of parameters gradient
				addCoeff(id, h);
				val x2 = Math.dotProduct(costV, executeArray(in));
				addCoeff(id, -h);
				
				val y = getCoeffParam(grad._1, id); // Computed gradient
				val y2 = ((x2 - x) / h); // Approximation
				
				if(math.abs(y - y2) < threshold) loop(i + 1);
				else {if(verbose) {println(("parameter", y, y2, (y - y2)))}; false};
			}
			else { // Test of input gradient
				val id2 = id - nbParams;
				in(id2) += h;
				val x2 = Math.dotProduct(costV, executeArray(in));
				in(id2) -= h;

				val y = grad._2(id2); // Computed gradient
				val y2 = ((x2 - x) / h); // Approximation
				
				if(math.abs(y - y2) < threshold) loop(i + 1);
				else {if(verbose) {println(("input", y, y2, (y - y2)))}; false};
			}
		}
		else true;
		
		loop(0);
	}
}

trait LearningNeuralNetwork[@specialized(Double) TOut] extends NeuralNetwork[TOut] with TestableLearningSystem {
	// d is dc/dout[i]
	def gradient(in: Array[Double], d: TOut): (List[ParameterBrick], Array[Double]) = gradient(in)(d); // (List[dc/dparam], dc/din[i])
	
	def gradient(in: Array[Double]): (TOut => (List[ParameterBrick], Array[Double])); // (List[dc/dparam], dc/din[i])

	// A copy of the network with p (not a copy) as parameters
	def duplicate(p: List[ParameterBrick]): LearningNeuralNetwork[TOut] = duplicateFeed(p) match {
		case (x, Nil) => x;
		case _ => {throw IncompatibleParameters; this;}
	}
	
	// Similar to duplicate, but also returns the unused ParameterBrick
	def duplicateFeed(p: List[ParameterBrick]): (LearningNeuralNetwork[TOut], List[ParameterBrick]);

	def testWidth = width;
	
	def emptyParam: List[ParameterBrick] = param.map(_.empty);
	
	//def update(params: List[LayerParam]): Boolean = (layers, params).zipped.map((layer: Layer, p: LayerParam) => layer.p.update(p)).reduce(_ & _);
	def update(params: List[ParameterBrick]): Unit = (param, params).zipped.foreach((p: ParameterBrick, q: ParameterBrick) => p.update(q));
	
	//def adaUpdate(hist: List[LayerParam], grad: List[LayerParam], adagrad: Double, fudgeFactor: Double): Boolean = (layers, hist, grad).zipped.map((layer: Layer, pHist: LayerParam, pGrad: LayerParam) => layer.p.adaUpdate(pHist, pGrad, adagrad, fudgeFactor)).reduce(_ & _);
	def adaUpdate(hist: List[ParameterBrick], grad: List[ParameterBrick], adagrad: Double, fudgeFactor: Double): Unit = (param, hist, grad).zipped.foreach((p: ParameterBrick, pHist: ParameterBrick, pGrad: ParameterBrick) => p.adaUpdate(pHist, pGrad, adagrad, fudgeFactor));
	
	//def adaUpdate(hist: NeuralNetwork[TOut], grad: NeuralNetwork[TOut], adagrad: Double, fudgeFactor: Double): Boolean = (layers, hist.layers, grad.layers).zipped.map((layer: Layer, pHist: Layer, pGrad: Layer) => layer.p.adaUpdate(pHist.p, pGrad.p, adagrad, fudgeFactor)).reduce(_ & _);
	def adaUpdate(hist: NeuralNetwork[TOut], grad: NeuralNetwork[TOut], adagrad: Double, fudgeFactor: Double): Unit = (param, hist.param, grad.param).zipped.foreach((p: ParameterBrick, pHist: ParameterBrick, pGrad: ParameterBrick) => p.adaUpdate(pHist, pGrad, adagrad, fudgeFactor));
	
	def nanScan: Boolean = {
		var i = 0;
		for(brick <- param) {
			if(brick.nanScan) {
				Console.println("brick " + i);
				return true;
			}
			
			i += 1;
		}
		
		return false;
	}
}

trait SequentialLNN[@specialized(Double) TOut] extends LearningNeuralNetwork[TOut] with SequentialNN[TOut] {
	def outputAndGradient(in: Array[Double]): (Array[Double], Array[Double] => (List[LayerParam], Array[Double])) = { // (out, dc/dout => (List[dc/dparam], dc/din))
		// First pass
		val (v, tmp) = {
			var acc: List[(Array[Double] => (LayerParam, Array[Double]))] = Nil; // dc/dout => (dc/dparam, dc/dvn[i])
			var vn = in;
			var l = layers;
			while(!l.isEmpty) {
				val (vn2, dF) = l.head.gradient(vn);
				acc = (dF :: acc);
				
				vn = vn2;
				l = l.tail;
			}
			
			(vn, acc);
		}
		
		(
			v,
			((d: Array[Double]) => { // d is dc/dvn[i]
				// Second pass
				var acc: List[LayerParam] = Nil;
				
				var dvn = d; // dc/vn[i]
				var l = tmp;
				while(!l.isEmpty) {
					val dF = l.head;
					
					val (dParam, dvn2) = dF(dvn);
					
					acc = dParam :: acc;
					
					dvn = dvn2 // dc/dv(n-1)[i]
					l = l.tail;
				}
				
				(acc, dvn);
			})
		);
	}
}

object StringToFunction {
	def apply(str: String): Option[NonLinearity with Differentiable] = if(str == "None") None;
	else if(str == "ReQU") Some(ReQU);
	else if(str == "ReLU") Some(ReLU);
	else if(str == "SoftMax") Some(SoftMax);
	else if(str == "SoftQuadSafe") Some(SoftQuadSafe);
	else if(str.startsWith("SoftQuadSafe")) {
		// ex: "SoftQuadSafe_-1"
		val bias = str.drop(13).toDouble;
		
		Some(SoftQuadSafeBiased(bias));
	}
	else if(str == "SoftPlusSafe") Some(SoftPlusSafe);
	else if(str.startsWith("SoftPlusSafe")) {
		// ex: "SoftPlusSafe_-1"
		val bias = str.drop(13).toDouble;
		
		Some(SoftPlusSafeBiased(bias));
	}
	else if(str == "BiLog") Some(BiLog(1.0));
	else if(str.startsWith("BiLog")) {
		// ex: "BiLog_2"
		val beta = str.drop(6).toDouble;
		
		Some(BiLog(beta));
	}
	else if(str == "Exp") Some(Exp);
	else if(str.startsWith("Exp")) {
		// ex: "Exp_-1"
		val bias = str.drop(4).toDouble;
		
		Some(ExpBiased(bias));
	}
	else if(str.startsWith("BrokenLine")) {
		// ex: "BrokenLine_2_0.5"
		val tmp = str.drop(11).split('_');
		val cPos = tmp(0).toDouble;
		val cNeg = tmp(1).toDouble;
		
		Some(BrokenLine(cPos, cNeg));
	}
	else if(str == "Sigmoid") Some(Sigmoid);
	else if(str == "Weird") Some(Weird);
	else {
		assert(str == "Tanh", "unknown non linearity \"" + str + "\"");
		
		Some(Tanh);
	}
}

trait NNFactory {
	def buildConvLayer(imageWidth: Int, imageHeight: Int, pixelSize: Int, nbFilters: Int, layerOptions: Array[String], distribution: (Double => Double)): Layer = convLayerGenerator(imageWidth, imageHeight, pixelSize, nbFilters, layerOptions, distribution)();
	def convLayerGenerator(imageWidth: Int, imageHeight: Int, pixelSize: Int, nbFilters: Int, layerOptions: Array[String], distribution: (Double => Double)): (() => Layer) = {
		// Ex: "ML|ReLU 9x9 2x2" or equivalently "ML|ReLU 9 2"
		val filtersOptions = layerOptions(0).split('|');
		
		val (filtersXSize, filtersYSize) = {
			val tmp = layerOptions(1).split('x').map(_.toInt);
			if(tmp.length == 1) (tmp.head, tmp.head);
			else (tmp(0), tmp(1));
		}
		
		val (poolingX, poolingY) = {
			val tmp = layerOptions(2).split('x').map(_.toInt);
			if(tmp.length == 1) (tmp.head, tmp.head);
			else (tmp(0), tmp(1));
		}
		
		assert(((imageWidth - filtersXSize + 1) % poolingX) == 0);
		assert(((imageHeight - filtersYSize + 1) % poolingY) == 0);
		
		val filtersInSize = filtersXSize * filtersYSize * pixelSize;
		val filtersOutSize = 1;
		
		(() => {
			val filters = Array.fill(nbFilters)(buildLayer(filtersInSize, filtersOutSize, filtersOptions, distribution));
			
			MaxPoolingConvolutionalLayer(filters, filtersXSize, filtersYSize, pixelSize, imageWidth, imageHeight, poolingX, poolingY);
		});
	}
	
	def buildLayer(layerInput: Int, layerOutput: Int, layerOptions: Array[String], distribution: (Double => Double)): Layer = layerGenerator(layerInput, layerOutput, layerOptions, distribution)();
	def layerGenerator(layerInput: Int, layerOutput: Int, layerOptions: Array[String], distribution: (Double => Double)): (() => Layer) = {
		val layerType = layerOptions.head;
		
		val nonLinearity: Option[NonLinearity with Differentiable] = StringToFunction(layerOptions(1));
		
		if(layerType == "SLL") { // SparseLinesLayer
			val k = layerInput / 2;
			val alpha = math.ceil(math.sqrt(layerInput)).toInt;
			assert(k >= alpha);
			
			val c: Double = math.sqrt(alpha);
			
			(() => {
				val coeffs = Array.fill(layerOutput)(Array.fill(k)(distribution(c)));
				val positions = Array.fill(layerOutput)(util.Random.shuffle(0 to (layerInput - 1)).take(k).toArray);
				
				val t = Array.fill(layerOutput)(distribution(1));
				
				SparseLinesLayer(coeffs, positions, t, layerInput, alpha, nonLinearity);
			});
		}
		else if(layerType == "EWML") { // EWMultLayer
			val leftOptions = layerOptions(2).split('|');
			require(leftOptions.head != "EWML"); // More recursion would lead to problems
			
			val rightOptions = layerOptions(3).split('|');
			require(rightOptions.head != "EWML"); // More recursion would lead to problems
			
			(() => {
				val left = layerGenerator(layerInput, layerOutput, leftOptions, distribution)();
				val right = layerGenerator(layerInput, layerOutput, rightOptions, distribution)();
				
				EWMultLayer(left, right, nonLinearity);
			});
		}
		else if(layerType == "ML") { // MatrixLayer
			val c: Double = math.sqrt(layerInput);
			
			(() => {
				val m = Array.fill(layerInput * layerOutput)(distribution(c));
				val t = Array.fill(layerOutput)(distribution(1));
				
				MatrixLayer(m, t, nonLinearity);
			});
		}
		else { // Array2DLayer
			assert(layerType == "2DL", layerType + " != 2DL");
			
			val c: Double = math.sqrt(layerInput);
			
			(() => {
				val m = Array.fill(layerOutput)(Array.fill(layerInput)(distribution(c)));
				val t = Array.fill(layerOutput)(distribution(1));
				
				Array2DLayer(m, t, nonLinearity);
			});
		}
	}
}

// TODO object ConvNetFactory extends NNFactory

object TransformNNFactory extends NNFactory {
	def apply(layerTypes: Array[String], layerSizes: Array[Int], distributionOpt: Option[(Double => Double)] = None): TransformNN = {
		require(layerTypes.length + 1 == layerSizes.length, layerTypes.length + " + 1 != " + layerSizes.length);
		
		var layers = List[Layer]();
		
		val distribution: (Double => Double) = distributionOpt match { // How the coefficient are randomly drawn depending on the sqrt(input size)
			case Some(f) => f;
			//case _ => ((c: Double) => (util.Random.nextDouble - 0.5) / c); // Between (-0.5 and 0.5) / sqrt(layerInput)
			case _ => ((c: Double) => (util.Random.nextGaussian) / c); // Gaussian with mean 0 and standard deviation 1/sqrt(layerInput)
		}
		
		var i = 0;
		while(i < layerTypes.length) {
			val layerOptions = layerTypes(i).split(':');
			val layerOutput = layerSizes(i);
			val layerInput = layerSizes(i + 1);
			
			val layer = buildLayer(layerInput, layerOutput, layerOptions, distribution);
			
			layers ::= layer;
			
			i += 1;
		}
		
		TransformNN(layers);
	}
}

object TransformNN {
	def apply(layers: List[Layer]): TransformNN = new TransformNN(layers);
	
	// Inverse of the custom serialization
	def read(str: String): TransformNN = read(str.split('\n'), 1)._1; // The first line is for the type of network
	def read(data: Array[String], i: Int): (TransformNN, Int) = {
		var j = i;
		
		val layers = {
			val tmp = Serializer.readLayers(data, j);
			j = tmp._2;
			
			tmp._1.toList;
		}
		
		(new TransformNN(layers), j);
	}
}

@SerialVersionUID(3941150803677520723L)
class TransformNN(val layers: List[Layer]) extends SequentialLNN[Array[Double]] {
	def execute(in: Array[Double]): Array[Double] = executeArray(in);
	
	def transformAndGradient(in: Array[Double]): (Array[Double], Array[Double] => (List[ParameterBrick], Array[Double])) = outputAndGradient(in);
	
	def gradient(in: Array[Double]): (Array[Double] => (List[ParameterBrick], Array[Double])) = transformAndGradient(in)._2; // dout => (List[dc/dparam], dc/din)
	
	// A copy of the network with p (not a copy) as parameters
	override def duplicate(p: List[ParameterBrick]): TransformNN = duplicateFeed(p) match {
		case (x, Nil) => x;
		case _ => {throw IncompatibleParameters; this;}
	}
	
	// Similar to duplicate, but also returns the unused ParameterBrick
	def duplicateFeed(p: List[ParameterBrick]): (TransformNN, List[ParameterBrick]) = {
		val tmp = Math.zip(layers, p);

		(TransformNN(tmp._1.map(x => x._1.duplicate(x._2))), tmp._3);
	}
	
	def customSerialize: mutable.StringBuilder = {
		val stringBuilder = new mutable.StringBuilder();
		customSerialize(stringBuilder);
		
		stringBuilder;
	}
	
	def customSerialize(stringBuilder: mutable.StringBuilder): Unit = {
		stringBuilder ++= "TransformNN\n";
		stringBuilder ++= layers.length.toString + '\n';
		layers.foreach(_.customSerialize(stringBuilder));
	}

	override def toString: String = "TransformNN(" + layers.mkString(", ") + ")";
}

object Identity extends TransformNN(Nil);

object ScoreNNFactory extends NNFactory {
	// for instance apply(Array(""), Array(32,128,128,160))
	def apply(layerTypes: Array[String], layerSizes: Array[Int], distributionOpt: Option[(Double => Double)] = None): ScoreNN = {
		require(layerTypes.length == layerSizes.length, layerTypes.length + " != " + layerSizes.length);
		
		var layers = List[Layer]();
		
		val distribution: (Double => Double) = distributionOpt match { // How the coefficient are randomly drawn depending on the sqrt(input size)
			case Some(f) => f;
			//case _ => ((c: Double) => (util.Random.nextDouble - 0.5) / c); // Between (-0.5 and 0.5) / sqrt(layerInput)
			case _ => ((c: Double) => (util.Random.nextGaussian) / c); // Gaussian with mean 0 and standard deviation 1/sqrt(layerInput)
		}
		
		var i = 0;
		while(i < layerTypes.length) {
			val layerOptions = layerTypes(i).split(':');
			val layerOutput = if(i > 0) layerSizes(i - 1) else 1;
			val layerInput = layerSizes(i);
			
			val layer = buildLayer(layerInput, layerOutput, layerOptions, distribution);
			
			layers ::= layer;
			
			i += 1;
		}
		
		ScoreNN(layers);
	}
}

object ScoreNN {
	def apply(layers: List[Layer]): ScoreNN = new ScoreNN(layers);
	
	// Inverse of the custom serialization
	def read(str: String): ScoreNN = read(str.split('\n'), 1)._1; // The first line is for the type of network
	def read(data: Array[String], i: Int): (ScoreNN, Int) = {
		var j = i;
		
		val layers = {
			val tmp = Serializer.readLayers(data, j);
			j = tmp._2;
			
			tmp._1.toList;
		}
		
		(new ScoreNN(layers), j);
	}
}

@SerialVersionUID(3941150803677520723L)
class ScoreNN(val layers: List[Layer]) extends SequentialLNN[Double] {
	def execute(in: Array[Double]): Double = executeArray(in).head;
	
	def scoreAndGradient(in: Array[Double]): (Double, Double => (List[ParameterBrick], Array[Double])) = {
		val tmp = outputAndGradient(in);
		
		(tmp._1.head, (d: Double) => tmp._2(Array(d)));
	}
	
	def gradient(in: Array[Double]): (Double => (List[ParameterBrick], Array[Double])) = scoreAndGradient(in)._2; // dout => (List[dc/dparam], dc/din)
	
	def customSerialize: mutable.StringBuilder = {
		val stringBuilder = new mutable.StringBuilder();
		customSerialize(stringBuilder);
		
		stringBuilder;
	}
	
	def customSerialize(stringBuilder: mutable.StringBuilder): Unit = {
		stringBuilder ++= "ScoreNN\n";
		stringBuilder ++= layers.length.toString + '\n';
		layers.foreach(_.customSerialize(stringBuilder));
	}
	
	// Gradient for the proto-score (e.g. without the last non-linearity)
	def scoreAndProtoGradient(in: Array[Double]): (Double, Double => (List[ParameterBrick], Array[Double])) = { // (out, List[dc/dparam], dc/din)
		// First pass
		val (v, tmp) = {
			var acc: List[(Array[Double] => (LayerParam, Array[Double]))] = Nil; // dvn[i]/dparam, dvn[i]/dv(n-1)[j]
			var vn = in;
			var l = layers;
			while(l.nonEmpty) {
				val layer = l.head;
				l = l.tail;
				
				if(l.nonEmpty) {
					val (vn2, dF) = layer.gradient(vn);
					acc = dF :: acc;
				
					vn = vn2;
				}
				else { // Special treatment for the last layer, as we don't take the non-linearity into account
					val (vn2, dF) = layer.protoGradient(vn);
					acc = dF :: acc;
				
					vn = vn2;
				}
			}
			
			(vn, acc);
		}
		
		(
			v.head,
			((d: Double) => { // d is dc/dvn
				// Second pass
				var acc: List[LayerParam] = Nil;
			
				var dvn = Array(d); // dc/vn
				var l = tmp;
				while(!l.isEmpty) {
					val dF = l.head;
				
					val (dParam, dvn2) = dF(dvn);
				
					acc = dParam :: acc;
				
					dvn = dvn2 // dc/dv(n-1)[i]
					l = l.tail;
				}
			
				(acc, dvn);
			})
		);
	}
	
	def protoGradient(in: Array[Double]): (Double => (List[ParameterBrick], Array[Double])) = scoreAndProtoGradient(in)._2; // (List[dc/dparam], dc/din)
	def protoGradient(in: Array[Double], d: Double): (List[ParameterBrick], Array[Double]) = protoGradient(in)(d); // (List[dc/dparam], dc/din)
	
	// A copy of the network with p (not a copy) as parameters
	override def duplicate(p: List[ParameterBrick]): ScoreNN = duplicateFeed(p) match {
		case (x, Nil) => x;
		case _ => {throw IncompatibleParameters; this;}
	}
	
	// Similar to duplicate, but also returns the unused ParameterBrick
	def duplicateFeed(p: List[ParameterBrick]): (ScoreNN, List[ParameterBrick]) = {
		val tmp = Math.zip(layers, p);

		(ScoreNN(tmp._1.map(x => x._1.duplicate(x._2))), tmp._3);
	}

	override def toString: String = "ScoreNN(" + layers.mkString(", ") + ")";
}
