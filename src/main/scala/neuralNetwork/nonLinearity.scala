// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neuralNetwork;

trait NonLinearity extends java.io.Serializable {
	def apply(v: Array[Double]): Array[Double];
	
	// Proof of non-linearity
	protected def x: Double = -1.0;
	protected def y: Double = 2.0;
	final private def testNonLinearity(x: Double, y: Double): Boolean = {
		val v = apply(Array(x, y));
		
		(math.abs(v(0) - x/y*v(1)) > 1e-6);
	}
	require(testNonLinearity(x, y) == true);
}

trait PointWiseNonLinearity extends NonLinearity {
	def apply(v: Array[Double]): Array[Double] = Math.arrayMap(v, this.apply);
//	def apply(v: Array[Double]): Array[Double] = v.map(this.apply);
	
	def apply(x: Double): Double;
}

trait Differentiable {
	// Computes dc/din[i] from in[i'] and dc/dOut[i''] (= dOut)
	def derivative(in: Array[Double], dOut: Array[Double]): Array[Double];
}

trait PointWiseDifferentiable extends Differentiable {
	def derivative(in: Array[Double], dOut: Array[Double]): Array[Double] = Math.multiply(Math.arrayMap(in, this.derivative), dOut); // dc/din[i]
//	def derivative(in: Array[Double], dOut: Array[Double]): Array[Double] = Math.multiply(in.map(this.derivative), dOut); // dc/din[i]
	
	def derivative(x: Double): Double;
}

object SoftMax extends NonLinearity with Differentiable {
	def apply(v: Array[Double]): Array[Double] = Math.softMax(v);
	
	def derivative(in: Array[Double], dOut: Array[Double]): Array[Double] = derivativeFromOutput(Math.softMax(in), dOut);
	
	def derivativeFromOutput(out: Array[Double], dOut: Array[Double]): Array[Double] = { // dc/din[i]
		val tmp = Math.multiply(out, dOut); // dOut(i) * s(i)
		
		val sum = tmp.sum; // Sum(i, dOut(i) * s(i))
		
		Math.substractTo(tmp, Math.multiply(out, sum)); // s(i) * (dOut(i) - Sum(j, dOut(j) * s(j)))
		
		tmp;
	}
	
	override def toString: String = "SoftMax";
}

case class BrokenLine(val cPos: Double, val cNeg: Double) extends PointWiseNonLinearity with PointWiseDifferentiable {
	def apply(x: Double): Double = if(x > 0) (x * cPos) else (x * cNeg);
	
	def derivative(x: Double): Double = if(x > 0) cPos else cNeg;
}

case class thresholdClipper(val t: Double) extends PointWiseNonLinearity with PointWiseDifferentiable {
	def apply(x: Double): Double = if(x > t) t else if(x > -t) x else -t;
	
	def derivative(x: Double): Double = if(x < t && x > -t) 1.0 else 0.0;
	
	// For the proof of non-linearity
	override def x: Double = -t;
	override def y: Double = 2 * t;
}

// -log(1 - beta * x) | log(1 + beta * x)
case class BiLog(val beta: Double) extends PointWiseNonLinearity with PointWiseDifferentiable with java.io.Serializable {
	def apply(x: Double): Double = if(x >= 0.0) (math.log(1.0 + beta * x) / beta) else (- math.log(1.0 - beta * x) / beta);
	
	def derivative(x: Double): Double = if(x >= 0.0) (beta / (1.0 + beta * x)) else (beta / (1.0 - beta * x));
}

// beta <-- --> log(x)
case class CustomFunction(val beta: Double) extends PointWiseNonLinearity with PointWiseDifferentiable with java.io.Serializable {
	def apply(x: Double): Double = math.log(1.0 + (if(x < 0) math.exp(x) else (1 + x))) + beta;
	
	def derivative(x: Double): Double = if(x < 0.0) {
		val z = math.exp(x);
		
		z / (1.0 + z);
	}
	else {
		1.0 / (2.0 + x);
	}
}

// log in the negatives, linear in the positives
object LogPlus extends PointWiseNonLinearity with PointWiseDifferentiable with java.io.Serializable {
	def apply(x: Double): Double = if(x >= 0.0) x else -math.log(1.0 - x);
	
	def derivative(x: Double): Double = if(x >= 0.0) 1.0 else (1.0 / (1.0 - x)); 
}

object Weird extends PointWiseNonLinearity with PointWiseDifferentiable {
	def apply(x: Double): Double = if(x <= -1.0) -1.0;
	else if(x <= 1.0) x;
	else Double.PositiveInfinity;
	
	def derivative(x: Double): Double = if(x >= -1.0 && x <= 1.0) 1.0 else 0.0;
	
	override def toString: String = "Weird";
}

// exp (beware of infinities)
object Exp extends PointWiseNonLinearity with PointWiseDifferentiable with java.io.Serializable {
	def apply(x: Double): Double = math.exp(x);
	
	def derivative(x: Double): Double = math.exp(x);
	
	override def toString: String = "Exp";
}

// exp + beta (beware of infinities)
case class ExpBiased(val beta: Double) extends PointWiseNonLinearity with PointWiseDifferentiable with java.io.Serializable {
	def apply(x: Double): Double = math.exp(x) + beta;
	
	def derivative(x: Double): Double = math.exp(x);
}

// 0 <-- 1 --> O(n^2)
object SoftQuadSafe extends PointWiseNonLinearity with PointWiseDifferentiable with java.io.Serializable { // ReQU can be stuck in 0
	def apply(x: Double): Double = if(x >= 0.0) (0.5 + (Math.square(1.0 + x) / 2)) else math.exp(x);
	
	def derivative(x: Double): Double = if(x >= 0.0) (x + 1.0) else math.exp(x);
	
	override def toString: String = "SoftQuadSafe";
}

// beta <-- (1 + beta) --> O(n^2)
case class SoftQuadSafeBiased(val beta: Double) extends PointWiseNonLinearity with PointWiseDifferentiable with java.io.Serializable { // ReQU can be stuck in 0
	def apply(x: Double): Double = if(x >= 0.0) (0.5 + (Math.square(1.0 + x) / 2) + beta) else (math.exp(x) + beta);
	
	def derivative(x: Double): Double = if(x >= 0.0) (x + 1.0) else math.exp(x);
}

// 0 <-- 1 --> O(n)
object SoftPlusSafe extends PointWiseNonLinearity with PointWiseDifferentiable with java.io.Serializable { // ReLU can be stuck in 0
	def apply(x: Double): Double = if(x >= 0.0) (1.0 + x) else math.exp(x);
	
	def derivative(x: Double): Double = if(x >= 0.0) 1.0 else math.exp(x);
	
	override def toString: String = "SoftPlusSafe";
}

// beta <-- (1 + beta) --> O(n)
case class SoftPlusSafeBiased(val beta: Double) extends PointWiseNonLinearity with PointWiseDifferentiable with java.io.Serializable { // ReLU can be stuck in 0
	def apply(x: Double): Double = if(x >= 0.0) (1.0 + x + beta) else (math.exp(x) + beta);
	
	def derivative(x: Double): Double = if(x >= 0.0) 1.0 else math.exp(x); 
}

// Rectified quadratic unit
object ReQU extends PointWiseNonLinearity with PointWiseDifferentiable with java.io.Serializable {
	def apply(x: Double): Double = if(x > 0.0) Math.square(x) else 0.0;
	
	def derivative(x: Double): Double = if(x > 0.0) (2 * x) else 0.0;
	
	override def toString: String = "ReQU";
}

// Rectified linear unit
object ReLU extends PointWiseNonLinearity with PointWiseDifferentiable with java.io.Serializable {
	def apply(x: Double): Double = math.max(x, 0.0);
	
	def derivative(x: Double): Double = if(x >= 0.0) 1.0 else 0.0;
	
	override def toString: String = "ReLU";
}

// 0 <--> 1
object Sigmoid extends PointWiseNonLinearity with PointWiseDifferentiable with java.io.Serializable {
	def apply(x: Double): Double = 1.0 / (1.0 + math.pow(math.E, -x));
	
	def derivative(x: Double): Double = {
		val y = apply(x);
		
		y * (1.0 - y);
	}
	
	override def toString: String = "Sigmoid";
}

// -1 <--> 1
object Tanh extends PointWiseNonLinearity with PointWiseDifferentiable with java.io.Serializable {
	def apply(x: Double): Double = math.tanh(x);
	
	def derivative(x: Double): Double = {
		val y = apply(x);
		
		1.0 - math.pow(y, 2);
	}
	
	override def toString: String = "Tanh";
}
