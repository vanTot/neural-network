// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neuralNetwork;

import neuralNetwork._;

import annotation.tailrec;
import collection._;

/** Cross-entropy between discrete probability distributions
*
* [[https://en.wikipedia.org/wiki/Cross_entropy]]
*/
object CrossEntropy {
	@inline
	private def safeValue(x: Double): Double = math.max(x, fudgeFactor); // To avoid 0s
	private val fudgeFactor = 1.0E-8;
	
	/** Calculates the cross-entropy between two probability distributions, one of them being degenerate
	*
	* @param i the support of the degenerate distribution p
	* @param q an array of Double representing the second probability distribution
	* @return the cross-entropy between p and q
	*/
	def apply(i: Int, q: Array[Double]): Double = -math.log(safeValue(q(i)));
	
	/** Calculates the cross-entropy between two probability distributions
	*
	* @param p an array of Double representing the first probability distribution
	* @param q an array of Double representing the second probability distribution
	* @return the cross-entropy between p and q
	*/
	def apply(p: Array[Double], q: Array[Double]): Double = {
		var res = 0.0;
		
		var i = 0;
		while(i < p.length) {
			if(p(i) > 0.0) res += p(i) * math.log(safeValue(q(i)));
			
			i += 1;
		}
		
		-res;
	}
	
	/** Calculates the cross-entropy between two binary probability distributions, one of them being degenerate
	*
	* @param c indicates the support of the degenerate distribution p (true for the first event, false for the second)
	* @param x the probability of the first event according to the distribution q
	* @return the cross-entropy between p and q
	*/
	def apply(c: Boolean, x: Double): Double = c match {
		case true => -math.log(safeValue(x));
		case false => -math.log(safeValue(1.0 - x));
	}
	
	/** Calculates the derivative of the opposite of the cross-entropy between two probability distributions, one of them being degenerate
	*
	* @param i the support of the degenerate distribution p
	* @param q an array of Double representing the second probability distribution
	* @return the derivative (w.r.t. q) of the opposite of the cross-entropy between p and q
	*/
	def derivativeOp(i: Int, q: Array[Double]): Array[Double] = {
		val res = Array.ofDim[Double](q.length);
		
		res(i) = 1.0 / safeValue(q(i));
		
		res;
	}
	
	/** Calculates the derivative of the opposite of the cross-entropy between two probability distributions
	*
	* @param p an array of Double representing the first probability distribution
	* @param q an array of Double representing the second probability distribution
	* @return the derivative (w.r.t. q) of the opposite of the cross-entropy between p and q
	*/
	def derivativeOp(p: Array[Double], q: Array[Double]): Array[Double] = {
		var res = Array.ofDim[Double](p.length);
		
		var i = 0;
		while(i < p.length) {
			if(p(i) > 0.0) res(i) = p(i) / (safeValue(q(i)));
			
			i += 1;
		}
		
		res;
	}
	
	/** Calculates the derivative of the opposite of the cross-entropy between two binary probability distributions, one of them being degenerate
	*
	* @param c indicates the support of the degenerate distribution p (true for the first event, false for the second)
	* @param x the probability of the first event according to the distribution q
	* @return the derivative (w.r.t. x) of the opposite of the cross-entropy between p and q
	*/
	def derivativeOp(c: Boolean, x: Double): Double = c match {
		case true => 1.0 / safeValue(x);
		case false => - 1.0 / safeValue(1.0 - x);
	}
}

/** Squared error cost function
*/
object SquaredError {
	/** Calculates the squared error between two Double
	*
	* @param p a Double
	* @param q a Double
	* @return the squared difference between p and qS
	*/
	def apply(p: Double, q: Double): Double = Math.square(p - q);
	
	/** Calculates the derivative of the opposite of the squared error between two Double
	*
	* @param p a Double
	* @param q a Double
	* @return the derivative (w.r.t. q) of the opposite of the squared error between p and q
	*/
	def derivativeOp(p: Double, q: Double): Double = 2.0 * (p - q);
}
