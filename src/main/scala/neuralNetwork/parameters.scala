// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neuralNetwork;

import annotation.tailrec;
import collection._;

import learning._;

case class IncompatibleParameterBricks(purpose: String, bricks: List[ParameterBrick]) extends Exception;

trait Model extends java.io.Serializable {
	def parameters: CompoundParameter;
	
	def regularize(method: Regularization): Unit = method match {
		case L2(x) => parameters.multiplyTo(1.0 - x);
	}
}

trait ParameterBrick extends java.io.Serializable {
	def update(p: ParameterBrick): Unit = addTo(p);
	
	def addTo(that: ParameterBrick): Unit; // Inplace
	def substractTo(that: ParameterBrick): Unit; // Inplace
	def equateTo(that: ParameterBrick): Unit; // Inplace
	def adaUpdate(hist: ParameterBrick, grad: ParameterBrick, adagrad: Double, fudgeFactor: Double): Unit;
	
	def deepCopy: ParameterBrick;
	def empty: ParameterBrick;
	
	def multiplyTo(x: Double): Unit; // Inplace
	
	def clip(f: (Double => Double)): Unit; // Inplace
	def stats: (Double, Double, Int); // sum and max (in abs) + number of params
	lazy val nbParams = stats._3;
	def norm1: Double;
	def norm2Sq: Double;

	// Get the value of the coeff at index id
	def getCoeff(id: Int): Double;
	
	// Add x to the coeff at index id
	def addCoeff(id: Int, x: Double): Unit;
	
	
	// Max and average (in abs)
	def statsAvg: (Double, Double) = {
		val tmp: (Double, Double, Int) = stats;
		
		(tmp._2, (tmp._1 / tmp._3));
	}
	
	def norm2: Double = math.sqrt(norm2Sq);
	
	// Inplace
	def optClip(optF: Option[PointWiseNonLinearity], cN: Option[Double]): Unit = {
		optF match {
			case Some(f) => clip(f.apply);
			case None => ();
		}
		
		cN match {
			case Some(threshold) => clipNormalize(threshold);
			case None => ();
		}
		
		();
	}
	
	// If the norm 2 is higher than a given threshold, normalizes (inplace) to the threshold
	// Returns true iff normalization was performed
	def clipNormalize(threshold: Double): Boolean = {
		val x = norm2;
		
		if(x > threshold) {
			multiplyTo(threshold / x);
			
			true;
		}
		else false;
	}

	def nanScan: Boolean;
}

object CompoundParameter {
	def apply(bricks: ParameterBrick*): CompoundParameter = new CompoundParameterImp(bricks.toArray);
	def apply(bricks: Array[ParameterBrick]): CompoundParameter = new CompoundParameterImp(bricks);
	
	@SerialVersionUID(3941150803677520723L)
	class CompoundParameterImp(val bricks: Array[ParameterBrick]) extends CompoundParameter {
		def deepCopy: CompoundParameterImp = new CompoundParameterImp(bricks.map(_.deepCopy));
		def empty: CompoundParameterImp = new CompoundParameterImp(bricks.map(_.empty));
	}
}

trait CompoundParameter extends ParameterBrick {
	def bricks: Array[ParameterBrick];
	
	def deepCopy: CompoundParameter; // Could probably
	def empty: CompoundParameter;    // be removed
	
	// Inplace
	final def addTo(that: ParameterBrick): Unit = that match {
		case that: CompoundParameter => {
			require(bricks.length == that.bricks.length);
			var i = 0;
			while(i < bricks.length) {
				bricks(i).addTo(that.bricks(i));
				
				i += 1;
			}
		}
		
		case _ => throw IncompatibleParameterBricks("addTo", List(this, that));
	}
	
	// Inplace
	final def substractTo(that: ParameterBrick): Unit = that match {
		case that: CompoundParameter => {
			require(bricks.length == that.bricks.length);
			var i = 0;
			while(i < bricks.length) {
				bricks(i).substractTo(that.bricks(i));
				
				i += 1;
			}
		}
		
		case _ => throw IncompatibleParameterBricks("substractTo", List(this, that));
	}
	
	// Inplace
	final def equateTo(that: ParameterBrick): Unit = that match {
		case that: CompoundParameter => {
			require(bricks.length == that.bricks.length);
			var i = 0;
			while(i < bricks.length) {
				bricks(i).equateTo(that.bricks(i));
				
				i += 1;
			}
		}
		
		case _ => throw IncompatibleParameterBricks("substractTo", List(this, that));
	}
	
	final def adaUpdate(hist: ParameterBrick, grad: ParameterBrick, adagrad: Double, fudgeFactor: Double): Unit = (hist, grad) match {
		case (hist: CompoundParameter, grad: CompoundParameter) => {
			require(bricks.length == hist.bricks.length);
			require(bricks.length == grad.bricks.length);
			
			var i = 0;
			while(i < bricks.length) {
				bricks(i).adaUpdate(hist.bricks(i), grad.bricks(i), adagrad, fudgeFactor);
				
				i += 1;
			}
		}
		
		case _ => throw IncompatibleParameterBricks("adagrad", List(this, hist, grad));
	}
	
	// Inplace
	def multiplyTo(x: Double): Unit = {
		var i = 0;
		while(i < bricks.length) {
			bricks(i).multiplyTo(x);
			
			i += 1;
		}
	}

	// Get the value of the coeff at index id
	def getCoeff(id: Int): Double = {
		@tailrec
		def loop(i: Int, tmp: Int): Double = if(tmp >= bricks(i).nbParams) loop((i + 1), (tmp - bricks(i).nbParams)) else bricks(i).getCoeff(tmp);

		loop(0, id);
	}
	
	// Add x to the coeff at index id
	def addCoeff(id: Int, x: Double): Unit = {
		@tailrec
		def loop(i: Int, tmp: Int): Unit = if(tmp >= bricks(i).nbParams) loop((i + 1), (tmp - bricks(i).nbParams)) else bricks(i).addCoeff(tmp, x);

		loop(0, id);
	}
	
	// Inplace
	def clip(f: (Double => Double)): Unit = {
		var i = 0;
		while(i < bricks.length) {
			bricks(i).clip(f);
			
			i += 1;
		}
	}
	
	// sum and max (in abs) + number of params
	def stats: (Double, Double, Int) = {
		var sum = 0.0;
		var max = 0.0;
		var nb = 0;
		
		@inline
		def aux(tmp: (Double, Double, Int)): Unit = {
			sum += tmp._1;
			if(tmp._2 > max) max = tmp._2;
			nb += tmp._3;
		}
		
		var i = 0;
		while(i < bricks.length) {
			aux(bricks(i).stats);
			
			i += 1;
		}
		
		(sum, max, nb);
	}
	
	def norm1: Double = {
		var norm = 0.0;
		
		var i = 0;
		while(i < bricks.length) {
			norm += bricks(i).norm1;
			
			i += 1;
		}
		
		norm;
	}
	
	def norm2Sq: Double = {
		var norm = 0.0;
		
		var i = 0;
		while(i < bricks.length) {
			norm += bricks(i).norm2Sq;
			
			i += 1;
		}
		
		norm;
	}

	def nanScan: Boolean = ???
}

/** Implicit conversion for the main ParameterBrick.
 *
 *  {{{
 *  import ParameterBrickConversion._;
 *  val l: List[LayerParam] = ???
 *  val m: mutable.Map[Int, Array[Double]] = ???
 *  val c: CompoundParameter = CompoundParameter(l, m);
 *  }}}
 */
object ParameterBrickConversions {
	implicit def ParamListConversion(l: List[ParameterBrick]): ParamList = ParamList(l);
	implicit def sparseArray2DConversion(m: mutable.Map[Int, Array[Double]]): SparseArray2D = SparseArray2D(m);
	implicit def array2DConversion(m: Array[Array[Double]]): Array2D = Array2D(m);
	implicit def array1DConversion(a: Array[Double]): Array1D = Array1D(a);
}

@SerialVersionUID(3941150803677520723L)
case class ParamList(val l: List[ParameterBrick]) extends ParameterBrick {
	//def addTo(l2: List[LayerParam]): Unit = if(!(l, l2).zipped.map((p, q) => p.addTo(q)).reduce(_ & _)) throw IncompatibleParameters;
	def addTo(l2: List[ParameterBrick]): Unit = (l, l2).zipped.foreach((p, q) => p.addTo(q));
	
	def addTo(that: ParameterBrick): Unit = that match {
		//case that: LayerParamList => if(!(l, that.l).zipped.map((p, q) => p.addTo(q)).reduce(_ & _)) throw IncompatibleParameters;
		case that: ParamList => (l, that.l).zipped.foreach((p, q) => p.addTo(q));
		case _ => throw IncompatibleParameterBricks("addTo", List(this, that));
	}
	
	def substractTo(that: ParameterBrick): Unit = that match {
		//case that: LayerParamList => if(!(l, that.l).zipped.map((p, q) => p.substractTo(q)).reduce(_ & _)) throw IncompatibleParameters;
		case that: ParamList => (l, that.l).zipped.foreach((p, q) => p.substractTo(q));
		case _ => throw IncompatibleParameterBricks("substractTo", List(this, that));
	}
	
	def equateTo(that: ParameterBrick): Unit = that match {
		//case that: LayerParamList => if(!(l, that.l).zipped.map((p, q) => p.equateTo(q)).reduce(_ & _)) throw IncompatibleParameters;
		case that: ParamList => (l, that.l).zipped.foreach((p, q) => p.equateTo(q));
		case _ => throw IncompatibleParameterBricks("equateTo", List(this, that));
	}
	
	def adaUpdate(hist: ParameterBrick, grad: ParameterBrick, adagrad: Double, fudgeFactor: Double): Unit = (hist, grad) match {
		//case (hist: LayerParamList, grad: LayerParamList) => if(!(l, hist.l, grad.l).zipped.map((param: LayerParam, pHist: LayerParam, pGrad: LayerParam) => param.adaUpdate(pHist, pGrad, adagrad, fudgeFactor)).reduce(_ & _)) throw IncompatibleParameters;
		case (hist: ParamList, grad: ParamList) => (l, hist.l, grad.l).zipped.foreach((param: ParameterBrick, pHist: ParameterBrick, pGrad: ParameterBrick) => param.adaUpdate(pHist, pGrad, adagrad, fudgeFactor));
		case _ => throw IncompatibleParameterBricks("adagrad", List(this, hist, grad));
	}
	
	def deepCopy: ParamList = ParamList(l.map(_.deepCopy));
	def empty: ParamList = ParamList(l.map(_.empty));
	
	def multiplyTo(x: Double): Unit = l.foreach{layerParam => layerParam.multiplyTo(x)};
	
	// Get the value of the coeff at index id
	def getCoeff(id: Int): Double = {
		@tailrec
		def loop(tmpLayers: List[ParameterBrick], tmpId: Int): Double = {
			val h :: t = tmpLayers;
			if(tmpId >= h.nbParams) loop(t, (tmpId - h.nbParams)) else h.getCoeff(tmpId);
		}

		loop(l, id);
	}
	
	// Add x to the coeff at index id
	def addCoeff(id: Int, x: Double): Unit = {
		@tailrec
		def loop(tmpLayers: List[ParameterBrick], tmpId: Int): Unit = {
			val h :: t = tmpLayers;
			if(tmpId >= h.nbParams) loop(t, (tmpId - h.nbParams)) else h.addCoeff(tmpId, x);
		}

		loop(l, id);
	}

	def clip(f: (Double => Double)): Unit = l.foreach{layerParam => layerParam.clip(f)};
	
	def stats: (Double, Double, Int) = {
		var sum = 0.0;
		var max = 0.0;
		var nb = 0;
		
		@inline
		def aux(tmp: (Double, Double, Int)): Unit = {
			sum += tmp._1;
			if(tmp._2 > max) max = tmp._2;
			nb += tmp._3;
		}
		
		l.foreach{layerParam => aux(layerParam.stats)};
		
		(sum, max, nb);
	}
	
	def norm1: Double = l.map(p => p.norm1).reduce(_ + _);
	
	def norm2Sq: Double = l.map(p => p.norm2Sq).reduce(_ + _);

	def nanScan: Boolean = ???
}

@SerialVersionUID(3941150803677520723L)
case class SparseArray2D(val m: mutable.Map[Int, Array[Double]]) extends ParameterBrick {
	def addTo(m2: mutable.Map[Int, Array[Double]]): Unit = structureHandlers.SparseArray2DHandler.addTo(m, m2);
	
	def addTo(that: ParameterBrick): Unit = that match {
		case that: SparseArray2D => structureHandlers.SparseArray2DHandler.addTo(m, that.m);
		case _ => throw IncompatibleParameterBricks("addTo", List(this, that));
	}
	
	def substractTo(that: ParameterBrick): Unit = that match {
		case that: SparseArray2D => structureHandlers.SparseArray2DHandler.substractTo(m, that.m);
		case _ => throw IncompatibleParameterBricks("substractTo", List(this, that));
	}
	
	def equateTo(that: ParameterBrick): Unit = that match {
		case that: SparseArray2D => ???
		case _ => throw IncompatibleParameterBricks("equateTo", List(this, that));
	}
	
	def adaUpdate(hist: ParameterBrick, grad: ParameterBrick, adagrad: Double, fudgeFactor: Double): Unit = (hist, grad) match {
		case (hist: SparseArray2D, grad: SparseArray2D) => ???
		case _ => throw IncompatibleParameterBricks("adagrad", List(this, hist, grad));
	}
	
	def deepCopy: SparseArray2D = SparseArray2D(mutable.Map() ++= m);
	def empty: SparseArray2D = SparseArray2D(mutable.Map());
	
	def multiplyTo(x: Double): Unit = structureHandlers.SparseArray2DHandler.multiplyTo(m, x);

	// Get the value of the coeff at index id
	def getCoeff(id: Int): Double = {
		???
	}
	
	// Add x to the coeff at index id
	def addCoeff(id: Int, x: Double): Unit = {
		???
	}
	
	def clip(f: (Double => Double)): Unit = structureHandlers.SparseArray2DHandler.clip(m, f);
	
	def stats: (Double, Double, Int) = structureHandlers.SparseArray2DHandler.stats(m);
	
	def norm1: Double = structureHandlers.SparseArray2DHandler.norm1(m);
	
	def norm2Sq: Double = structureHandlers.SparseArray2DHandler.norm2Sq(m);
	
	def addUpdate(id: Int, v: Array[Double]): Unit = m.get(id) match {
		case Some(vec) => Math.addTo(vec, v);
		case None => m(id) = v;
		//case None => m(id) = v.clone;
	}
	
	def addUpdate(id: Int, v: Array[Double], pos: Int, length: Int): Unit = m.get(id) match {
		case Some(vec) => Math.addTo(vec, v, pos);
		case None => m(id) = java.util.Arrays.copyOfRange(v, pos, (pos + length));
	}

	def nanScan: Boolean = ???
}

@SerialVersionUID(3941150803677520723L)
case class Array2D(val m: Array[Array[Double]]) extends ParameterBrick {
	def addTo(m2: Array[Array[Double]]): Unit = structureHandlers.Array2DHandler.addTo(m, m2);
	
	def addTo(that: ParameterBrick): Unit = that match {
		case that: Array2D => structureHandlers.Array2DHandler.addTo(m, that.m);
		case that: SparseArray2D => structureHandlers.Array2DHandler.addTo(m, that.m);
		case _ => throw IncompatibleParameterBricks("addTo", List(this, that));
	}
	
	def substractTo(that: ParameterBrick): Unit = that match {
		case that: Array2D => structureHandlers.Array2DHandler.substractTo(m, that.m);
		case that: SparseArray2D => structureHandlers.Array2DHandler.substractTo(m, that.m);
		case _ => throw IncompatibleParameterBricks("substractTo", List(this, that));
	}
	
	def equateTo(that: ParameterBrick): Unit = that match {
		case that: Array2D => Math.equateTo(m, that.m);
		case _ => throw IncompatibleParameterBricks("equateTo", List(this, that));
	}
	
	def adaUpdate(hist: ParameterBrick, grad: ParameterBrick, adagrad: Double, fudgeFactor: Double): Unit = (hist, grad) match {
		case (hist: Array2D, grad: SparseArray2D) => {
			for((id, gradVec) <- grad.m) {
				val histVec = hist.m(id);
				val paramVec = m(id);
				
				Math.adaUpdate(paramVec, histVec, gradVec, adagrad, fudgeFactor);
			}
		}
		
		case (hist: Array2D, grad: Array2D) => Math.adaUpdate(m, hist.m, grad.m, adagrad, fudgeFactor);
		
		case _ => throw IncompatibleParameterBricks("adagrad", List(this, hist, grad));
	}
	
	def deepCopy: Array2D = Array2D(m.map(_.clone));
	def empty: Array2D = Array2D(Array.ofDim[Double](m.size, (if(m.nonEmpty) m.head.size else 0)));
	
	def multiplyTo(x: Double): Unit = structureHandlers.Array2DHandler.multiplyTo(m, x);
	
	// Get the value of the coeff at index id
	def getCoeff(id: Int): Double = {
		@tailrec
		def loop(i: Int, tmp: Int): Double = if(tmp >= m(i).length) loop((i + 1), (tmp - m(i).length)) else m(i)(tmp);

		loop(0, id);
	}
	
	// Add x to the coeff at index id
	def addCoeff(id: Int, x: Double): Unit = {
		@tailrec
		def loop(i: Int, tmp: Int): Unit = if(tmp >= m(i).length) loop((i + 1), (tmp - m(i).length)) else m(i)(tmp) += x;

		loop(0, id);
	}

	def clip(f: (Double => Double)): Unit = structureHandlers.Array2DHandler.clip(m, f);
	
	def stats: (Double, Double, Int) = structureHandlers.Array2DHandler.stats(m);
	
	def norm1: Double = structureHandlers.Array2DHandler.norm1(m);
	
	def norm2Sq: Double = structureHandlers.Array2DHandler.norm2Sq(m);

	def nanScan: Boolean = ???
}

@SerialVersionUID(3941150803677520723L)
case class Array1D(val a: Array[Double]) extends ParameterBrick {
	def addTo(a2: Array[Double]): Unit = structureHandlers.ArrayHandler.addTo(a, a2);
	
	def addTo(that: ParameterBrick): Unit = that match {
		case that: Array1D => structureHandlers.ArrayHandler.addTo(a, that.a);
		case _ => throw IncompatibleParameterBricks("addTo", List(this, that));
	}
	
	def substractTo(that: ParameterBrick): Unit = that match {
		case that: Array1D => structureHandlers.ArrayHandler.substractTo(a, that.a);
		case _ => throw IncompatibleParameterBricks("substractTo", List(this, that));
	}
	
	def equateTo(that: ParameterBrick): Unit = that match {
		case that: Array1D => Math.equateTo(a, that.a);
		case _ => throw IncompatibleParameterBricks("equateTo", List(this, that));
	}
	
	def adaUpdate(hist: ParameterBrick, grad: ParameterBrick, adagrad: Double, fudgeFactor: Double): Unit = (hist, grad) match {
		case (hist: Array1D, grad: Array1D) => Math.adaUpdate(a, hist.a, grad.a, adagrad, fudgeFactor);
		case _ => throw IncompatibleParameterBricks("adagrad", List(this, hist, grad));
	}
	
	def deepCopy: Array1D = Array1D(a.clone);
	def empty: Array1D = Array1D(Array.ofDim[Double](a.length));
	
	def multiplyTo(x: Double): Unit = structureHandlers.ArrayHandler.multiplyTo(a, x);
	
	def clip(f: (Double => Double)): Unit = structureHandlers.ArrayHandler.clip(a, f);
	
	def stats: (Double, Double, Int) = structureHandlers.ArrayHandler.stats(a);

	// Get the value of the coeff at index id
	def getCoeff(id: Int): Double = a(id);
	
	// Add x to the coeff at index id
	def addCoeff(id: Int, x: Double): Unit = a(id) += x;
	
	def norm1: Double = structureHandlers.ArrayHandler.norm1(a);
	
	def norm2Sq: Double = structureHandlers.ArrayHandler.norm2Sq(a);

	def nanScan: Boolean = ???
}
