// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neuralNetwork;

import annotation.tailrec;
import collection._;
import util.Try;

import learning._;
import embeddings._;

class ParsedOptions(val arguments: Map[String, String], val flags: Set[String]) {
	def apply(name: String): String = arguments(name);
	
	def flag(name: String, print: Boolean = false): Boolean = {
		val res = flags.contains(name);
		if(print) Console.println(name + ": " + res);
		
		res;
	}
	
	def get(name: String, print: Boolean = false): Option[String] = {
		val res = arguments.get(name);
		if(print) Console.println(name + ": " + res);
		
		res;
	}
	
	def getString(name: String, default: => String, print: Boolean = false): String = {
		val res = arguments.getOrElse(name, default);
		if(print) Console.println(name + ": " + res);
		
		res;
	}
	
	def getDoubleOpt(name: String): Option[Double] = Try(arguments(name).toDouble).toOption;
	
	def getDouble(name: String, default: => Double, print: Boolean = false): Double = {
		val res = Try(arguments(name).toDouble).toOption match {
			case Some(value) => value;
			case None => default;
		}
		if(print) Console.println(name + ": " + res);
		
		res;
	}
	
	def getIntOpt(name: String): Option[Int] = Try(arguments(name).toInt).toOption;
	
	def getInt(name: String, default: => Int, print: Boolean = false): Int = {
		val res = Try(arguments(name).toInt).toOption match {
			case Some(value) => value;
			case None => default;
		}
		if(print) Console.println(name + ": " + res);
		
		res;
	}
	
	def regularizer(print: Boolean = false): Regularizer = {
		val period = getInt("regPeriod", 0);
		val rate = getDouble("regRate", 0.01);
		
		val res = ClockRegularizer(period, L2(rate));
		if(print) Console.println("regularizer: " + res);
		
		res;
	}
	
	def learningMethod(print: Boolean = false): LearningMethod = {
		val res = arguments.get("learning") match {
			case Some("normal") => {
				val a = getDouble("a", 0.01);
				val k = getDouble("k", 1);
				val p = getDouble("p", 1);
				
				NormalLearning(a, k, p);
			}

			case Some("adaptable") => {
				val a = getDouble("a", 0.01);

				AdaptableLearning(a);
			}

			case Some("superAdaptable") => {
				val a = getDouble("a", 0.01);
				val thr1 = getDouble("thr1", 0.1);
				val thr2 = getDouble("thr2", 0.1);

				SuperAdaptableLearning(a, thr1, thr2);
			}
			
			case Some("momentum") => {
				val a = getDouble("a", 0.01);
				val k = getDouble("k", 1);
				val p = getDouble("p", 1);
				val m = getDouble("m", 0.9);
				
				MomentumLearning(a, k, p, m);
			}
			
			case _ => {
				val a = getDouble("a", 0.1);
				
				Adagrad(a);
			}
		}
		if(print) Console.println("learningMethod: " + res);
		
		res;
	}
	
	def embeddings(defaultSize: => Int, print: Boolean = false): Dictionary = {
		val tmp = arguments.get("embeddings-path") match {
			case Some(path) => {
				arguments.get("embeddings-type") match {
					case Some("GloVe") => Some(new GloVeDictionary(path));
					case _ => None;
				}
			}
			
			case None => None;
		};
		
		val res = tmp match {
			case Some(dico) => dico;
			case None => {
				val vectorSize = getInt("wordEmbeddingSize", defaultSize, true);
				
				new EmptyDictionary(vectorSize);
			}
		}
		if(print) Console.println("wordEmbeddings: " + res);
		
		res;
	}
}

trait ParseOptionsTrait[T] {
	def get(args: Array[String]): T;
	
	def apply(args: Array[String]): (Map[String, String], Set[String]) = {
		val arguments = mutable.Map[String, String]();
		val flags = mutable.Set[String]();
		
		{var i = 0;
		while(i < args.length) {
			if(args(i).startsWith("--")) {
				val argName = args(i).substring(2);
				
				arguments += ((argName, args(i + 1)));
				i += 1;
			}
			else if(args(i).startsWith("-")) {
				flags += args(i).substring(1);
			}
			else {
				Console.println("ParseArgument: Why \"" + args(i) + "\"?");
				System.exit(0);
			}
			
			i += 1;
		}}
		
		(arguments, flags);
	}
}

object ParseOptions extends ParseOptionsTrait[ParsedOptions] {
	def get(args: Array[String]): ParsedOptions = {
		val (arguments, flags) = apply(args);
		
		new ParsedOptions(arguments, flags);
	}
}
