// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neuralNetwork;

import annotation.tailrec;
import collection._;

class Selector(val vectors: Array[Array[Double]], f: (Array[Double] => Int)) extends NeuralNetwork[Array[Double]]  {
	val height: Int = vectors.head.length;
	val width: Int = vectors.length;

	val param: List[ParameterBrick] = List(Array2D(vectors));
	
	def select(in: Array[Double]): Int = f(in);

	def get(id: Int): Option[Array[Double]] = if(id < vectors.length) Some(vectors(id)) else None;

	def execute(in: Array[Double]): Array[Double] = vectors(f(in));
}
