// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neuralNetwork;

import annotation.tailrec;
import collection._;

object Serializer {
	def readLayer(data: Array[String], i: Int): (Layer, Int) = data(i) match {
		case "Array2DLayer" => Array2DLayer.read(data, (i + 1));
		case "MatrixLayer" => MatrixLayer.read(data, (i + 1));
		case "MaxPoolingConvolutionalLayer" => MaxPoolingConvolutionalLayer.read(data, (i + 1));
	}
	
	def readLayers(data: Array[String], i: Int): (Array[Layer], Int) = {
		var j = i;
		
		val nbLayers = data(j).toInt;
		j += 1;
		
		val layers = {
			val tmp = Array.ofDim[Layer](nbLayers);
			
			var k = 0;
			while(k < nbLayers) {
				val x = readLayer(data, j);
				
				tmp(k) = x._1;
				j = x._2;
				
				k += 1;
			}
			
			tmp;
		}
		
		(layers, j);
	}
	
	// Should be the inverse of FunctionToString.apply
	def apply(nonLinearity: Option[NonLinearity with Differentiable]): String = nonLinearity match {
		case None => "None";
		case Some(f) => f.toString; // Not always right
	}
}
