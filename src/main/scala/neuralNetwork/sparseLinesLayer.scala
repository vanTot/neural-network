// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neuralNetwork;

import annotation.tailrec;

class SparseLinesLayer(val p: SparseLinesLayerParam, val nonLinearity: Option[NonLinearity with Differentiable]) extends Layer {
	def apply(in: Array[Double]): Array[Double] = nonLinearity match {
		case Some(f) => f(p.apply(in));
		case None => p.apply(in);
	}
	
	def gradient(in: Array[Double]): (Array[Double], (Array[Double] => (SparseLinesLayerParam, Array[Double]))) = nonLinearity match { // (out[i], (dc/dout[i] => (dparam, dc/din[i])))
		case Some(f) => {
			val u = p.apply(in); // Only the first alpha positions are taken into account
			
			val dF = ((deltaV: Array[Double]) => {
				val tmp = f.derivative(u, deltaV); // dc/d(A.in+B)[i]
			
				// All positions are taken into account
				val dCoeffs = Array.ofDim[Array[Double]](p.height);
				var i = 0;
				while(i < p.height) {
					val lineP = p.positions(i);
				
					val dLine = Array.ofDim[Double](lineP.length);
				
					var j = 0;
					while(j < lineP.length) {
						dLine(j) = tmp(i) * in(lineP(j));
					
						j += 1;
					}
				
					dCoeffs(i) = dLine;
				
					i += 1;
				}
				
				val dP = SparseLinesLayerParam(dCoeffs, null, tmp, p.width, p.alpha); // dc/dparam
				
				// Only the first alpha positions are taken into account
				val dIn = Array.ofDim[Double](p.width); // dc/din[i]
				{var i = 0;
				while(i < p.height) {
					val lineC = p.coeffs(i);
					val lineP = p.positions(i);
					
					var j = 0;
					while(j < p.alpha) {
						dIn(lineP(j)) += lineC(j) * tmp(i);
				
						j += 1;
					}
					
					i += 1;
				}}
				
				(dP, dIn);
			});
			
			(f(u), dF);
			
			/*val x = f.derivative(u);
		
			val dpCondensed = Math.tensorProduct(x, in);
			val dpF = ((deltaV: Array[Double]) => {
				//SparseLinesLayerParam(Math.multiplyLineWise(deltaV, dpCondensed), null, Math.multiply(deltaV, x), p.width, p.alpha);
			
				val dCoeffs = Array.ofDim[Array[Double]](p.height);
				var i = 0;
				while(i < p.height) {
					val lineDpCondensed = dpCondensed(i);
					val lineP = p.positions(i);
				
					val dLine = Array.ofDim[Double](lineP.length);
				
					var j = 0;
					while(j < lineP.length) {
						dLine(j) = deltaV(i) * lineDpCondensed(lineP(j));
					
						j += 1;
					}
				
					dCoeffs(i) = dLine;
				
					i += 1;
				}
			
				SparseLinesLayerParam(dCoeffs, null, Math.multiply(deltaV, x), p.width, p.alpha);
			});
		
			// Only the first alpha positions are taken into account
			val din = Array.ofDim[Double](p.height, p.width);
			{var i = 0;
			while(i < p.height) {
				val line = din(i);
			
				val lineC = p.coeffs(i);
				val lineP = p.positions(i);
			
				var j = 0;
				while(j < p.alpha) {
					line(lineP(j)) = x(i) * lineC(j);
				
					j += 1;
				}
			
				i += 1;
			}}
		
			(f(u), dpF, din);*/
		}
		
		case None => protoGradient(in);
	}
	
	// Here the gradient does not take the non-linearity into account (as if f.derivative = 1)
	def protoGradient(in: Array[Double]): (Array[Double], (Array[Double] => (SparseLinesLayerParam, Array[Double]))) = { // (out[i], (dc/dout[i] => (dparam, dc/din[i])))
		val out = nonLinearity match {
			case Some(f) => f(p.apply(in));
			case None => p.apply(in);
		}
		
		val dF = ((deltaV: Array[Double]) => {
			// All positions are taken into account
			val dCoeffs = Array.ofDim[Array[Double]](p.height);
			var i = 0;
			while(i < p.height) {
				val lineP = p.positions(i);
			
				val dLine = Array.ofDim[Double](lineP.length);
			
				var j = 0;
				while(j < lineP.length) {
					dLine(j) = deltaV(i) * in(lineP(j));
				
					j += 1;
				}
			
				dCoeffs(i) = dLine;
			
				i += 1;
			}
			
			val dP = SparseLinesLayerParam(dCoeffs, null, deltaV, p.width, p.alpha); // dc/dparam
			
			// Only the first alpha positions are taken into account
			val dIn = Array.ofDim[Double](p.width); // dc/din[i]
			{var i = 0;
			while(i < p.height) {
				val lineC = p.coeffs(i);
				val lineP = p.positions(i);
				
				var j = 0;
				while(j < p.alpha) {
					dIn(lineP(j)) += lineC(j) * deltaV(i);
			
					j += 1;
				}
				
				i += 1;
			}}
			
			(dP, dIn);
		});
		
		(out, dF);
		
		/*val dpF = ((deltaV: Array[Double]) => {
			//SparseLinesLayerParam(Math.tensorProduct(deltaV, in), null, deltaV, p.width, p.alpha)
			
			val dCoeffs = Array.ofDim[Array[Double]](p.height);
			var i = 0;
			while(i < p.height) {
				val lineP = p.positions(i);
				
				val dLine = Array.ofDim[Double](lineP.length);
				
				var j = 0;
				while(j < lineP.length) {
					dLine(j) = deltaV(i) * in(lineP(j));
					
					j += 1;
				}
				
				dCoeffs(i) = dLine;
				
				i += 1;
			}
			
			SparseLinesLayerParam(dCoeffs, null, deltaV, p.width, p.alpha);
		});
		
		// Only the first alpha positions are taken into account
		val din = Array.ofDim[Double](p.height, p.width);
		{var i = 0;
		while(i < p.height) {
			val line = din(i);
			
			val lineC = p.coeffs(i);
			val lineP = p.positions(i);
			
			var j = 0;
			while(j < p.alpha) {
				line(lineP(j)) = lineC(j);
				
				j += 1;
			}
			
			i += 1;
		}}
		
		(out, dpF, din);*/
	}
	
	// A copy of the layer with a copy of the parameter
	def deepCopy: SparseLinesLayer = new SparseLinesLayer(p.deepCopy, nonLinearity);
	
	// A copy of the layer with q (not a copy) as parameter
	def duplicate(q: ParameterBrick): SparseLinesLayer = q match {
		case q: SparseLinesLayerParam => new SparseLinesLayer(q, nonLinearity);
		case _ => {throw IncompatibleParameters; this;}
	}
	
	override def toString: String = "SpareLinesLayer((" + p.height + ", " + p.width + "), " + nonLinearity + ")";
}

object SparseLinesLayer {
	def apply(coeffs: Array[Array[Double]], positions: Array[Array[Int]], t: Array[Double], width: Int, alpha: Int, nonLinearity: Option[NonLinearity with Differentiable] = None): SparseLinesLayer = new SparseLinesLayer(SparseLinesLayerParam(coeffs, positions, t, width, alpha), nonLinearity);
	
	def apply(coeffs: Array[Array[Double]], positions: Array[Array[Int]], t: Array[Double], width: Int, alpha: Int, f: NonLinearity with Differentiable): SparseLinesLayer = new SparseLinesLayer(SparseLinesLayerParam(coeffs, positions, t, width, alpha), Some(f));
}

// The positions array indicates which positions the coefficients refer to
case class SparseLinesLayerParam(val coeffs: Array[Array[Double]], val positions: Array[Array[Int]], val t: Array[Double], val width: Int, val alpha: Int) extends LayerParam {
	if(positions != null) reorganize(None); // positions can be null because whee use this class for adagrad history for instance
	
	def height: Int = coeffs.length;
	
	@inline
	def apply(in: Array[Double]): Array[Double] = {
		require(in.length == width, in.length + " !=  " + width);
		
		val res = Array.ofDim[Double](height);
		
		var i = 0;
		while(i < height) {
			val lineC = coeffs(i);
			val lineP = positions(i);
			
			var tmp = t(i);
			var j = 0;
			while(j < alpha) {
				tmp += lineC(j) * in(lineP(j));
				
				j += 1;
			}
			
			res(i) = tmp;
			
			i += 1;
		}
		
		res;
	}
	
	// The parameter is used to synchronize the adagrad history for instance
	@inline
	private def reorganize(optMatrix: Option[Array[Array[Double]]] = None): Unit = {
		@inline
		def aux(i: Int): Unit = {
			val arrayC = coeffs(i);
			val arrayP = positions(i);
			val optArray = optMatrix.map(_(i));
			
			@inline
			def swap(a: Int, b: Int): Unit = {
				val tmpC = arrayC(a);
				arrayC(a) = arrayC(b);
				arrayC(b) = tmpC;
				
				val tmpP = arrayP(a);
				arrayP(a) = arrayP(b);
				arrayP(b) = tmpP;
				
				optArray match {
					case Some(arrayO) => {
						val tmpO = arrayO(a);
						arrayO(a) = arrayO(b);
						arrayO(b) = tmpO;
					}
					
					case None => ();
				}
			}
			
			val pivotId = alpha - 1;
			
			@inline
			def divide(begin: Int, end: Int): (Int, Int) = {
				swap(pivotId, end);
				
				val pivot = math.abs(arrayC(end));
				
				var i = begin;
				while(math.abs(arrayC(i)) > pivot) i += 1;
				val l = i;
				
				var j = i;
				while(i < end) {
					if(math.abs(arrayC(i)) >= pivot) {
						swap(i, j);
						j += 1;
					}
					
					i += 1;
				}
				
				swap(j, end);
				
				// Now, (math.abs(arrayC(k)) < pivot <=> k > j) and (math.abs(arrayC(k)) == pivot => l <= k <= j)
				val k = j;
				
				i = (j - 1);
				while(i >= l) {
					if(math.abs(arrayC(i)) == pivot) {
						swap(i, j - 1);
						j -= 1;
					}
					
					i -= 1;
				}
				
				(j, k); // The (first, last element) == pivot
			}
			
			var begin = 0;
			var end = arrayC.length - 1;
			
			while(begin <= pivotId && end >= pivotId) {
				val (i, j) = divide(begin, end);
				
				if(i > pivotId) end = i - 1;
				else if(j >= pivotId) return ();
				else begin = j + 1;
			}
		}
		
		var i = 0;
		while(i < height) {
			aux(i);
			
			i += 1;
		}
	}
	
	override def update(p: ParameterBrick): Unit = {
		addTo(p);
		reorganize(None);
	}
	
	// This method does not check anything. It only makes sense if the other param has exactly the same structure (each coeff in the same order)
	def addTo(p: ParameterBrick): Unit = p match {
		case SparseLinesLayerParam(coeffs2, _, t2, _, _) => {
			Math.addTo(coeffs, coeffs2);
			Math.addTo(t, t2);
		}
		
		case _ => throw IncompatibleParameterBricks("addTo", List(this, p));
	}
	
	// This method does not check anything. It only makes sense if the other param has exactly the same structure (each coeff in the same order)
	def substractTo(p: ParameterBrick): Unit = p match {
		case SparseLinesLayerParam(coeffs2, _, t2, _, _) => {
			Math.substractTo(coeffs, coeffs2);
			Math.substractTo(t, t2);
		}
		
		case _ => throw IncompatibleParameterBricks("substractTo", List(this, p));
	}
	
	def multiplyTo(x: Double): Unit = {
		Math.multiplyTo(coeffs, x);
		Math.multiplyTo(t, x);
	}
	
	def multiply(x: Double): LayerParam = SparseLinesLayerParam(Math.multiply(coeffs, x), null, Math.multiply(t, x), width, alpha);
	
	def equateTo(p: ParameterBrick): Unit = p match {
		case SparseLinesLayerParam(coeffs2, positions2, t2, width2, alpha2) => {
			Math.equateTo(coeffs, coeffs2);
			Math.equateTo(positions, positions2);
			Math.equateTo(t, t2);
			assert(width == width2);
			//width = width2;
			assert(alpha == alpha2);
			//alpha = alpha2;
		}
		
		case _ => throw IncompatibleParameterBricks("equateTo", List(this, p));
	}
	
	def deepCopy: SparseLinesLayerParam = SparseLinesLayerParam(coeffs.map(_.clone), positions.map(_.clone), t.clone, width, alpha);
	
	def adaUpdate(hist: ParameterBrick, grad: ParameterBrick, adagrad: Double, fudgeFactor: Double): Unit = (hist, grad) match {
		case (SparseLinesLayerParam(cHist, _, tHist, _, _), SparseLinesLayerParam(cGrad, _, tGrad, _, _)) => {
			// matrix
			Math.adaUpdate(coeffs, cHist, cGrad, adagrad, fudgeFactor);
			reorganize(Some(cHist));
			
			// t
			Math.adaUpdate(t, tHist, tGrad, adagrad, fudgeFactor);
		}
		
		case _ => throw IncompatibleParameterBricks("adaUpdate", List(this, hist, grad));
	}
	
	def norm1: Double = structureHandlers.Array2DHandler.norm1(coeffs) + structureHandlers.ArrayHandler.norm1(t);
	
	def norm2Sq: Double = structureHandlers.Array2DHandler.norm2Sq(coeffs) + structureHandlers.ArrayHandler.norm2Sq(t);
	//lazy val norm2Sq_memo: Double = norm2Sq;
	
	def empty: LayerParam = SparseLinesLayerParam(coeffs.map(line => Array.ofDim[Double](line.length)), null, Array.ofDim[Double](t.length), width, alpha);
	
	def nanScan: Boolean = structureHandlers.Array2DHandler.nanScan(coeffs) && structureHandlers.ArrayHandler.nanScan(t);
	
	// sum and max (in abs) + number of params (all params are taken into account)
	def stats: (Double, Double, Int) = {
		val statsC = structureHandlers.Array2DHandler.stats(coeffs);
		val statsT = structureHandlers.ArrayHandler.stats(t);
		
		((statsC._1 + statsT._1), math.max(statsC._2, statsT._2), (statsC._3 + statsT._3));
	}
	
	// Get the value of the coeff at index id
	def getCoeff(id: Int): Double = {
		val tmp = coeffs.head.length;
		val i = id / tmp;
		val j = i % tmp;
		if(i < height) coeffs(i)(j);
		else t(j);
	}
	
	// Add x to the coeff at index id
	def addCoeff(id: Int, x: Double): Unit = {
		val tmp = coeffs.head.length;
		val i = id / tmp;
		val j = i % tmp;
		if(i < height) coeffs(i)(j) += x;
		else t(j) += x;
	}
	
	// Inplace
	def clip(f: (Double => Double)): Unit = {
		structureHandlers.Array2DHandler.clip(coeffs, f);
		structureHandlers.ArrayHandler.clip(t, f);			
	}
}
