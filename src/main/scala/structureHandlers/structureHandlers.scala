// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package structureHandlers;

import annotation.tailrec;
import collection._;

import neuralNetwork.Math;

trait Handler[T] {
	def addTo(e1: T, e2: T): Unit;
	def substractTo(e1: T, e2: T): Unit;
	def multiply(e: T, s: Double): T;
	def multiplyTo(e: T, s: Double): Unit;
	def equateTo(e1: T, e2: T): Unit;
	def norm2Sq(e: T): Double;
	def norm1(e: T): Double;
	//def sign(e: T): T;
	
	def print(e: T): Unit;
}

object ArrayHandler extends Handler[Array[Double]] {
	def addTo(e1: Array[Double], e2: Array[Double]): Unit = {
		Math.addTo(e1, e2);
	}
	
	def substractTo(e1: Array[Double], e2: Array[Double]): Unit = {
		Math.substractTo(e1, e2);
	}
	
	def multiply(e: Array[Double], s: Double): Array[Double] = {
		Math.multiply(e, s);
	}
	
	def multiplyTo(e: Array[Double], s: Double): Unit = {
		Math.multiplyTo(e, s);
	}
	
	def equateTo(e1: Array[Double], e2: Array[Double]): Unit = Math.equateTo(e1, e2);
	
	def norm2Sq(e: Array[Double]): Double = {
		var norm = 0.0;
		
		var i = 0;
		while(i < e.length) {
			norm += math.pow(e(i), 2);
			
			i += 1;
		}
		
		norm;
	}
	
	def norm1(e: Array[Double]): Double = {
		var norm = 0.0;
		
		var i = 0;
		while(i < e.length) {
			norm += math.abs(e(i));
			
			i += 1;
		}
		
		norm;
	}
	
	/*def sign(e: Array[Double]): Array[Double] = {
		
	}*/
	
	def nanScan(e: Array[Double]): Boolean = {
		var i = 0;
		while(i < e.length) {
			if(e(i).isNaN) {
				Console.println(i);
				return true;
			}
			
			i += 1;
		}
		
		return false;
	}
	
	def print(e: Array[Double]): Unit = {
		Console.println("Array[Double]");
		Console.println(e.mkString(", "));
	}
	
	// sum and max (in abs) + size
	def stats(e: Array[Double]): (Double, Double, Int) = {
		var sum = 0.0
		var max = 0.0;
		
		var i = 0;
		while(i < e.size) {
			val abs = math.abs(e(i));
			
			sum += abs;
			if(abs > max) max = abs;
			
			i += 1;
		}
		
		(sum, max, e.size);
	}
	
	// Inplace
	def clip(e: Array[Double], f: (Double => Double)): Unit = {
		var i = 0;
		while(i < e.length) {
			e(i) = f(e(i));
			
			i += 1;
		}
	}
	
	// Custom serialization
	def customSerialize(e: Array[Double], stringBuilder: mutable.StringBuilder): Unit = {
		stringBuilder ++= e.mkString(";");
		stringBuilder += '\n';
	}
	
	// Inverse of serialization
	def read(data: Array[String], i: Int): (Array[Double], Int) = {
		(data(i).split(';').map(_.toDouble), (i + 1));
	}
}

//object ArrayArray2DHandler extends Handler[(Array[Double], Array[Array[Double]])] {
//	def addTo(e1: (Array[Double], Array[Array[Double]]), e2: (Array[Double], Array[Array[Double]])): Unit = {
//		Math.addTo(e1._1, e2._1);
//		Math.addTo(e1._2, e2._2);
//	}
//	
//	def substractTo(e1: (Array[Double], Array[Array[Double]]), e2: (Array[Double], Array[Array[Double]])): Unit = {
//		Math.substractTo(e1._1, e2._1);
//		Math.substractTo(e1._2, e2._2);
//	}
//	
//	def multiply(e: (Array[Double], Array[Array[Double]]), s: Double): (Array[Double], Array[Array[Double]]) = {
//		(Math.multiply(e._1, s),
//		Math.multiply(e._2, s));
//	}
//	
//	def multiplyTo(e: (Array[Double], Array[Array[Double]]), s: Double): Unit = {
//		Math.multiplyTo(e._1, s);
//		Math.multiplyTo(e._2, s);
//	}
//	
//	def equateTo(e1: (Array[Double], Array[Array[Double]]), e2: (Array[Double], Array[Array[Double]])): Unit = ???
//	
//	def norm2Sq(e:(Array[Double], Array[Array[Double]])): Double = {
//		ArrayHandler.norm2Sq(e._1) + Array2DHandler.norm2Sq(e._2);
//	}
//	
//	def norm1(e: (Array[Double], Array[Array[Double]])): Double = {
//		ArrayHandler.norm1(e._1) + Array2DHandler.norm1(e._2);
//	}
//	
//	def print(e: (Array[Double], Array[Array[Double]])): Unit = {
//		Console.println("(Array[Double], Array[Array[Double]])");
//		Console.println(e._1.mkString(", "));
//		Console.println("dim: " + e._2.length);
//		Console.println(e._2.deep.mkString("\n"));
//	}
//}

object Array2DHandler extends Handler[Array[Array[Double]]] {
	def addTo(e1: Array[Array[Double]], e2: Array[Array[Double]]): Unit = {
		Math.addTo(e1, e2);
	}
	
	def addTo(e1: Array[Array[Double]], e2: Map[Int, Array[Double]]): Unit = {
		for(elem <- e2) Math.addTo(e1(elem._1), elem._2);
	}
	
	def substractTo(e1: Array[Array[Double]], e2: Array[Array[Double]]): Unit = {
		Math.substractTo(e1, e2);
	}
	
	def substractTo(e1: Array[Array[Double]], e2: Map[Int, Array[Double]]): Unit = {
		for(elem <- e2) Math.substractTo(e1(elem._1), elem._2);
	}
	
	def multiply(e: Array[Array[Double]], s: Double): Array[Array[Double]] = {
		Math.multiply(e, s);
	}
	
	def multiplyTo(e: Array[Array[Double]], s: Double): Unit = {
		Math.multiplyTo(e, s);
	}
	
	def equateTo(e1: Array[Array[Double]], e2: Array[Array[Double]]): Unit = Math.equateTo(e1, e2);
	
	def norm2Sq(e: Array[Array[Double]]): Double = {
		var norm = 0.0;
		
		var i = 0;
		while(i < e.length) {
			val line = e(i);
			
			var j = 0;
			while(j < line.length) {
				norm += math.pow(line(j), 2);
				
				j += 1;
			}
			
			i += 1;
		}
		
		norm;
	}
	
	def norm1(e: Array[Array[Double]]): Double = {
		var norm = 0.0;
		
		var i = 0;
		while(i < e.length) {
			val line = e(i);
			
			var j = 0;
			while(j < line.length) {
				norm += math.abs(line(j));
				
				j += 1;
			}
			
			i += 1;
		}
		
		norm;
	}
	
	def print(e: Array[Array[Double]]): Unit = {
		Console.println("Array[Array[Double]]");
		Console.println(e.deep.mkString("\n"));
	}
	
	def nanScan(e: Array[Array[Double]]): Boolean = {
		var i = 0;
		while(i < e.length) {
			val line = e(i);
			var j = 0;
			while(j < line.length) {
				if(line(j).isNaN) {
					Console.println(i + ", " + j);
					return true;
				}
				
				j += 1;
			}
			
			i += 1;
		}
		
		return false;
	}
	
	// sum and max (in abs) + size
	def stats(e: Array[Array[Double]]): (Double, Double, Int) = {
		var sum = 0.0
		var max = 0.0;
		
		var i = 0;
		while(i < e.size) {
			val line = e(i);
			
			var j = 0;
			while(j < line.size) {
				val abs = math.abs(line(j));
			
				sum += abs;
				if(abs > max) max = abs;
				
				j += 1;
			}
			
			i += 1;
		}
		
		(sum, max, (e.size * e(0).size));
	}
	
	// Inplace
	def clip(e: Array[Array[Double]], f: (Double => Double)): Unit = {
		var i = 0;
		while(i < e.size) {
			val line = e(i);
			
			var j = 0;
			while(j < line.size) {
				line(j) = f(line(j));
				
				j += 1;
			}
			
			i += 1;
		}
	}
	
	// Custom serialization
	def customSerialize(e: Array[Array[Double]], stringBuilder: mutable.StringBuilder): Unit = {
		stringBuilder ++= e.length.toString;
		stringBuilder += '\n';
		
		var i = 0;
		while(i < e.length) {
			stringBuilder ++= e(i).mkString(";");
			stringBuilder += '\n';
			
			i += 1;
		}
	}
	
	// Inverse of serialization
	def read(data: Array[String], i: Int): (Array[Array[Double]], Int) = {
		var j = i;
		
		val height = data(j).toInt;
		j += 1;
		
		val m = Array.ofDim[Array[Double]](height);
		
		var k = 0;
		while(k < height) {
			m(k) = data(j).split(';').map(_.toDouble);
			j += 1;
			
			k += 1;
		}
		
		(m, j);
	}
}

// TODO This is not a Handler
object SparseArrayHandler {
	def addTo(e1: mutable.Map[Int, Double], e2: mutable.Map[Int, Double]): Unit = {
		for((key, value) <- e2) { // dCosts (shift costs)
			e1(key) = e1.getOrElse(key, 0.0) + value;
		}
	}
	
	def substractTo(e1: mutable.Map[Int, Double], e2: mutable.Map[Int, Double]): Unit = {
		for((key, value) <- e2) { // dCosts (shift costs)
			e1(key) = e1.getOrElse(key, 0.0) - value;
		}
	}
	
	def multiply(e: mutable.Map[Int, Double], s: Double): mutable.Map[Int, Double] = {
		val res = mutable.Map[Int, Double]();
		for((key, value) <- e) res(key) = value * s;
		res;
	}
	
	def multiplyTo(e: mutable.Map[Int, Double], s: Double): Unit = {
		for((key, value) <- e) e(key) = value * s;
	}
	
	def norm2Sq(e: mutable.Map[Int, Double]): Double = e.values.foldLeft(0.0)(_ + math.pow(_, 2));
	
	def norm1(e: mutable.Map[Int, Double]): Double = e.values.foldLeft(0.0)(_ + math.abs(_));
	
	def nanScan(e: mutable.Map[Int, Double]): Boolean = {
		for((key, value) <- e) {
			if(value.isNaN) {
				Console.println(key);
				return true;
			}
		}
		
		return false;
	}
	
	// sum and max (in abs) + size
	def stats(e: mutable.Map[Int, Double]): (Double, Double, Int) = {
		var sum = 0.0
		var max = 0.0;
		
		for(x <- e.values) {
			val abs = math.abs(x);
			
			sum += abs;
			if(abs > max) max = abs;
		}
		
		(sum, max, e.size);
	}
	
	// Inplace
	def clip(e: mutable.Map[Int, Double], f: (Double => Double)): Unit = {
		e.transform { case (k, v) => f(v); }; // TODO maybe mapValues is more appropriate
	}
}

object SparseArray2DHandler extends Handler[mutable.Map[Int, Array[Double]]] {
	def addTo(e1: mutable.Map[Int, Array[Double]], e2: mutable.Map[Int, Array[Double]]): Unit = {
		for(elem <- e2) {
			e1.get(elem._1) match {
				case None => e1(elem._1) = elem._2;
				case Some(vec) => Math.addTo(vec,  elem._2);
			}
		}
	}
	
	def substractTo(e1: mutable.Map[Int, Array[Double]], e2: mutable.Map[Int, Array[Double]]): Unit = {
		for(elem <- e2) {
			e1.get(elem._1) match {
				case None => e1(elem._1) = Math.negate(elem._2);
				case Some(vec) => Math.substractTo(vec,  elem._2);
			}
		}
	}
	
	def multiply(e: mutable.Map[Int, Array[Double]], s: Double): mutable.Map[Int, Array[Double]] = {
		val res = mutable.Map[Int, Array[Double]]();
		for((key, value) <- e) res(key) = Math.multiply(value, s);
		res;
		//e.mapValues(x => Math.multiply(x, s));
	}
	
	def multiplyTo(e: mutable.Map[Int, Array[Double]], s: Double): Unit = {
		for(elem <- e) Math.multiplyTo(elem._2, s);
	}
	
	def equateTo(e1: mutable.Map[Int, Array[Double]], e2: mutable.Map[Int, Array[Double]]): Unit = ???
	
	def norm2Sq(e: mutable.Map[Int, Array[Double]]): Double = {
		var norm = 0.0;
		
		for(x <- e.values) {
			var i = 0;
			while(i < x.length) {
				norm += math.pow(x(i), 2);
				
				i += 1;
			}
		}
		
		norm;
	}
	
	def norm1(e: mutable.Map[Int, Array[Double]]): Double = {
		var norm = 0.0;
		
		for(x <- e.values) {
			var i = 0;
			while(i < x.length) {
				norm += math.abs(x(i));
				
				i += 1;
			}
		}
		
		norm;
	}
	
	def nanScan(e: mutable.Map[Int, Array[Double]]): Boolean = {
		for(x <- e) {
			var j = 0;
			while(j < x._2.length) {
				if(x._2(j).isNaN) {
					Console.println(x._1 + ", " + j);
					return true;
				}
				
				j += 1;
			}
		}
		
		return false;
	}
	
	// sum and max (in abs) + size
	def stats(e: mutable.Map[Int, Array[Double]]): (Double, Double, Int) = {
		var sum = 0.0
		var max = 0.0;
		
		val length: Int = e.headOption match {
			case Some(x) => x._2.length;
			case None => 0;
		}
		
		for(x <- e.values) {
			var i = 0;
			while(i < length) {
				val abs = math.abs(x(i));
			
				sum += abs;
				if(abs > max) max = abs;
				
				i += 1;
			}
		}
		
		(sum, max, (e.size * length));
	}
	
	// Inplace
	def clip(e: mutable.Map[Int, Array[Double]], f: (Double => Double)): Unit = {
		e.transform { case (k, v) => v.map(f); }; // TODO maybe mapValues is more appropriate
	}
	
	def print(e: mutable.Map[Int, Array[Double]]): Unit = {
		Console.println("mutable.Map[Int, Array[Double]]");
		
		for(x <- e) {
			Console.println(x._1 + " -> Array(" + x._2.mkString(", ") + ")");
		}
	}
}

object TensorHandler extends Handler[Array[Array[Array[Double]]]] {
	def addTo(e1: Array[Array[Array[Double]]], e2: Array[Array[Array[Double]]]): Unit = {
		Math.addTo(e1, e2);
	}
	
	def substractTo(e1: Array[Array[Array[Double]]], e2: Array[Array[Array[Double]]]): Unit = {
		Math.substractTo(e1, e2);
	}
	
	def multiply(e: Array[Array[Array[Double]]], s: Double): Array[Array[Array[Double]]] = {
		Math.multiply(e, s);
	}
	
	def multiplyTo(e: Array[Array[Array[Double]]], s: Double): Unit = {
		Math.multiplyTo(e, s);
	}
	
	def equateTo(e1: Array[Array[Array[Double]]], e2: Array[Array[Array[Double]]]): Unit = ???
	
	def norm2Sq(e: Array[Array[Array[Double]]]): Double = {
		var norm = 0.0;
		
		var i = 0;
		while(i < e.length) {
			val m = e(i);
			
			var j = 0;
			while(j < m.length) {
				val l = m(j);
				
				var k = 0;
				while(k < l.length) {
					norm += math.pow(l(k), 2);
					
					k += 1;
				}
				
				j += 1;
			}
			
			i += 1;
		}
		
		norm;
	}
	
	def norm1(e: Array[Array[Array[Double]]]): Double = {
		var norm = 0.0;
		
		var i = 0;
		while(i < e.length) {
			val m = e(i);
			
			var j = 0;
			while(j < m.length) {
				val l = m(j);
				
				var k = 0;
				while(k < l.length) {
					norm += math.abs(l(k));
					
					k += 1;
				}
				
				j += 1;
			}
			
			i += 1;
		}
		
		norm;
	}
	
	def print(e: Array[Array[Array[Double]]]): Unit = {
		Console.println("Array[Array[Array[Double]]]");
		Console.println("dim: " + e.length);
		Console.println(e.deep.mkString("\n"));
	}
}
