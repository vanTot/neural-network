// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import org.junit.Test;
import junit.framework.TestCase;
import org.junit.Assert._;

import collection.mutable;

import neuralNetwork._;

class Array2DLayerTests extends TestCase {
	def test {
		val delta: Double = 1e-15;
		val delta2: Double = 1e-6;
		
		// Check that protoGradient is equivalent to gradient with a dummy linearity
		val height = 12;
		val width = 10;
		
		val m: Array[Array[Double]] = Array.fill[Array[Double]](height)(Array.fill[Double](width)(util.Random.nextDouble));
		val t: Array[Double] = Array.fill[Double](height)(util.Random.nextDouble);
		
		val layerA = Array2DLayer(m, t, Sigmoid);
		
		val dummyF: NonLinearity with Differentiable = new PointWiseNonLinearity with PointWiseDifferentiable {
			def apply(x: Double): Double = Sigmoid(x);
			def derivative(x: Double): Double = 1.0;
		};
		
		val layerB = Array2DLayer(m, t, dummyF);
		
		val in: Array[Double] = Array.fill[Double](width)(util.Random.nextDouble);
		
		val (outA, dFA) = layerA.protoGradient(in);
		val (outB, dFB) = layerB.gradient(in);
		
		assertArrayEquals(outA, outB, delta);
		
		val dE: Array[Double] = Array.fill[Double](height)(util.Random.nextDouble);
		val (dpA, dinA) = dFA(dE);
		val (dpB, dinB) = dFB(dE);
		assertEquals(dpA.m.length, height);
		assertEquals(dpB.m.length, height);
		for(i <- 0 until height) assertArrayEquals(dpA.m(i), dpB.m(i), delta);
		assertArrayEquals(dpA.t, dpB.t, delta);
		
		assertArrayEquals(dinA, dinB, delta);
	}
}
