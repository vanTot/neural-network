// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import org.junit.Test;
import junit.framework.TestCase;
import org.junit.Assert._;

import collection.mutable;

import neuralNetwork._;

class AttentionTests extends TestCase {
	def test {
		val delta: Double = 1e-15;
		val delta2: Double = 1e-6;
		
		val h: Double = 1e-6;
		val threshold: Double = 1e-7;

		// One cannot test non-derivable systems that way
		// So avoid SSL layer and ReLU non-linearity
		
		{val contextSize = 32;
		val inputSize = 16;
		
		val attention = Attention(contextSize, inputSize);
		assertTrue(attention.testGradient(h, threshold));}
	}
}
