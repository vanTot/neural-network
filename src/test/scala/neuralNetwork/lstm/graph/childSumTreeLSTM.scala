// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import org.junit.Test;
import junit.framework.TestCase;
import org.junit.Assert._;

import collection.mutable;

import neuralNetwork.lstm.graph._;
import neuralNetwork._;

class ChildSumTreeLSTMTests extends TestCase {
	def test {
		val delta: Double = 1e-15;
		val delta2: Double = 1e-6;
		
		val leaves = { // Very simple ChildSumTreeLSTMs
			val m: Array[Array[Double]] = Array(Array(-1.0, 2.0));
			val t: Array[Double] = Array(-0.5);
			
			val layer1 = Array2DLayer(m, t, Sigmoid);
			val NN1 = TransformNN(List(layer1));
			
			val layer2 = Array2DLayer(m, t, Tanh);
			val NN2 = TransformNN(List(layer2));
			
			List(0, 1, 2).map(x => new LeafLChildSumTreeLSTMImp(
				NN1,
				NN1,
				NN2,
				NN1,
				Array(x + 1.0), // cellState
				Array(x + 2.0), // output
				new Array[Double](1), // dCellState
				new Array[Double](1) // dOutput
			));
		}
		
		val in = Array(4.0);
		
		val nn = leaves.head.update(leaves.tail, in);
		
		val nCS = (
			(1.0 * Sigmoid(5.5))           // remembered from 1st child
			+ (2.0 * Sigmoid(4.5))         // remembered from 2nd child
			+ (3.0 * Sigmoid(3.5))         // remembered from 3rd child
			+ (Tanh(-1.5) * Sigmoid(-1.5)) // memorized from input
		);
		
		val tanhNCS = Tanh(nCS); // Tanh of new cell state
		
		assertArrayEquals(nn.output, Array(tanhNCS * Sigmoid(-1.5)), delta);
		
		val dc1 = 0.5;
		nn.dCellState(0) = dc1;
		val dc2 = 1.5;
		nn.dOutput(0) = dc2;
		
		val (_, dIn) = nn.gradient;
		
		val dCS = dc2 * Sigmoid(-1.5) * (1.0 - tanhNCS * tanhNCS) + dc1;
		
		(leaves, List(0, 1, 2)).zipped.foreach((leaf, x) => {
			assertEquals(leaf.dCellState(0), dCS * Sigmoid(5.5 - x), delta);
			
			assertEquals(leaf.dOutput(0), (
				dc2 * tanhNCS * Sigmoid.derivative(-1.5) * -1.0        // filter part
				+ dCS * (x + 1.0) * Sigmoid.derivative(5.5 - x) * -1.0 // candidate part
				+ dCS * Tanh(-1.5) * Sigmoid.derivative(-1.5) * -1.0   // memorizeGate part
				+ dCS * Sigmoid(-1.5) * Tanh.derivative(-1.5) * -1.0   // forgetGate part
			), delta);
		});
		
		assertEquals(dIn(0), (
			dc2 * tanhNCS * Sigmoid.derivative(-1.5) * 2.0      // filter part
			+ dCS * 1.0 * Sigmoid.derivative(5.5) * 2.0         // 1st child candidate part
			+ dCS * 2.0 * Sigmoid.derivative(4.5) * 2.0         // 2nd child candidate part
			+ dCS * 3.0 * Sigmoid.derivative(3.5) * 2.0         // 3rd child candidate part
			+ dCS * Tanh(-1.5) * Sigmoid.derivative(-1.5) * 2.0 // memorizeGate part
			+ dCS * Sigmoid(-1.5) * Tanh.derivative(-1.5) * 2.0 // forgetGate part
		), delta);
	}
}
