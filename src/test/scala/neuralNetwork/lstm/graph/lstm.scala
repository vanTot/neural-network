// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import org.junit.Test;
import junit.framework.TestCase;
import org.junit.Assert._;

import collection.mutable;

import neuralNetwork.lstm.graph._;
import neuralNetwork._;

class GraphLSTMTests extends TestCase {
	def test {
		val delta: Double = 1e-15;
		val delta2: Double = 1e-6;
		
		// Very very simple LSTM
		val initLSTM = {
			val m: Array[Array[Double]] = Array(Array(-1.0, 1.0)); // TODO don't use 1.0
			val t: Array[Double] = Array(-0.5);
			
			val layer = Array2DLayer(m, t, Sigmoid);
			val NN = TransformNN(List(layer));
			
			val cellState = Array(1.0); // TODO don't use 1.0
			val output = Array(2.0);
			
			new LeafLLSTMImp(
				NN,
				NN,
				NN,
				cellState,
				output,
				new Array[Double](1),
				new Array[Double](1)
			);
		}
		
		val in = Array(3.0);
		
		val nn = initLSTM.update(in);
		
		val nCS = (2.0 - Sigmoid(0.5)) * Sigmoid(0.5); // new cell state
		val tanhNCS = math.tanh(nCS); // Tanh of new cell state
		
		assertArrayEquals(nn.output, Array(tanhNCS * Sigmoid(0.5)), delta);
		
		val dc1 = 0.5;
		nn.dCellState(0) = dc1;
		val dc2 = 1.5;
		nn.dOutput(0) = dc2;
		
		val (_, dIn) = nn.gradient;
		
		val dCS = dc2 * Sigmoid(0.5) * (1.0 - tanhNCS * tanhNCS) + dc1;
		
		assertEquals(initLSTM.dCellState(0), dCS * Sigmoid(0.5), delta);
		
		assertEquals(initLSTM.dOutput(0),
			dc2 * tanhNCS * Sigmoid.derivative(0.5) * -1.0 // filter part
			+ dCS * (1.0 - Sigmoid(0.5)) * Sigmoid.derivative(0.5) * -1.0 // candidate part
			+ dCS * (1.0 - Sigmoid(0.5)) * Sigmoid.derivative(0.5) * -1.0 // forgetGate part
		, delta);
		
		assertEquals(dIn(0), 
			dc2 * tanhNCS * Sigmoid.derivative(0.5) * 1.0 // filter part
			+ dCS * (1.0 - Sigmoid(0.5)) * Sigmoid.derivative(0.5) * 1.0 // candidate part
			+ dCS * (1.0 - Sigmoid(0.5)) * Sigmoid.derivative(0.5) * 1.0 // forgetGate part
		, delta);
	}
}
