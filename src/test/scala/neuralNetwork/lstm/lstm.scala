// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import org.junit.Test;
import junit.framework.TestCase;
import org.junit.Assert._;

import collection.mutable;

import neuralNetwork.lstm._;
import neuralNetwork._;

class LSTMTests extends TestCase {
	def test {
		val delta: Double = 1e-15;
		val delta2: Double = 1e-6;
		
		val h: Double = 1e-6;
		val threshold: Double = 1e-6;

		// One cannot test non-derivable systems that way
		// So avoid SSL layer and ReLU non-linearity
		
		// LSTM
		{val memorySize = 32;
		val inSize = 16;
		
		val network = LSTMFactory(memorySize, inSize);
		assertTrue(network.testGradient(h, threshold));}
		
		// BinaryLSTM
		{val memorySize = 32;
		val inSize = 16;
		
		val network = BinaryLSTMFactory(memorySize, inSize);
		assertTrue(network.testGradient(h, threshold));}
		
		// LSTM serialization
		{val lstm = LSTMFactory(32, 16);
		
		val str = {
			val stringBuilder = new mutable.StringBuilder();
			lstm.customSerialize(stringBuilder);
			
			stringBuilder.toString;
		}
		
		val lstm2 = LearningLSTM.read(str);
		
		for(_ <- 0 until 10) {
			val in = Array.fill(lstm.width)(util.Random.nextDouble - 0.5);
			
			val out = lstm.executeArray(in);
			val out2 = lstm2.executeArray(in);
			
			assertArrayEquals(out, out2, delta);
		}}
		
		// LSTMEncoder
		{val inputSize = 16;
		val outputSize = 16;

		val encoder = LSTMEncoder(inputSize, outputSize);
		assertTrue(encoder.testGradient(h, threshold));}

		// BiLSTMEncoder
		{val inputSize = 16;
		val forwardSize = 10;
		val backwardSize = 6;
		
		val encoder = BiLSTMEncoder(inputSize, forwardSize, backwardSize);
		assertTrue(encoder.testGradient(h, threshold));}
	}
}
