// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import org.junit.Test;
import junit.framework.TestCase;
import org.junit.Assert._;

import collection.mutable;

import neuralNetwork._;

class MathTests extends TestCase {
	def test {
		val delta: Double = 1e-15;
		val delta2: Double = 1e-6;
		
		// square(x: Double): Double
		for(_ <- 0 to 10) {val x = util.Random.nextDouble;
		assertEquals(Math.square(x), math.pow(x, 2), delta);}
		
		// crossEntropy(p: Array[Double], q: Array[Double]): Double
		assertEquals(Math.crossEntropy(Array(0.0, 0.2, 0.8), Array(0.0, 0.3, 0.7)), -(0.2 * math.log(0.3) + 0.8 * math.log(0.7)), delta);
		assertEquals(Math.crossEntropy(Array(0.8, 0.05, 0.15), Array(0.1, 0.2, 0.7)), -(0.8 * math.log(0.1) + 0.05 * math.log(0.2) + 0.15 * math.log(0.7)), delta);
		
		// indexOfMax(v: Array[Double]): Int
		for(i <- 0 to 10) {
			val input = Array.fill(11)(util.Random.nextDouble);
			input(i) += 1.1;
			val j = util.Random.nextInt(11);
			input(j) = input(i);
			assertEquals(Math.indexOfMax(input), math.min(i, j));
		}
		
		// weightedSum(vectors: Array[Array[Double]], weights: Array[Double]): Array[Double]
		{val v1 = Array(0.1, 0.2, 0.3, 0.4);
		val v2 = Array(-0.2, 0.3, 0.4, -0.5);
		val v3 = Array(1.0, 0.0, 0.3, 0.0);
		val res = Math.weightedSum(Array(v1, v2, v3), Array(10, -1, 0.5));
		assertArrayEquals(res, Array(1.7, 1.7, 2.75, 4.5), delta);}

		// weightedSum(vectors: Seq[Array[Double]], weights: Seq[Double]): Array[Double]
		{val v1 = Array(0.1, 0.2, 0.3, 0.4);
		val v2 = Array(-0.2, 0.3, 0.4, -0.5);
		val v3 = Array(1.0, 0.0, 0.3, 0.0);
		val res = Math.weightedSum(Seq(v1, v2, v3), Seq(10, -1, 0.5));
		assertArrayEquals(res, Array(1.7, 1.7, 2.75, 4.5), delta);}

		// sum(vectors: Array[Array[Double]]): Array[Double]
		{val v1 = Array(0.1, 0.2, 0.3, 0.4);
		val v2 = Array(-0.2, 0.3, 0.4, -0.5);
		val v3 = Array(1.0, 0.0, 0.3, 0.0);
		val res = Math.sum(Array(v1, v2, v3));
		assertArrayEquals(res, Array(0.9, 0.5, 1.0, -0.1), delta);}
		
		// sum(vectors: List[Array[Double]]): Array[Double]
		{val v1 = Array(0.1, 0.2, 0.3, 0.4);
		val v2 = Array(-0.2, 0.3, 0.4, -0.5);
		val v3 = Array(1.0, 0.0, 0.3, 0.0);
		val res = Math.sum(List(v1, v2, v3));
		assertArrayEquals(res, Array(0.9, 0.5, 1.0, -0.1), delta);}
		
		// arrayMap[@specialized(Int, Double) T](v: Array[T], f: (T => T)): Array[T]
		for(_ <- 0 to 10) {
			val input = Array.fill(10)(util.Random.nextDouble);
			val res = Math.arrayMap(input, math.pow(_, 2));
			assertArrayEquals(res, input.map(math.pow(_, 2)), delta)
		}
		
		// norm2Sq(v: IndexedSeq[Double]): Double
		{val res = Math.norm2Sq(Array(1.0, 2.0, -1.0));
		assertEquals(res, 6.0, delta);}
		
		// selectFromProb(prob: IndexedSeq[Double]): Int
		{val res = Math.selectFromProb(Array(1.0, 0.0, 0.0, 0.0));
		assertEquals(res, 0);}
		{val res = Math.selectFromProb(Array(0.0, 1.0, 0.0, 0.0));
		assertEquals(res, 1);}
		{val res = Math.selectFromProb(Array(0.0, 0.0, 1.0, 0.0));
		assertEquals(res, 2);}
		{val res = Math.selectFromProb(Array(0.0, 0.0, 0.0, 1.0));
		assertEquals(res, 3);}
		
		// normalizeTo(v: Array[Double]): Unit
		{val input = Array(0.1, 0.2, 0.3, 0.4);
		Math.normalizeTo(input);
		assertArrayEquals(input, Array(0.1, 0.2, 0.3, 0.4), delta);}
		{val input = Array(0.2, 0.4, 0.6, 0.8);
		Math.normalizeTo(input);
		assertArrayEquals(input, Array(0.1, 0.2, 0.3, 0.4), delta);}
		{val input = Array(0.02, 0.04, 0.06, 0.08);
		Math.normalizeTo(input);
		assertArrayEquals(input, Array(0.1, 0.2, 0.3, 0.4), delta);}
		
		// atanh(x: Double): Double
		for(_ <- 0 to 10) {val x = util.Random.nextDouble * 2 - 1.0;
		assertEquals(math.tanh(Math.atanh(x)), x, delta);}
		for(_ <- 0 to 10) {val x = util.Random.nextDouble * 20 - 10.0;
		assertEquals(Math.atanh(math.tanh(x)), x, delta2);}
		
		// softMax(v: Array[Double]): Array[Double]
		{val x = math.E + 3.0;
		val res = Math.softMax(Array(1.0, 0.0, 0.0, 0.0));
		assertArrayEquals(res, Array((math.E / x), (1.0 / x), (1.0 / x), (1.0 / x)), delta);}
		{val x = math.E + 3.0;
		val res = Math.softMax(Array(0.0, 1.0, 0.0, 0.0));
		assertArrayEquals(res, Array((1.0 / x), (math.E / x), (1.0 / x), (1.0 / x)), delta);}
		{val x = math.E + 3.0;
		val res = Math.softMax(Array(0.0, 0.0, 1.0, 0.0));
		assertArrayEquals(res, Array((1.0 / x), (1.0 / x), (math.E / x), (1.0 / x)), delta);}
		{val x = math.E + 3.0;
		val res = Math.softMax(Array(0.0, 0.0, 0.0, 1.0));
		assertArrayEquals(res, Array((1.0 / x), (1.0 / x), (1.0 / x), (math.E / x)), delta);}
		{val res = Math.softMax(Array(0.0, 0.0, 0.0, 1000.0));
		assertArrayEquals(res, Array(0.0, 0.0, 0.0, 1.0), delta);}
		
		// softMax(v: List[Double]): List[Double]
		for(_ <- 0 to 10) {
			val input = Array.fill(10)(util.Random.nextDouble * 10);
			assertArrayEquals(Math.softMax(input), Math.softMax(input.toList).toArray, delta);
		}
		
		// sigmoid(x: Double): Double
		{val res = Math.sigmoid(0.0); // sig(0) = 1/2
		assertEquals(res, 0.5, delta);}
		{val res = Math.sigmoid(999999999.9); // sig < 1
		assertTrue(res <= 1.0);}
		{val res = Math.sigmoid(-999999999.9); // sig > 0
		assertTrue(res >= 0.0);}
		for(_ <- 0 to 10) {var x = util.Random.nextDouble; // sig(-x) = 1 - sig(x)
		assertEquals(Math.sigmoid(x), 1.0 - Math.sigmoid(-x), delta);};
		
		// multiplyTo(v: Array[Double], s: Double): Unit
		{val input = Array(0.1, 0.2, 0.3, 0.4);
		Math.multiplyTo(input, 1.0);
		assertArrayEquals(input, Array(0.1, 0.2, 0.3, 0.4), delta);}
		{val input = Array(0.1, 0.2, 0.3, 0.4);
		Math.multiplyTo(input, -2.0);
		assertArrayEquals(input, Array(-0.2, -0.4, -0.6, -0.8), delta);}
		
		// multiplyTo(m: Array[Array[Double]], s: Double): Unit
		{val input = Array(Array(0.1, 0.2, 0.3, 0.4), Array(-0.1, -0.2, -0.3, -0.4));
		Math.multiplyTo(input, 1.0);
		assertArrayEquals(input(0), Array(0.1, 0.2, 0.3, 0.4), delta);
		assertArrayEquals(input(1), Array(-0.1, -0.2, -0.3, -0.4), delta);}
		{val input = Array(Array(0.1, 0.2, 0.3, 0.4), Array(-0.1, -0.2, -0.3, -0.4));
		Math.multiplyTo(input, -1.0);
		assertArrayEquals(input(0), Array(-0.1, -0.2, -0.3, -0.4), delta);
		assertArrayEquals(input(1), Array(0.1, 0.2, 0.3, 0.4), delta);}
		
		// multiplyTo(t: Array[Array[Array[Double]]], s: Double): Unit
		{val input = Array(Array(Array(0.1, 0.2, 0.3, 0.4), Array(-0.1, -0.2, -0.3, -0.4)), Array(Array(-0.1, -0.2, -0.3, -0.4), Array(0.1, 0.2, 0.3, 0.4)));
		Math.multiplyTo(input, 1.0);
		assertArrayEquals(input(0)(0), Array(0.1, 0.2, 0.3, 0.4), delta);
		assertArrayEquals(input(0)(1), Array(-0.1, -0.2, -0.3, -0.4), delta);
		assertArrayEquals(input(1)(0), Array(-0.1, -0.2, -0.3, -0.4), delta);
		assertArrayEquals(input(1)(1), Array(0.1, 0.2, 0.3, 0.4), delta);}
		{val input = Array(Array(Array(0.1, 0.2, 0.3, 0.4), Array(-0.1, -0.2, -0.3, -0.4)), Array(Array(-0.1, -0.2, -0.3, -0.4), Array(0.1, 0.2, 0.3, 0.4)));
		Math.multiplyTo(input, -1.0);
		assertArrayEquals(input(0)(0), Array(-0.1, -0.2, -0.3, -0.4), delta);
		assertArrayEquals(input(0)(1), Array(0.1, 0.2, 0.3, 0.4), delta);
		assertArrayEquals(input(1)(0), Array(0.1, 0.2, 0.3, 0.4), delta);
		assertArrayEquals(input(1)(1), Array(-0.1, -0.2, -0.3, -0.4), delta);}
		
		// multiply(v: Array[Double], s: Double): Array[Double]
		{val res = Math.multiply(Array(0.1, 0.2, 0.3, 0.4), 1.0);
		assertArrayEquals(res, Array(0.1, 0.2, 0.3, 0.4), delta);}
		{val res = Math.multiply(Array(0.1, 0.2, 0.3, 0.4), -2.0);
		assertArrayEquals(res, Array(-0.2, -0.4, -0.6, -0.8), delta);}
		
		// multiply(m: Array[Array[Double]], s: Double): Array[Array[Double]]
		{val res = Math.multiply(Array(Array(0.1, 0.2, 0.3, 0.4), Array(-0.1, -0.2, -0.3, -0.4)), 1.0);
		assertArrayEquals(res(0), Array(0.1, 0.2, 0.3, 0.4), delta);
		assertArrayEquals(res(1), Array(-0.1, -0.2, -0.3, -0.4), delta);}
		{val res = Math.multiply(Array(Array(0.1, 0.2, 0.3, 0.4), Array(-0.1, -0.2, -0.3, -0.4)), -1.0);
		assertArrayEquals(res(0), Array(-0.1, -0.2, -0.3, -0.4), delta);
		assertArrayEquals(res(1), Array(0.1, 0.2, 0.3, 0.4), delta);}
		
		// multiply(m: Array[Array[Array[Double]]], s: Double): Array[Array[Array[Double]]]
		{val res = Math.multiply(Array(Array(Array(0.1, 0.2, 0.3, 0.4), Array(-0.1, -0.2, -0.3, -0.4)), Array(Array(-0.1, -0.2, -0.3, -0.4), Array(0.1, 0.2, 0.3, 0.4))), 1.0);
		assertArrayEquals(res(0)(0), Array(0.1, 0.2, 0.3, 0.4), delta);
		assertArrayEquals(res(0)(1), Array(-0.1, -0.2, -0.3, -0.4), delta);
		assertArrayEquals(res(1)(0), Array(-0.1, -0.2, -0.3, -0.4), delta);
		assertArrayEquals(res(1)(1), Array(0.1, 0.2, 0.3, 0.4), delta);}
		{val res = Math.multiply(Array(Array(Array(0.1, 0.2, 0.3, 0.4), Array(-0.1, -0.2, -0.3, -0.4)), Array(Array(-0.1, -0.2, -0.3, -0.4), Array(0.1, 0.2, 0.3, 0.4))), -1.0);
		assertArrayEquals(res(0)(0), Array(-0.1, -0.2, -0.3, -0.4), delta);
		assertArrayEquals(res(0)(1), Array(0.1, 0.2, 0.3, 0.4), delta);
		assertArrayEquals(res(1)(0), Array(0.1, 0.2, 0.3, 0.4), delta);
		assertArrayEquals(res(1)(1), Array(-0.1, -0.2, -0.3, -0.4), delta);}
		
		// multiply(t: Array[Array[Array[Double]]], v: Array[Double]): Array[Double]
		{val res = Math.multiply(Array(Array(Array(0.1, 0.2, 0.3), Array(-0.1, -0.2, -0.3), Array(1.0, 0.0, 0.0)), Array(Array(-0.1, -0.2, -0.3), Array(0.0, -1.0, 0.0), Array(0.1, 0.2, 0.3))), Array(1.0, 2.0, 3.0));
		assertArrayEquals(res, Array(1.6, -1.2), delta2);}
		
		// multiply(v1: Array[Double], v2: Array[Double]): Array[Double]
		{val res = Math.multiply(Array(0.1, 0.2, 0.3, 0.4), Array(1.0, -1.0, 2.0, -2.0));
		assertArrayEquals(res, Array(0.1, -0.2, 0.6, -0.8), delta);}
		
		// multiplyTo(v1: Array[Double], v2: Array[Double]): Unit
		{val input = Array(0.1, 0.2, 0.3, 0.4);
		Math.multiplyTo(input, Array(1.0, -1.0, 2.0, -2.0));
		assertArrayEquals(input, Array(0.1, -0.2, 0.6, -0.8), delta);}
		
		// sumCols(m: Array[Array[Double]]): Array[Double]
		{val res = Math.sumCols(Array(Array(0.1, 0.2, 0.3, 0.4), Array(0.1, -0.2, 0.3, -0.4)));
		assertArrayEquals(res, Array(0.2, 0.0, 0.6, 0.0), delta);}
		
		// multiplyLineWise(v: Array[Double], m: Array[Array[Double]]): Array[Array[Double]]
		{val res = Math.multiplyLineWise(Array(1.0, -2.0), Array(Array(0.1, 0.2, 0.3, 0.4), Array(0.1, -0.2, 0.3, -0.4)));
		assertArrayEquals(res(0), Array(0.1, 0.2, 0.3, 0.4), delta);
		assertArrayEquals(res(1), Array(-0.2, 0.4, -0.6, 0.8), delta);}
		
		// multiplyLayerWise(v: Array[Double], t: Array[Array[Array[Double]]]): Array[Array[Array[Double]]]
		{val res = Math.multiplyLayerWise(Array(1.0, -2.0),Array(Array(Array(0.1, 0.2, 0.3, 0.4), Array(-0.1, -0.2, -0.3, -0.4)), Array(Array(-0.1, -0.2, -0.3, -0.4), Array(0.1, 0.2, 0.3, 0.4))));
		assertArrayEquals(res(0)(0), Array(0.1, 0.2, 0.3, 0.4), delta);
		assertArrayEquals(res(0)(1), Array(-0.1, -0.2, -0.3, -0.4), delta);
		assertArrayEquals(res(1)(0), Array(0.2, 0.4, 0.6, 0.8), delta);
		assertArrayEquals(res(1)(1), Array(-0.2, -0.4, -0.6, -0.8), delta);}
		
		// matApply(m: Array[Array[Double]], v: Array[Double]): Array[Double]
		{val res = Math.matApply(Array(Array(0.1, 0.2, 0.3, 0.4), Array(0.1, -0.2, 0.3, -0.4)), Array(1.0, -2.0, 3.0, -4.0));
		assertArrayEquals(res, Array(-1.0, 3.0), delta);}
		
		// matApply(m: Array[Double], width: Int, height: Int, v: Array[Double]): Array[Double]
		{val res = Math.matApply(Array(0.1, 0.2, 0.3, 0.4, 0.1, -0.2, 0.3, -0.4), 4, 2, Array(1.0, -2.0, 3.0, -4.0));
		assertArrayEquals(res, Array(-1.0, 3.0), delta);}
		
		// matApplyTransposed(m: Array[Array[Double]], v: Array[Double]): Array[Double]
		{val res = Math.matApplyTransposed(Array(Array(0.1, 0.2, 0.3, 0.4), Array(0.1, -0.2, 0.3, -0.4)), Array(1.0, -2.0));
		assertArrayEquals(res, Array(-0.1, 0.6, -0.3, 1.2), delta);}
		
		// matApplyTransposed(m: Array[Double], width: Int, height: Int, v: Array[Double]): Array[Double]
		{val res = Math.matApplyTransposed(Array(0.1, 0.2, 0.3, 0.4, 0.1, -0.2, 0.3, -0.4), 4, 2, Array(1.0, -2.0));
		assertArrayEquals(res, Array(-0.1, 0.6, -0.3, 1.2), delta);}
		
		// dotProduct(v1: Array[Double], v2: Array[Double]): Double
		{val res = Math.dotProduct(Array(1.0, 2.0, 3.0), Array(3.0, -2.0, 1.0));
		assertEquals(res, 2.0, delta);}
		
		// tensorProduct2D(v1: Array[Double], v2: Array[Double]): Array[Array[Double]]
		{val res = Math.tensorProduct2D(Array(1.0, -2.0, 3.0), Array(3.0, -2.0, 1.0));
		assertArrayEquals(res(0), Array(3.0, -2.0, 1.0), delta);
		assertArrayEquals(res(1), Array(-6.0, 4.0, -2.0), delta);
		assertArrayEquals(res(2), Array(9.0, -6.0, 3.0), delta);}
		
		// tensorProductMat(v1: Array[Double], v2: Array[Double]): Array[Double]
		{val res = Math.tensorProductMat(Array(1.0, -2.0, 3.0), Array(3.0, -2.0, 1.0));
		assertArrayEquals(res, Array(3.0, -2.0, 1.0, -6.0, 4.0, -2.0, 9.0, -6.0, 3.0), delta);}
		
		// matMultiply(m1: Array[Array[Double]], m2: Array[Array[Double]]): Array[Array[Double]]
		{val res = Math.matMultiply(Array(Array(0.1, 0.2, 0.3), Array(0.1, -0.2, 0.3)), Array(Array(1.0, 2.0), Array(1.0, -2.0), Array(3.0, 4.0)));
		assertArrayEquals(res(0), Array(1.2, 1.0), delta);
		assertArrayEquals(res(1), Array(0.8, 1.8), delta);}
		
		// add(m1: Array[Array[Double]], m2: Array[Array[Double]]): Array[Array[Double]]
		{val res = Math.add(Array(Array(0.1, 0.2, 0.3), Array(0.3, 0.2, 0.1)), Array(Array(-0.1, -0.2, -0.3), Array(0.2, 0.4, 0.6)));
		assertArrayEquals(res(0), Array(0.0, 0.0, 0.0), delta);
		assertArrayEquals(res(1), Array(0.5, 0.6, 0.7), delta);}
		
		// addTo(m1: Array[Array[Double]], m2: Array[Array[Double]]): Unit
		{val input = Array(Array(0.1, 0.2, 0.3), Array(0.3, 0.2, 0.1));
		Math.addTo(input, Array(Array(-0.1, -0.2, -0.3), Array(0.2, 0.4, 0.6)));
		assertArrayEquals(input(0), Array(0.0, 0.0, 0.0), delta);
		assertArrayEquals(input(1), Array(0.5, 0.6, 0.7), delta);}
		
		// substractTo(m1: Array[Array[Double]], m2: Array[Array[Double]]): Unit
		{val input = Array(Array(0.1, 0.2, 0.3), Array(0.3, 0.2, 0.1));
		Math.substractTo(input, Array(Array(-0.1, -0.2, -0.3), Array(0.2, 0.4, 0.6)));
		assertArrayEquals(input(0), Array(0.2, 0.4, 0.6), delta);
		assertArrayEquals(input(1), Array(0.1, -0.2, -0.5), delta);}
		
		// add(v1: Array[Double], v2: Array[Double]): Array[Double]
		{val res = Math.add(Array(0.1, 0.2, 0.3), Array(0.6, -0.4, 0.2));
		assertArrayEquals(res, Array(0.7, -0.2, 0.5), delta);}
		
		// addTo(v1: Array[Double], v2: Array[Double]): Unit
		{val input = Array(0.1, 0.2, 0.3);
		Math.addTo(input, Array(0.6, -0.4, 0.2));
		assertArrayEquals(input, Array(0.7, -0.2, 0.5), delta);}
		
		// addTo(v1: Array[Double], v2: Array[Double], pos: Int): Unit
		{val vec = Array(0.0, 0.1, 0.2, 0.3, 0.4, 0.5);
		val u: Array[Double] = Array();
		val v: Array[Double] = Array(1.0, 2.0, 4.0);
		Math.addTo(u, vec, 3);
		assertArrayEquals(u, new Array[Double](0), delta);
		Math.addTo(v, vec, 0);
		assertArrayEquals(v, Array(1.0, 2.1, 4.2), delta);
		Math.addTo(v, vec, 1);
		assertArrayEquals(v, Array(1.1, 2.3, 4.5), delta);
		Math.addTo(v, vec, 2);
		assertArrayEquals(v, Array(1.3, 2.6, 4.9), delta);
		Math.addTo(v, vec, 3);
		assertArrayEquals(v, Array(1.6, 3.0, 5.4), delta);}
		
		// substract(v1: Array[Double], v2: Array[Double]): Array[Double]
		{val res = Math.substract(Array(0.1, 0.2, 0.3), Array(0.6, -0.4, 0.2));
		assertArrayEquals(res, Array(-0.5, 0.6, 0.1), delta);}
		
		// substractTo(v1: Array[Double], v2: Array[Double]): Unit
		{val input = Array(0.1, 0.2, 0.3);
		Math.substractTo(input, Array(0.6, -0.4, 0.2));
		assertArrayEquals(input, Array(-0.5, 0.6, 0.1), delta);}
		
		// addToWeighted(v1: Array[Double], v2: Array[Double], c: Double): Unit
		{val v1 = Array(1.0, 2.0, 3.0);
		val v2 = Array(-2.0, 3.0, -4.0);
		Math.addToWeighted(v1, v2, 0.1);
		assertArrayEquals(v1, Array(0.8, 2.3, 2.6), delta);}
		
		// equateTo(v1: Array[Double], v2: Array[Double]): Unit
		{val v1 = Array(0.0, 1.0, 2.0, 3.0, 4.0);
		val v2 = Array.fill(5)(util.Random.nextDouble);
		Math.equateTo(v1, v2);
		assertArrayEquals(v1, v2, delta);}
		
		// equateTo[@specialized(Int, Double) T](m1: Array[Array[T]], m2: Array[Array[T]]): Unit
		{val m1 = Array.fill(3, 4)(util.Random.nextDouble);
		val m2 = Array.fill(3, 4)(util.Random.nextDouble + 0.5);
		Math.equateTo(m1, m2);
		for(i <- 0 until 3) assertArrayEquals(m1(i), m2(i), delta);}
		
		// adaUpdate(m: Array[Array[Double]], mHist: Array[Array[Double]], mGrad: Array[Array[Double]], adagrad: Double, fudgeFactor: Double): Unit
		{val m = Array(Array(1.0, 2.0, 3.0), Array(4.0, -5.0, 6.0));
		val mHist = Array(Array(1.0, 2.0, 3.0), Array(4.0, 5.0, 6.0));
		val mGrad = Array(Array(1.0, 2.0, 3.0), Array(0.1, 0.2, 0.3));
		Math.adaUpdate(m, mHist, mGrad, 2.0, 0.1);
		assertArrayEquals(m(0), Array((1.0 + 2.0 / (0.1 + math.sqrt(2.0))), (2.0 + 4.0 / (0.1 + math.sqrt(6.0))), (3.0 + 6.0 / (0.1 + math.sqrt(12.0)))), delta);
		assertArrayEquals(m(1), Array((4.0 + 0.2 / (0.1 + math.sqrt(4.01))), (-5.0 + 0.4 / (0.1 + math.sqrt(5.04))), (6.0 + 0.6 / (0.1 + math.sqrt(6.09)))), delta);
		assertArrayEquals(mHist(0), Array(2.0, 6.0, 12.0), delta);
		assertArrayEquals(mHist(1), Array(4.01, 5.04, 6.09), delta);
		assertArrayEquals(mGrad(0), Array(1.0, 2.0, 3.0), delta);
		assertArrayEquals(mGrad(1), Array(0.1, 0.2, 0.3), delta);}
		
		// adaUpdate(t: Array[Double], tHist: Array[Double], tGrad: Array[Double], adagrad: Double, fudgeFactor: Double): Unit
		{val m = Array(4.0, -5.0, 6.0);
		val mHist = Array(4.0, 5.0, 6.0);
		val mGrad = Array(0.1, 0.2, 0.3);
		Math.adaUpdate(m, mHist, mGrad, 2.0, 0.1);
		assertArrayEquals(m, Array((4.0 + 0.2 / (0.1 + math.sqrt(4.01))), (-5.0 + 0.4 / (0.1 + math.sqrt(5.04))), (6.0 + 0.6 / (0.1 + math.sqrt(6.09)))), delta);
		assertArrayEquals(mHist, Array(4.01, 5.04, 6.09), delta);
		assertArrayEquals(mGrad, Array(0.1, 0.2, 0.3), delta);}
		
		// add(o1: Option[Array[Double]], o2: Option[Array[Double]]): Option[Array[Double]]
		{val o1 = Some(Array(1.0, 2.0));
		val o2 = Some(Array(2.0, -3.0));
		val res = Math.add(o1, o2);
		assertTrue(res.nonEmpty);
		assertArrayEquals(res.get, Array(3.0, -1.0), delta);}
		{val o1 = Some(Array(1.0, 2.0));
		val o2 = None;
		val res = Math.add(o1, o2);
		assertTrue(res.nonEmpty);
		assertArrayEquals(res.get, Array(1.0, 2.0), delta);}
		{val o1 = None;
		val o2 = Some(Array(2.0, -3.0));
		val res = Math.add(o1, o2);
		assertTrue(res.nonEmpty);
		assertArrayEquals(res.get, Array(2.0, -3.0), delta);}
		{val o1 = None;
		val o2 = None;
		val res = Math.add(o1, o2);
		assertTrue(res.isEmpty);}
		
		// substract(o1: Option[Array[Double]], o2: Option[Array[Double]]): Option[Array[Double]] = (o1, o2)
		{val o1 = Some(Array(1.0, 2.0));
		val o2 = Some(Array(2.0, -3.0));
		val res = Math.substract(o1, o2);
		assertTrue(res.nonEmpty);
		assertArrayEquals(res.get, Array(-1.0, 5.0), delta);}
		{val o1 = Some(Array(1.0, 2.0));
		val o2 = None;
		val res = Math.substract(o1, o2);
		assertTrue(res.nonEmpty);
		assertArrayEquals(res.get, Array(1.0, 2.0), delta);}
		{val o1 = None;
		val o2 = Some(Array(2.0, -3.0));
		val res = Math.substract(o1, o2);
		assertTrue(res.nonEmpty);
		assertArrayEquals(res.get, Array(-2.0, 3.0), delta);}
		{val o1 = None;
		val o2 = None;
		val res = Math.substract(o1, o2);
		assertTrue(res.isEmpty);}
		
		// add(o1: Option[Array[Double]], v2: Array[Double]): Array[Double]
		{val o1 = Some(Array(1.0, 2.0));
		val v2 = Array(2.0, -3.0);
		val res = Math.add(o1, v2);
		assertArrayEquals(res, Array(3.0, -1.0), delta);}
		{val o1 = None;
		val v2 = Array(2.0, -3.0);
		val res = Math.add(o1, v2);
		assertArrayEquals(res, Array(2.0, -3.0), delta);}
		
		// substract(o1: Option[Array[Double]], v2: Array[Double]): Array[Double]
		{val o1 = Some(Array(1.0, 2.0));
		val v2 = Array(2.0, -3.0);
		val res = Math.substract(o1, v2);
		assertArrayEquals(res, Array(-1.0, 5.0), delta);}
		{val o1 = None;
		val v2 = Array(2.0, -3.0);
		val res = Math.substract(o1, v2);
		assertArrayEquals(res, Array(-2.0, 3.0), delta);}
		
		// addTo(t1: Array[Array[Array[Double]]], t2: Array[Array[Array[Double]]]): Unit
		{val t1 = Array(Array(Array(0.1, 0.2, 0.3), Array(-0.1, -0.2, -0.3)), Array(Array(-0.1, -0.2, -0.3), Array(0.1, 0.2, 0.3)));
		Math.addTo(t1, Array(Array(Array(0.1, 0.1, 0.1), Array(-0.1, -0.2, -0.3)), Array(Array(0.2, 0.2, 0.2), Array(0.1, 0.2, 0.3))))
		assertArrayEquals(t1(0)(0), Array(0.2, 0.3, 0.4), delta);
		assertArrayEquals(t1(0)(1), Array(-0.2, -0.4, -0.6), delta);
		assertArrayEquals(t1(1)(0), Array(0.1, 0.0, -0.1), delta);
		assertArrayEquals(t1(1)(1), Array(0.2, 0.4, 0.6), delta);}
		
		// substractTo(t1: Array[Array[Array[Double]]], t2: Array[Array[Array[Double]]]): Unit
		{val t1 = Array(Array(Array(0.1, 0.2, 0.3), Array(-0.1, -0.2, -0.3)), Array(Array(-0.1, -0.2, -0.3), Array(0.1, 0.2, 0.3)));
		Math.substractTo(t1, Array(Array(Array(0.1, 0.1, 0.1), Array(-0.1, -0.2, -0.3)), Array(Array(0.1, 0.2, 0.3), Array(0.2, 0.2, 0.2))))
		assertArrayEquals(t1(0)(0), Array(0.0, 0.1, 0.2), delta);
		assertArrayEquals(t1(0)(1), Array(0.0, 0.0, 0.0), delta);
		assertArrayEquals(t1(1)(0), Array(-0.2, -0.4, -0.6), delta);
		assertArrayEquals(t1(1)(1), Array(-0.1, 0.0, 0.1), delta);}
		
		// negate(v: IndexedSeq[Double]): Array[Double]
		{val res = Math.negate(Array(1.0, 2.0, -3.0));
		assertArrayEquals(res, Array(-1.0, -2.0, 3.0), delta);}
		for(_ <- 0 to 10) {val v = Array.fill(10)(util.Random.nextDouble);
		val res = Math.negate(v);
		assertArrayEquals(res, v.map(-_), delta);}
		
		// negateTo(v: Array[Double]): Unit
		{val input = Array(1.0, 2.0, -3.0);
		Math.negateTo(input);
		assertArrayEquals(input, Array(-1.0, -2.0, 3.0), delta);}
		for(_ <- 0 to 10) {val input = Array.fill(10)(util.Random.nextDouble);
		val v = input.map(-_);
		Math.negateTo(input);
		assertArrayEquals(input, v, delta);}
		
		// add(p: (Int, Int), q: (Int, Int)): (Int, Int)
		{val res = Math.add((1, 2), (1, -2));
		assertEquals(res, (2, 0));}
		for(_ <- 0 to 10) {val a = util.Random.nextInt;
		val b = util.Random.nextInt;
		val c = util.Random.nextInt;
		val d = util.Random.nextInt;
		val res = Math.add((a, b), (c, d));
		assertEquals(res, ((a + c), (b + d)));}
		
		// add(p: (Int, Int, Int), q: (Int, Int, Int)): (Int, Int, Int)
		{val res = Math.add((1, 2, 3), (1, -2, 12));
		assertEquals(res, (2, 0, 15));}
		for(_ <- 0 to 10) {val a = util.Random.nextInt;
		val b = util.Random.nextInt;
		val c = util.Random.nextInt;
		val d = util.Random.nextInt;
		val e = util.Random.nextInt;
		val f = util.Random.nextInt;
		val res = Math.add((a, b, c), (d, e, f));
		assertEquals(res, ((a + d), (b + e), (c + f)));}
		
		// sum(ps: (Int, Int)*): (Int, Int)
		{val res = Math.sum((1, 2), (1, -2), (5, -5), (12, 0));
		assertEquals(res, (19, -5));}
		for(_ <- 0 to 10) {val a = (util.Random.nextInt, util.Random.nextInt);
		val b = (util.Random.nextInt, util.Random.nextInt);
		val c = (util.Random.nextInt, util.Random.nextInt);
		val d = (util.Random.nextInt, util.Random.nextInt);
		val res = Math.sum(a, b, c, d);
		assertEquals(res, ((a._1 + b._1 + c._1 + d._1), (a._2 + b._2 + c._2 + d._2)));}
	}
}
