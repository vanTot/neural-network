// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import org.junit.Test;
import junit.framework.TestCase;
import org.junit.Assert._;

import collection.mutable;

import neuralNetwork._;

class LearningNeuralNetworkTests extends TestCase {
	def test {
		val delta: Double = 1e-15;
		val delta2: Double = 1e-6;
		
		val h: Double = 1e-6;
		val threshold: Double = 5e-8;
		
		// One cannot test non-derivable systems that way
		// So avoid SSL layer and ReLU non-linearity

		{val layersDesc = "ML:SoftMax" +: Array.fill(3)("ML:" + "Tanh") :+ "ML:Sigmoid";
		val layersSize = 8 +: Array.fill(3 + 1)(32) :+ 16;
		
		val network = neuralNetwork.TransformNNFactory(layersDesc, layersSize);
		assertTrue(network.testGradient(h, threshold));}
		
		{val layersDesc = "ML:Sigmoid" +: Array.fill(3)("2DL:" + "Tanh") :+ "ML:Sigmoid";
		val layersSize = Array.fill(3 + 1)(32) :+ 16;
		
		val network = neuralNetwork.ScoreNNFactory(layersDesc, layersSize);
		assertTrue(network.testGradient(h, threshold));}
		
		/*{val imageWidth = 28;
			val imageHeight = 28;

			val network = {
				// 1 convolutional layer consisting of 10 filters of dimension 9x9 with 2x2 max-pooling
				val convLayer = {
					val nbFilters = 10;
					val filtersXSize = 9;
					val filtersYSize = 9;
					val poolingX = 2;
					val poolingY = 2;

					val filtersType = "ML";
					val filtersNonLinearity = "ReLU";
					val pixelSize = 1;

					val layerOptions = Array(
							(filtersType + "|" + filtersNonLinearity), // filters options
							(filtersXSize + "x" + filtersYSize), // filterSize 
							(poolingX + "x" + poolingY), // pooling
							).map(_.toString);

					val distribution = ((c: Double) => (util.Random.nextDouble - 0.5) / c);

					TransformNNFactory.buildConvLayer(imageWidth, imageHeight, pixelSize, nbFilters, layerOptions, distribution);
				}

				// followed by a matrix layer with SoftMax non-linearity
				val outLayer = {
					val inSize = convLayer.p.height;
					val outSize = 10;
					val distribution = ((c: Double) => (util.Random.nextDouble - 0.5) / c);

					TransformNNFactory.buildLayer(inSize, outSize, Array("ML", "SoftMax"), distribution);
				}

				TransformNN(List(convLayer, outLayer));
			}

			assertTrue(network.testGradient(h, threshold));}*/
	}
}

class TransformNNTests extends TestCase {
	def test {
		val delta: Double = 1e-15;
		val delta2: Double = 1e-6;
		
		// transformNN with one softMax-matrix layer
		val layer1 = {
			val m = Array(Array(-1.0, 2.0, 3.0), Array(4.0, 5.0, 6.0), Array(7.0, 8.0, -9.0));
			
			val t = Array(4.0, 13.0, -34.0)
			
			Array2DLayer(m, t, SoftMax);
		}
		
		val NN = TransformNN(List(layer1));
		
		val in = Array(-1.0, 2.0, -3.0);
		
		val (out, dF) = NN.transformAndGradient(in);
		
		// Transform
		assertArrayEquals(out, NN.execute(in), delta);
		
		assertArrayEquals(out, Array(
			math.exp(0)/(math.exp(0) + math.exp(1) + math.exp(2)),
			math.exp(1)/(math.exp(0) + math.exp(1) + math.exp(2)),
			math.exp(2)/(math.exp(0) + math.exp(1) + math.exp(2))
		), delta);
		
		// Gradient
		val dOut = Array(0.5, 0.3, 0.2);
		
		val (dParam, dIn) = dF(dOut);
//		val (dParam, dIn) = NN.gradient(in, dOut);
		
		dParam match {
			case (dL1: Array2DLayerParam) :: Nil => {
				assertEquals(dL1.m.length, 3);
				assertArrayEquals(dL1.m(0), Array(
					(dOut(0) * out(0) * (1 - out(0)) + dOut(1) * out(1) * (0 - out(0)) + dOut(2) * out(2) * (0 - out(0))) * in(0),
					(dOut(0) * out(0) * (1 - out(0)) + dOut(1) * out(1) * (0 - out(0)) + dOut(2) * out(2) * (0 - out(0))) * in(1),
					(dOut(0) * out(0) * (1 - out(0)) + dOut(1) * out(1) * (0 - out(0)) + dOut(2) * out(2) * (0 - out(0))) * in(2)
				), delta);
				assertArrayEquals(dL1.m(1), Array(
					(dOut(0) * out(0) * (0 - out(1)) + dOut(1) * out(1) * (1 - out(1)) + dOut(2) * out(2) * (0 - out(1))) * in(0),
					(dOut(0) * out(0) * (0 - out(1)) + dOut(1) * out(1) * (1 - out(1)) + dOut(2) * out(2) * (0 - out(1))) * in(1),
					(dOut(0) * out(0) * (0 - out(1)) + dOut(1) * out(1) * (1 - out(1)) + dOut(2) * out(2) * (0 - out(1))) * in(2)
				), delta);
				assertArrayEquals(dL1.m(2), Array(
					(dOut(0) * out(0) * (0 - out(2)) + dOut(1) * out(1) * (0 - out(2)) + dOut(2) * out(2) * (1 - out(2))) * in(0),
					(dOut(0) * out(0) * (0 - out(2)) + dOut(1) * out(1) * (0 - out(2)) + dOut(2) * out(2) * (1 - out(2))) * in(1),
					(dOut(0) * out(0) * (0 - out(2)) + dOut(1) * out(1) * (0 - out(2)) + dOut(2) * out(2) * (1 - out(2))) * in(2)
				), delta);
				
				assertArrayEquals(dL1.t, Array(
					(dOut(0) * out(0) * (1 - out(0)) + dOut(1) * out(1) * (0 - out(0)) + dOut(2) * out(2) * (0 - out(0))),
					(dOut(0) * out(0) * (0 - out(1)) + dOut(1) * out(1) * (1 - out(1)) + dOut(2) * out(2) * (0 - out(1))),
					(dOut(0) * out(0) * (0 - out(2)) + dOut(1) * out(1) * (0 - out(2)) + dOut(2) * out(2) * (1 - out(2)))
				), delta);
				
				assertArrayEquals(dIn, Array(
					dOut(0) * out(0) * ((1 - out(0)) * layer1.p.m(0)(0) + (0 - out(1)) * layer1.p.m(1)(0) + (0 - out(2)) * layer1.p.m(2)(0)) +
					dOut(1) * out(1) * ((0 - out(0)) * layer1.p.m(0)(0) + (1 - out(1)) * layer1.p.m(1)(0) + (0 - out(2)) * layer1.p.m(2)(0)) +
					dOut(2) * out(2) * ((0 - out(0)) * layer1.p.m(0)(0) + (0 - out(1)) * layer1.p.m(1)(0) + (1 - out(2)) * layer1.p.m(2)(0)), 
					
					dOut(0) * out(0) * ((1 - out(0)) * layer1.p.m(0)(1) + (0 - out(1)) * layer1.p.m(1)(1) + (0 - out(2)) * layer1.p.m(2)(1)) +
					dOut(1) * out(1) * ((0 - out(0)) * layer1.p.m(0)(1) + (1 - out(1)) * layer1.p.m(1)(1) + (0 - out(2)) * layer1.p.m(2)(1)) +
					dOut(2) * out(2) * ((0 - out(0)) * layer1.p.m(0)(1) + (0 - out(1)) * layer1.p.m(1)(1) + (1 - out(2)) * layer1.p.m(2)(1)), 
					
					dOut(0) * out(0) * ((1 - out(0)) * layer1.p.m(0)(2) + (0 - out(1)) * layer1.p.m(1)(2) + (0 - out(2)) * layer1.p.m(2)(2)) +
					dOut(1) * out(1) * ((0 - out(0)) * layer1.p.m(0)(2) + (1 - out(1)) * layer1.p.m(1)(2) + (0 - out(2)) * layer1.p.m(2)(2)) +
					dOut(2) * out(2) * ((0 - out(0)) * layer1.p.m(0)(2) + (0 - out(1)) * layer1.p.m(1)(2) + (1 - out(2)) * layer1.p.m(2)(2))
				), delta);
			}
			
			case _ => assert(false);
		}
	}
}

class ScoreNNTests extends TestCase {
	def test {
		val delta: Double = 1e-15;
		val delta2: Double = 1e-6;
		
		// scoreNN with 2 sigmoid-matrix layers
		{val layer1 = {
			val m = Array(Array(0.1, 0.2, 0.3, 0.4), Array(0.1, -0.2, 0.3, -0.4));
			
			val t = Array(0.5, -0.5)
			
			Array2DLayer(m, t, Sigmoid);
		}
		
		val layer2 = {
			val m = Array(Array(2.0, 1.0));
			
			val t = Array(-0.5);
			
			Array2DLayer(m, t, Sigmoid);
		}
		
		val NN = ScoreNN(List(layer1, layer2));
		
		val in = Array(1.0, -2.0, 3.0, -4.0);
		
		val (out, dF) = NN.scoreAndGradient(in);
		
		// Score
		assertEquals(out, NN.execute(in), delta);
		assertEquals(out, (Math.sigmoid(2 * Math.sigmoid(-0.5) + 1 * Math.sigmoid(2.5) - 0.5)), delta);
		
		// Gradient
		val dc = -1.0;
		
		val (dParam, dIn) = dF(dc);
//		val (dParam, dIn) = NN.gradient(in, dc);
		
		dParam match {
			case (dL1: Array2DLayerParam) :: (dL2: Array2DLayerParam) :: Nil => {
				// Layer 2
				val x = Sigmoid.derivative(2 * Math.sigmoid(-0.5) + 1 * Math.sigmoid(2.5) - 0.5);
				
				assertEquals(dL2.m.length, 1);
				assertArrayEquals(dL2.m(0), Array(dc * x * Math.sigmoid(-0.5), dc * x * Math.sigmoid(2.5)), delta);
				
				assertArrayEquals(dL2.t, Array(dc * x), delta);
				
				// Layer 1
				val y = Sigmoid.derivative(-0.5);
				val z = Sigmoid.derivative(2.5);
				
				assertEquals(dL1.m.length, 2);
				assertArrayEquals(dL1.m(0), Array(
					(dc * x * 2) * y * in(0),
					(dc * x * 2) * y * in(1),
					(dc * x * 2) * y * in(2),
					(dc * x * 2) * y * in(3)
				), delta);
				assertArrayEquals(dL1.m(1), Array(
					(dc * x * 1) * z * in(0),
					(dc * x * 1) * z * in(1),
					(dc * x * 1) * z * in(2),
					(dc * x * 1) * z * in(3)
				), delta);
				
				assertArrayEquals(dL1.t, Array((dc * x * 2) * y, (dc * x * 1) * z), delta);
				
				assertArrayEquals(dIn, Array(
					(dc * x * 2) * y * 0.1 + (dc * x * 1) * z * 0.1,
					(dc * x * 2) * y * 0.2 + (dc * x * 1) * z * -0.2,
					(dc * x * 2) * y * 0.3 + (dc * x * 1) * z * 0.3,
					(dc * x * 2) * y * 0.4 + (dc * x * 1) * z * -0.4
				), delta);
			}
			
			case _ => assert(false);
		}}
		
		// TODO faire deux NN avec 2 layers (mais l'un avec une dummy linéarité finale), et vérifier que protoGradient fait bien ce quil faut
	}
}


class SerializationTests extends TestCase {
	def test {
		val delta: Double = 1e-15;
		val delta2: Double = 1e-6;
		
		{val layersDesc = "ML:SoftMax" +: Array.fill(3)("ML:" + "ReLU") :+ "ML:Sigmoid";
		val layersSize = 8 +: Array.fill(3 + 1)(32) :+ 16;
		
		val network = neuralNetwork.TransformNNFactory(layersDesc, layersSize);
		
		val str = {
			val stringBuilder = new mutable.StringBuilder();
			network.customSerialize(stringBuilder);
			
			stringBuilder.toString;
		}
		
		val network2 = TransformNN.read(str);
		
		for(_ <- 0 to 10) {
			val in = Array.fill(16)(util.Random.nextDouble - 0.5);
			
			val out = network.execute(in);
			val out2 = network2.execute(in);
			
			assertArrayEquals(out, out2, delta);
		}}
		
		{val imageWidth = 28;
		val imageHeight = 28;
		
		val network = {
			// 1 convolutional layer consisting of 10 filters of dimension 9x9 with 2x2 max-pooling
			val convLayer = {
				val nbFilters = 10;
				val filtersXSize = 9;
				val filtersYSize = 9;
				val poolingX = 2;
				val poolingY = 2;
				
				val filtersType = "ML";
				val filtersNonLinearity = "ReLU";
				val pixelSize = 1;
				
				val layerOptions = Array(
					(filtersType + "|" + filtersNonLinearity), // filters options
					(filtersXSize + "x" + filtersYSize), // filterSize 
					(poolingX + "x" + poolingY), // pooling
				).map(_.toString);
				
				val distribution = ((c: Double) => (util.Random.nextDouble - 0.5) / c);
				
				TransformNNFactory.buildConvLayer(imageWidth, imageHeight, pixelSize, nbFilters, layerOptions, distribution);
			}
			
			// followed by a matrix layer with SoftMax non-linearity
			val outLayer = {
				val inSize = convLayer.p.height;
				val outSize = 10;
				val distribution = ((c: Double) => (util.Random.nextDouble - 0.5) / c);
				
				TransformNNFactory.buildLayer(inSize, outSize, Array("ML", "SoftMax"), distribution);
			}
			
			TransformNN(List(convLayer, outLayer));
		}
		
		val str = {
			val stringBuilder = new mutable.StringBuilder();
			network.customSerialize(stringBuilder);
			
			stringBuilder.toString;
		}
		
		val network2 = TransformNN.read(str);
		
		for(_ <- 0 to 10) {
			val in = Array.fill((imageWidth * imageHeight))(util.Random.nextDouble - 0.5);
			
			val out = network.execute(in);
			val out2 = network2.execute(in);
			
			assertArrayEquals(out, out2, delta);
		}}
	}
}
