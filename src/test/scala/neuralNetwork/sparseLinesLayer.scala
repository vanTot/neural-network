// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//	 http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import org.junit.Test;
import junit.framework.TestCase;
import org.junit.Assert._;

import collection.mutable;

import neuralNetwork._;

class SparseLinesLayerTests extends TestCase {
	def test {
		val delta: Double = 1e-15;
		val delta2: Double = 1e-6;
		
		// Check that a SparseLinesLayer is (almost) equivalent to a Array2DLayer with lots of 0
		val height = 10;
		val width = 100;
			
		val k = width / 2;
		val alpha = math.ceil(math.sqrt(width)).toInt;
		assert(k >= alpha);
		
		val coeffs = Array.fill(height)(Array.fill(k)(util.Random.nextDouble));
		val positions = Array.fill(height)(util.Random.shuffle(0 to (width - 1)).take(k).toArray);
		
		val t = Array.fill(height)(util.Random.nextDouble);
		
		val sparseLinesLayer = SparseLinesLayer(coeffs, positions, t, width, alpha, neuralNetwork.Tanh);
		
		val m = Array.ofDim[Double](height, width);
		{var i = 0;
		while(i < height) {
			var j = 0;
			while(j < alpha) {
				val pos = positions(i)(j);
				
				m(i)(pos) = coeffs(i)(j);
				
				j += 1;
			}
			
			i += 1;
		}}
		
		val matrixLayer = Array2DLayer(m, t, neuralNetwork.Tanh);
		
		val in = Array.fill(width)(util.Random.nextDouble);
		
		{val gradSLL = sparseLinesLayer.gradient(in);
		val gradML = matrixLayer.gradient(in);
		
		val (outSLL, dFSLL) = sparseLinesLayer.gradient(in);
		val (outML, dFML) = matrixLayer.gradient(in);
		
		// Same output
		assertArrayEquals(outSLL, outML, delta);
		
		// Same gradient
		val dE: Array[Double] = Array.fill[Double](height)(util.Random.nextDouble);
		val (dpSLL, dinSLL) = dFSLL(dE);
		val (dpML, dinML) = dFML(dE);
		
		for(i <- 0 until height) {
			val lineSLL = dpSLL.coeffs(i);
			val lineML = dpML.m(i);
			
			for(j <- 0 until lineSLL.length) {
				assertEquals(lineSLL(j), lineML(sparseLinesLayer.p.positions(i)(j)), delta);
			}
		}
		assertArrayEquals(dpSLL.t, dpML.t, delta);
		
		assertArrayEquals(dinSLL, dinML, delta);
		}
		
		// Proto-gradient
		{val gradSLL = sparseLinesLayer.protoGradient(in);
		val gradML = matrixLayer.protoGradient(in);
		
		val (outSLL, dFSLL) = sparseLinesLayer.gradient(in);
		val (outML, dFML) = matrixLayer.gradient(in);
		
		// Same output
		assertArrayEquals(outSLL, outML, delta);
		
		// Same gradient
		val dE: Array[Double] = Array.fill[Double](height)(util.Random.nextDouble);
		val (dpSLL, dinSLL) = dFSLL(dE);
		val (dpML, dinML) = dFML(dE);
		
		for(i <- 0 until height) {
			val lineSLL = dpSLL.coeffs(i);
			val lineML = dpML.m(i);
			
			for(j <- 0 until lineSLL.length) {
				assertEquals(lineSLL(j), lineML(sparseLinesLayer.p.positions(i)(j)), delta);
			}
		}
		assertArrayEquals(dpSLL.t, dpML.t, delta);
		
		assertArrayEquals(dinSLL, dinML, delta);
		}
	}
}
